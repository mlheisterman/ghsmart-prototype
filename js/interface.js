(function ($) {

  /* Set all select elements to use select2  */
  $('.select2').select2();


  /* Set up textarea elements to use froala WYSIWYG editor */
  $('.edit').editable({
    inlineMode: false,
    minHeight: 200,
    buttons: ['bold', 'italic', 'underline', 'insertOrderedList', 'insertUnorderedList', 'fullscreen', 'garvis'],
    key: 'YF-10usxwC-22A-16seA3rqkqisqyoE4obc==',
    customDropdowns: {
      garvis: {
        // Dropdown Title
        title: 'gARVIS Questions',

        // Dropdown Icon
        icon: {
          type: 'txt',
          value: 'gARVIS ?s'
        },

        // Callback on refresh.
        refresh: function () {
          console.log ('do refresh');
        },

        // Callback on dropdown show.
        refreshOnShow: function () {
          console.log ('do refresh when show');
        },

        // Options.
        options: {
          // First option.
          'Tell me more': function () {
            this.insertHTML('<h4>Tell me more.</h4><br>&nbsp;');
          },
          // Second option.
          'What do you mean?': function () {
            this.insertHTML('<h4>What do you mean?</h4><br>&nbsp;');
          },
          // Third option.
          'How so?': function () {
            this.insertHTML('<h4>How so?</h4><br>&nbsp;');
          },
          // Fourth option.
          'How did your performance compare to (peers/plan/previous year)?': function () {
            this.insertHTML('<h4>How did your performance compare to </h4>');
          },

          // Fifth option.
          'How did that impact (you/the organization)?': function () {
            this.insertHTML('<h4>How did that impact </h4>');
          },
          // Sixth option.
          'How did you (feel/dealwith that/communicate that)?': function () {
            this.insertHTML('<h4>How did you?</h4>');
          }
        }
      },
    }
  });

$('.edit-comments').editable({
    inlineMode: false,
    minHeight: 100,
    maxHeight: 100,
    buttons: ['bold', 'italic', 'underline', 'insertOrderedList', 'insertUnorderedList', 'fullscreen', 'garvis'],
    key: 'YF-10usxwC-22A-16seA3rqkqisqyoE4obc==',
    customDropdowns: {
      garvis: {
        // Dropdown Title
        title: 'gARVIS Questions',

        // Dropdown Icon
        icon: {
          type: 'txt',
          value: 'gARVIS ?s'
        },

        // Callback on refresh.
        refresh: function () {
          console.log ('do refresh');
        },

        // Callback on dropdown show.
        refreshOnShow: function () {
          console.log ('do refresh when show');
        },

        // Options.
        options: {
          // First option.
          'Tell me more': function () {
            this.insertHTML('<h4>Tell me more.</h4><br>&nbsp;');
          },
          // Second option.
          'What do you mean?': function () {
            this.insertHTML('<h4>What do you mean?</h4><br>&nbsp;');
          },
          // Third option.
          'How so?': function () {
            this.insertHTML('<h4>How so?</h4><br>&nbsp;');
          },
          // Fourth option.
          'How did your performance compare to (peers/plan/previous year)?': function () {
            this.insertHTML('<h4>How did your performance compare to </h4>');
          },

          // Fifth option.
          'How did that impact (you/the organization)?': function () {
            this.insertHTML('<h4>How did that impact </h4>');
          },
          // Sixth option.
          'How did you (feel/dealwith that/communicate that)?': function () {
            this.insertHTML('<h4>How did you?</h4>');
          }
        }
      },
    }
  });

  /* Sticky Table headers javascript */
  // $('.sticky-header').stickyTableHeaders();

  /* Hide/Show filters – Hidden by default */
  // $(document).on("click", ".expand-collapse-trigger", function(event) {
  //   $(".filters").toggleClass("expanded");
  //   $(".filters .fa").toggleClass("fa-plus");
  //   $(".filters .fa").toggleClass("fa-minus");
  // });
  
  /* If not loading the entire Materialize javascript, you can enable individually here */
  // $('.modal-trigger').leanModal();
  // $('ul.tabs').tabs();
  // $('.button-collapse').sideNav();


  /* Dynamic ScrollFire Actions */
  var options = [
    {selector: '#reference', offset: -1000, callback: 'Materialize.showStaggeredList("#reference")' }
  ];
  Materialize.scrollFire(options);

  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });
  $('select').material_select();


}(jQuery));

$(document).ready(function(){
  var infoWidth = $('#info-bar').width();
  //var mainHeight = $('main').height();
  //var footerHeight = $('footer').height();

  // Floating-Fixed Sidebar
  $('.sidebar').css('width', infoWidth);
  $('.sidebar').pushpin({ top: 0, offset: 10 });
  $('.button-collapse').sideNav();
  $('.scrollspy').scrollSpy();

  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();

  $('.tooltipped').tooltip({delay: 50});
});
