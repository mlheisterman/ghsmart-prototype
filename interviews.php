<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Inverviews</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="container">
      <div class="row">
        <?php include("components/global/side-upcoming.inc"); ?>
        <div class=" col s12 m9 border-left"> <!-- BEGIN Right Column -->
          <div class="section">
            <a href="dashboard.php">Dashboard</a> // Interviews
          </div>
          <div class="section">
            <div class="row"> <!-- BEGIN Section Header -->
              <h4 class="left col">Interviews</h4>
              <div class="right col">
                <!-- Dropdown Trigger -->
                <a class='btn pop' href='/new-interview.php'><i class="fa fa-plus left"></i> New Interview</a>
              </div>
            </div> <!-- END Section Header -->
            <div class="filters">
              <h5>Refine Results</h5>
              <form>
                <div class="input-field col s12 m3">
                  <input placeholder="Name" id="name" type="text">
                  <label for="name">Candidate Name</label>
                </div>
                <div class="input-field col s12 m3">
                  <select id="period">
                    <option value=""></option>
                    <option value="1">All</option>
                    <option value="2">Screening</option>
                    <option value="3">Topgrading</option>
                    <option value="4">Focus</option>
                    <option value="5">Reference</option>
                  </select>
                  <label for="period">Interview Type</label>
                </div>
                <div class="col s6 m3">
                  <label for="from-date">Pick a From Date</label>
                  <input id="from-date" type="date" class="datepicker">
                </div>
                <div class="col s6 m3">
                  <label for="to-date">Pick a To Date</label>
                  <input id="to-date" type="date" class="datepicker">
                </div>
              </form>
            </div>
            
            <table class="bordered white inner"><!-- BEGIN Table -->
              <thead>
                <tr>
                  <th data-field="date">Date</th>
                  <th data-field="name">Name</th>
                  <th data-field="interview">Interview</th>
                  <th data-field="scorecard">Scorecard</th>
                  <th data-field="score">Score</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>Jul 09</td>
                  <td><a href="/candidate-albert-norris.php">Albert Norris</a></td>
                  <td><a href="/reference-interview-albert-norris.php">Reference</a></td>
                  <td>Developer</td>
                  <td>93%</td>
                </tr>
                <tr>
                  <td>Jul 09</td>
                  <td><a href="#!" class="fake-link">Chris Wilkerson</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Developer</td>
                  <td>80%</td>
                </tr>
                <tr>
                  <td>Jul 09</td>
                  <td><a href="#!" class="fake-link">Frank Thomas</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Business Development</td>
                  <td>74%</td>
                </tr>
                <tr>
                  <td>Jul 08</td>
                  <td><a href="#!" class="fake-link">Jackie Sumlin</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Business Development</td>
                  <td>95%</td>
                </tr>
                <tr>
                  <td>Jul 08</td>
                  <td><a href="#!" class="fake-link">Sandy Williams</a></td>
                  <td><a href="#!" class="fake-link">Focus</a></td>
                  <td>Business Development</td>
                  <td>81%</td>
                </tr>
                <tr>
                  <td>Jul 08</td>
                  <td><a href="#!" class="fake-link">Tim Hamilton</a></td>
                  <td><a href="#!" class="fake-link">Topgrading</a></td>
                  <td>Developer</td>
                  <td>74%</td>
                </tr>
                <tr>
                  <td>Jul 08</td>
                  <td><a href="#!" class="fake-link">Stacey Webster</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Developer</td>
                  <td>97%</td>
                </tr>
                <tr>
                  <td>Jul 08</td>
                  <td><a href="#!" class="fake-link">Sam Sneau</a></td>
                  <td><a href="#!" class="fake-link">Reference</a></td>
                  <td>Developer</td>
                  <td>90%</td>
                </tr>
                <tr>
                  <td>Jul 08</td>
                  <td><a href="#!" class="fake-link">Jason Demitri</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Developer</td>
                  <td>81%</td>
                </tr>
                <tr>
                  <td>Jul 07</td>
                  <td><a href="/candidate-albert-norris.php">Albert Norris</a></td>
                  <td><a href="/reference-interview-albert-norris.php">Reference</a></td>
                  <td>Developer</td>
                  <td>93%</td>
                </tr>
                <tr>
                  <td>Jul 07</td>
                  <td><a href="#!" class="fake-link">Chris Wilkerson</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Developer</td>
                  <td>80%</td>
                </tr>
                <tr>
                  <td>Jul 07</td>
                  <td><a href="#!" class="fake-link">Frank Thomas</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Business Development</td>
                  <td>74%</td>
                </tr>
                <tr>
                  <td>Jul 06</td>
                  <td><a href="#!" class="fake-link">Jackie Sumlin</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Business Development</td>
                  <td>95%</td>
                </tr>
                <tr>
                  <td>Jul 06</td>
                  <td><a href="#!" class="fake-link">Sandy Williams</a></td>
                  <td><a href="#!" class="fake-link">Focus</a></td>
                  <td>Business Development</td>
                  <td>81%</td>
                </tr>
                <tr>
                  <td>Jul 06</td>
                  <td><a href="#!" class="fake-link">Tim Hamilton</a></td>
                  <td><a href="#!" class="fake-link">Topgrading</a></td>
                  <td>Developer</td>
                  <td>74%</td>
                </tr>
                <tr>
                  <td>Jul 06</td>
                  <td><a href="#!" class="fake-link">Stacey Webster</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Developer</td>
                  <td>97%</td>
                </tr>
                <tr>
                  <td>Jul 06</td>
                  <td><a href="#!" class="fake-link">Sam Sneau</a></td>
                  <td><a href="#!" class="fake-link">Reference</a></td>
                  <td>Developer</td>
                  <td>90%</td>
                </tr>
                <tr>
                  <td>Jul 06</td>
                  <td><a href="#!" class="fake-link">Jason Demitri</a></td>
                  <td><a href="#!" class="fake-link">Screening</a></td>
                  <td>Developer</td>
                  <td>81%</td>
                </tr>
                <tr>
                  <td colspan="5">
                    <ul class="pagination">
                      <li class="disabled"><a href="#!" class="fake-link"><i class="material-icons">chevron_left</i></a></li>
                      <li class="active"><a href="#!" class="fake-link">1</a></li>
                      <li class="waves-effect"><a href="#!" class="fake-link">2</a></li>
                      <li class="waves-effect"><a href="#!" class="fake-link">3</a></li>
                      <li class="waves-effect"><a href="#!" class="fake-link">4</a></li>
                      <li class="waves-effect"><a href="#!" class="fake-link">5</a></li>
                      <li class="waves-effect"><a href="#!" class="fake-link"><i class="material-icons">chevron_right</i></a></li>
                    </ul>
                  </td>
                </tr>
              </tbody>
            </table> <!-- END Table -->
          </div>
        </div> <!-- END Right Column -->
      </main>
        <?php include("components/global/footer.inc"); ?>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
