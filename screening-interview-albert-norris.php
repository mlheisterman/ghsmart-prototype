<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Screening Interview - Albert Norris</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="row">
      <div id="info-bar" class="col s12 m4 l3">
        <div class="sidebar">
          <div class="row">
            <div class="col m4">
              <img class="circle" src="/images/albert.jpg">
            </div>
            <div class="col m8">
              <h5 class="martop-0"><a href="/candidate-albert-norris.php">Albert Norris</a></h5>
              <ul>
                <li><strong>Scorecard:</strong> <a href="/scorecard-developer.php">Developer</a></li>
                <li><strong>Appt:</strong> 07/07/2015 (<a href="mailto:albert.norris@microsoft.com">email</a>) (<a href="tel://1-555-555-5555">phone</a>)</li>
              </ul>
            </div>
            <div class="col m12">
              <a class="waves-effect waves-light btn left tiny modal-trigger" style="margin-right: 4px;" href="#compmodal">Competencies</a>
                  <a class="waves-effect waves-light btn left tiny modal-trigger" style="margin-right: 4px;" href="#outcomemodal">Outcomes</a><!-- Competencies Modal Trigger -->
                  <a class="waves-effect waves-light btn tiny modal-trigger" href="#refmodal">References</a> <!-- Reference Modal Trigger -->
            </div>
          </div>
          <div class="col s12">
            <h6 class="martop-0"><a href="https://www.linkedin.com/in/albertcmartin" target="_blank"><i class="fa fa-linkedin-square" style="font-size: 1.25rem;"></i></a> Job History</h6>
            <table class="no-bg">
              <thead>
                <tr>
                    <th data-field="id">Years</th>
                    <th data-field="name">Position</th>
                    <th data-field="price">Company</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>2010-2015</td>
                  <td>Senior Developer</td>
                  <td>Oracle</td>
                </tr>
                <tr>
                  <td>2007-2010</td>
                  <td>Junior Developer</td>
                  <td>Microsoft</td>
                </tr>
                <tr>
                  <td>2006-2007</td>
                  <td>Intern</td>
                  <td>Microsoft</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col hide-on-small-only s12">
            <h5 class="marbot-0 martop-2">Jump to Questions</h5>
            <ul class="table-of-contents">
              <li><a href="#career-goals">Career goals?</a></li>
              <li><a href="#good-prof">What are you good at?</a></li>
              <li><a href="#bad-prof">What are you bad at?</a></li>
              <li><a href="#bosses">How would your bosses rate you?</a></li>
              <li><a href="#general">General notes</a></li>
            </ul>
          </div>
        </div>
      </div>     

      <div class="col s12 m8 l9 offset-l3 marbot-5">
        <div class="section">
          <a href="dashboard.php">Dashboard</a> // <a href="/interviews.php">Interviews</a> // Screening - Albert Norris
        </div>
        <div id="career-goals" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">What are your career goals? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ideally, a candidate will share career goals that match your company's needs.  If he or she lacks goals or sounds like an echo of your own Web site, screen that person out."><i class="material-icons">info_outline</i></a></h5>
            <form>
              <textarea class="edit text-small" name="test" rows="6"></textarea>
            </form> 
          </div>
        </div>
        <div id="good-prof" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">What are you really good at professionally? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="If you see a major gap between the candidate's strengths and your scorecard, screen that person out."><i class="material-icons">info_outline</i></a></h5>
            <form>
              <textarea class="edit text-small" name="test" rows="6"></textarea>
            </form> 
          </div>
        </div>
        <div id="bad-prof" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">What are you not good at or not interested in doing professionally? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="There should be 5-8 areas where the candidate falls short or lacks interest.  If you can't get him or her to identify real areas, screen that person out."><i class="material-icons">info_outline</i></a></h5>
            <form>
              <textarea class="edit text-small" name="test" rows="6"></textarea>
            </form> 
          </div>
        </div>
        <div id="bosses" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">Who were your last 5 bosses and how will they rate you on a 1-10 scale WHEN we talk to them? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="This is not IF, but WHEN.  Scores should be 8+.  Too many 6-, screen that person out.  Be sure to ask the candidate how to spell the name and then add the reference using the button in the left column of this page."><i class="material-icons">info_outline</i></a></h5>
            <form>
              <textarea class="edit text-small" name="test" rows="6"></textarea>
            </form> 
          </div>
        </div>
        <div id="general" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">General Notes</h5>
            <form>
              <textarea class="edit text-small" name="test"></textarea>
            </form> 
          </div>
        </div>
        <div id="general" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">Candidate's Screening Interview Checklist and Score <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Only those who have both checks and a score of 90+ go on to the next interview."><i class="material-icons">info_outline</i></a></h5>
            <form>
              <div class='col s12 white z-depth-1'>
                <div class="input-field col s6 m4 border-right">
                  <input type="checkbox" class="filled-in" id="scorecard" />
                  <label for="scorecard">Candidate matches scorecard</label>
                  <input type="checkbox" class="filled-in" id="weakness" />
                  <label for="weakness">Candidate weaknesses manageable</label>
                </div>
                <div class="input-field col s6 m2 martop-2">
                  <input placeholder="1-100" id="score" type="number" min="1" max="100">
                  <label for="score">Screening Score</label>
                </div>
                <div class="input-field col s6 m4 martop-2">
                  <a class="btn disabled">Promote to Topgrading</a>
                </div>
              </div>
            </form> 
          </div>
        </div>
        <div class="fixed-action-btn" style="bottom: 50px; left: 24px;">
          <h6 class="good-color">last autosave: 12:43am</h6>
        </div>
      </div>
      
      <!-- Reference Modal Body -->
      <div id="refmodal" class="modal bottom-sheet">
        <div class="modal-content">
          <h5>References</h5>
          <div class="col s12 m6 border-right">
            <ul>
              <li>John Franklin (<a class="fake-link" href="#!">edit</a>)</li>
              <li>Ben Jackson (<a class="fake-link" href="#!">edit</a>)</li>
              <li>Sally Loyd (<a class="fake-link" href="#!">edit</a>)</li>
            </ul>
          </div>
          <div class="col s12 m6">
            <form>
              <div class="input-field col s12">
                <input placeholder="Reference Name" id="name" type="text">
                <label for="name">Add a Reference</label>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer no-bg">
          <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Save/Close</a>
        </div>
      </div>
        <?php include("components/global/bottom-competencies-modal.inc"); ?>
        <?php include("components/global/bottom-outcomes-modal.inc"); ?>
    </main>
    <?php include("components/global/footer.inc"); ?>
    <?php include("components/global/foot.inc"); ?>
  </body>
</html>
