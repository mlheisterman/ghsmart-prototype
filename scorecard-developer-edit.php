<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Developer | Edit</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="container">
      <div class="row">
        <?php include("components/global/side-upcoming.inc"); ?>
        <div class=" col s12 m9 border-left"> <!-- BEGIN Right Column -->
          <div class="section">
            <a href="/dashboard.php">Dashboard</a> // <a href="/scorecards.php">Scorecards</a> // <a href="/scorecard-developer.php">Developer</a> // Edit
          </div>
          <div class="section">
            <div class="row"> <!-- BEGIN Section Header -->
              <h4 class="col s12">Developer Scorecard</h4>
            </div> <!-- END Section Header -->
            <div class="col s12 padall-1">
              <form>
                <div class="row white padall-1">
                  <h5 class="col s12">General Information</h5>
                  <div class="input-field col s6">
                    <input id="name" type="text" class="validate" value="Developer">
                    <label for="name">Position Name</label>
                  </div>
                  <div class="input-field col s12">
                    <textarea id="mission" class="materialize-textarea">
                      To deliver lasting value to our clients through the selection and implementation of bestfit web
                      technologies that solve their business problems elegantly, securely and reliably, all with a
                      friendly, client centered approach to service and communication.</textarea>
                    <label for="mission">Mission</label>
                  </div>
                </div>
                <div class="row white padall-1">
                  <h5 class="col s12">Critical Competencies (choose all applicable)</h5>
                  <div class="input-field col s12 m6">
                    <select>
                      <option value="" disabled>Choose your option</option>
                      <option value="1" selected>Efficiency</option>
                      <option value="2">Honesty/Integrity</option>
                      <option value="3">Organization and Planning</option>
                      <option value="4">Aggressiveness</option>
                      <option value="5">Follow-through on commitments</option>
                      <option value="6">Intelligence</option>
                      <option value="7">Analytical Skills</option>
                      <option value="7">Attention to detail</option>  
                      <option value="7">Persistence</option> 
                      <option value="7">Proactivity</option> 
                      <option value="7">Ability to hire A Players (for managers)</option> 
                      <option value="7">Ability to develop people (for managers)</option> 
                      <option value="7">Flexibility/adaptability</option> 
                      <option value="7">Calm under pressure</option> 
                      <option value="7">Strategic thinking/visioning</option> 
                      <option value="7">Creativity/innovation</option> 
                      <option value="7">Enthusiasm</option>                   
                      <option value="7">Work ethic</option> 
                      <option value="7">High standards</option> 
                      <option value="7">Listening</option> 
                      <option value="7">Openness to criticism and ideas</option> 
                      <option value="7">Communication</option> 
                      <option value="7">Teamwork</option> 
                      <option value="7">Persuasion</option> 
                    </select>
                  </div>
                  <div class="input-field col s12 m6">
                    <select>
                      <option value="" disabled>Choose your option</option>
                      <option value="1">Efficiency</option>
                      <option value="2">Honesty/Integrity</option>
                      <option value="3">Organization and Planning</option>
                      <option value="4">Aggressiveness</option>
                      <option value="5">Follow-through on commitments</option>
                      <option value="6">Intelligence</option>
                      <option value="7">Analytical Skills</option>
                      <option value="7">Attention to detail</option>  
                      <option value="7">Persistence</option> 
                      <option value="7">Proactivity</option> 
                      <option value="7">Ability to hire A Players (for managers)</option> 
                      <option value="7">Ability to develop people (for managers)</option> 
                      <option value="7">Flexibility/adaptability</option> 
                      <option value="7">Calm under pressure</option> 
                      <option value="7">Strategic thinking/visioning</option> 
                      <option value="7">Creativity/innovation</option> 
                      <option value="7" selected>Enthusiasm</option>                   
                      <option value="7">Work ethic</option> 
                      <option value="7">High standards</option> 
                      <option value="7">Listening</option> 
                      <option value="7">Openness to criticism and ideas</option> 
                      <option value="7">Communication</option> 
                      <option value="7">Teamwork</option> 
                      <option value="7">Persuasion</option> 
                    </select>
                  </div>
                  <div class="input-field col s12 m6">
                    <select>
                      <option value="" disabled>Choose your option</option>
                      <option value="1">Efficiency</option>
                      <option value="2">Honesty/Integrity</option>
                      <option value="3">Organization and Planning</option>
                      <option value="4">Aggressiveness</option>
                      <option value="5">Follow-through on commitments</option>
                      <option value="6">Intelligence</option>
                      <option value="7">Analytical Skills</option>
                      <option value="7">Attention to detail</option>  
                      <option value="7">Persistence</option> 
                      <option value="7">Proactivity</option> 
                      <option value="7">Ability to hire A Players (for managers)</option> 
                      <option value="7">Ability to develop people (for managers)</option> 
                      <option value="7">Flexibility/adaptability</option> 
                      <option value="7">Calm under pressure</option> 
                      <option value="7">Strategic thinking/visioning</option> 
                      <option value="7">Creativity/innovation</option> 
                      <option value="7">Enthusiasm</option>                   
                      <option value="7" selected>Work ethic</option> 
                      <option value="7">High standards</option> 
                      <option value="7">Listening</option> 
                      <option value="7">Openness to criticism and ideas</option> 
                      <option value="7">Communication</option> 
                      <option value="7">Teamwork</option> 
                      <option value="7">Persuasion</option> 
                    </select>
                  </div>
                  <div class="input-field col s12 m6">
                    <select>
                      <option value="" disabled>Choose your option</option>
                      <option value="1">Efficiency</option>
                      <option value="2">Honesty/Integrity</option>
                      <option value="3">Organization and Planning</option>
                      <option value="4" selected>Aggressiveness</option>
                      <option value="5">Follow-through on commitments</option>
                      <option value="6">Intelligence</option>
                      <option value="7">Analytical Skills</option>
                      <option value="7">Attention to detail</option>  
                      <option value="7">Persistence</option> 
                      <option value="7">Proactivity</option> 
                      <option value="7">Ability to hire A Players (for managers)</option> 
                      <option value="7">Ability to develop people (for managers)</option> 
                      <option value="7">Flexibility/adaptability</option> 
                      <option value="7">Calm under pressure</option> 
                      <option value="7">Strategic thinking/visioning</option> 
                      <option value="7">Creativity/innovation</option> 
                      <option value="7">Enthusiasm</option>                   
                      <option value="7">Work ethic</option> 
                      <option value="7">High standards</option> 
                      <option value="7">Listening</option> 
                      <option value="7">Openness to criticism and ideas</option> 
                      <option value="7">Communication</option> 
                      <option value="7">Teamwork</option> 
                      <option value="7">Persuasion</option> 
                    </select>
                  </div>
                  <div class="input-field col s12 m6">
                    <select>
                      <option value="" disabled>Choose your option</option>
                      <option value="1">Efficiency</option>
                      <option value="2">Honesty/Integrity</option>
                      <option value="3">Organization and Planning</option>
                      <option value="4">Aggressiveness</option>
                      <option value="5">Follow-through on commitments</option>
                      <option value="6">Intelligence</option>
                      <option value="7">Analytical Skills</option>
                      <option value="7">Attention to detail</option>  
                      <option value="7">Persistence</option> 
                      <option value="7">Proactivity</option> 
                      <option value="7">Ability to hire A Players (for managers)</option> 
                      <option value="7">Ability to develop people (for managers)</option> 
                      <option value="7">Flexibility/adaptability</option> 
                      <option value="7">Calm under pressure</option> 
                      <option value="7">Strategic thinking/visioning</option> 
                      <option value="7" selected>Creativity/innovation</option> 
                      <option value="7">Enthusiasm</option>                   
                      <option value="7">Work ethic</option> 
                      <option value="7">High standards</option> 
                      <option value="7">Listening</option> 
                      <option value="7">Openness to criticism and ideas</option> 
                      <option value="7">Communication</option> 
                      <option value="7">Teamwork</option> 
                      <option value="7">Persuasion</option> 
                    </select>
                  </div>   
                  <div class="input-field col s12 m6">
                    <select>
                      <option value="" disabled>Choose your option</option>
                      <option value="1">Efficiency</option>
                      <option value="2">Honesty/Integrity</option>
                      <option value="3">Organization and Planning</option>
                      <option value="4">Aggressiveness</option>
                      <option value="5">Follow-through on commitments</option>
                      <option value="6">Intelligence</option>
                      <option value="7">Analytical Skills</option>
                      <option value="7">Attention to detail</option>  
                      <option value="7">Persistence</option> 
                      <option value="7">Proactivity</option> 
                      <option value="7">Ability to hire A Players (for managers)</option> 
                      <option value="7">Ability to develop people (for managers)</option> 
                      <option value="7">Flexibility/adaptability</option> 
                      <option value="7">Calm under pressure</option> 
                      <option value="7">Strategic thinking/visioning</option> 
                      <option value="7">Creativity/innovation</option> 
                      <option value="7">Enthusiasm</option>                   
                      <option value="7">Work ethic</option> 
                      <option value="7" selected>High standards</option> 
                      <option value="7">Listening</option> 
                      <option value="7">Openness to criticism and ideas</option> 
                      <option value="7">Communication</option> 
                      <option value="7">Teamwork</option> 
                      <option value="7">Persuasion</option> 
                    </select>
                  </div>
                  <div class="input-field col s12 m6">
                    <select>
                      <option value="" disabled selected>Choose your option</option>
                      <option value="1">Efficiency</option>
                      <option value="2">Honesty/Integrity</option>
                      <option value="3">Organization and Planning</option>
                      <option value="4">Aggressiveness</option>
                      <option value="5">Follow-through on commitments</option>
                      <option value="6">Intelligence</option>
                      <option value="7">Analytical Skills</option>
                      <option value="7">Attention to detail</option>  
                      <option value="7">Persistence</option> 
                      <option value="7">Proactivity</option> 
                      <option value="7">Ability to hire A Players (for managers)</option> 
                      <option value="7">Ability to develop people (for managers)</option> 
                      <option value="7">Flexibility/adaptability</option> 
                      <option value="7">Calm under pressure</option> 
                      <option value="7">Strategic thinking/visioning</option> 
                      <option value="7">Creativity/innovation</option> 
                      <option value="7">Enthusiasm</option>                   
                      <option value="7">Work ethic</option> 
                      <option value="7">High standards</option> 
                      <option value="7">Listening</option> 
                      <option value="7">Openness to criticism and ideas</option> 
                      <option value="7">Communication</option> 
                      <option value="7">Teamwork</option> 
                      <option value="7">Persuasion</option> 
                    </select>
                  </div>
                </div>
                <div class="row white martop-1 padall-1">
                  <h5 class="col s12">Custom Competencies (choose all applicable)</h5>
                  <div class="input-field col s12 m6">
                    <select>
                      <option value="" disabled selected>Choose your option</option>
                      <option value="1">Front End (JavaScript, HTML, CSS)</option>
                      <option value="2">Ember</option>
                      <option value="3">Angular</option>
                      <option value="4">PHP</option>
                      <option value="5">Objective-C</option>
                      <option value="6">C#</option>
                      <option value="7">Java</option>
                      <option value="7">Agile</option>  
                    </select>
                  </div>
                </div>
                <div class="row white martop-1 padall-1">
                  <h5 class="col s12">Outcomes</h5>
                  <div class="input-field col s12 martop-2">
                    <input id="outcome-1" type="text" class="validate" value="Maintain 60% test coverage on all code released.">
                    <label for="outcome-1">Outcome 1</label>
                  </div>
                  <div class="input-field col s12">
                    <h6>Outcome 1 Details</h6>
                    <textarea id="outcome-1-desc" class="edit text-small materialize-textarea">Tracking coverage with codecy, we can see how well your code is performing.</textarea>
                  </div>
                  <div class="input-field col s12 martop-2">
                    <input id="outcome-2" type="text" class="validate" value="Produce at a velocity that matches or exceeds the rest of the team.">
                    <label for="outcome-2">Outcome 2</label>
                  </div>
                  <div class="input-field col s12">
                    <h6>Outcome 2 Details</h6>
                    <textarea id="outcome-2-desc" class="edit text-small materialize-textarea">Building and sizing a backlog is very important as it is an indicator of your attention to detail.</textarea>
                  </div>
                  <div class="input-field col s12 martop-2">
                    <input id="outcome-3" type="text" class="validate" value="Add one coding language or framework to our toolkit per year.">
                    <label for="outcome-3">Outcome 3</label>
                  </div>
                  <div class="input-field col s12">
                    <h6>Outcome 3 Details</h6>
                    <textarea id="outcome-3-desc" class="edit text-small materialize-textarea">We already have PHP, Objective C, Ruby, HTML, CSS, and mySQL javascript languages.  As for frameworks, we have Angular, Ember, Drupal, sails, mongoDB, and Solr.</textarea>
                  </div>   
                  <div class="input-field col s12 martop-2">
                    <input id="outcome-4" type="text" class="validate" value="Improve adherence to sprint point commitments to 90%.">
                    <label for="outcome-4">Outcome 4</label>
                  </div>
                  <div class="input-field col s12">
                    <h6>Outcome 4 Details</h6>
                    <textarea id="outcome-4-desc" class="edit text-small materialize-textarea">Our teams are currently at about 85%.</textarea>
                  </div>                                   
                  <div class="col s12 martop-1">
                    <a href="#!" class="btn waves-effect waves-light fake-link"><i class="material-icons left">library_add</i>Add another Outcome</a>
                  </div>
                </div>
                <a href="/scorecard-developer.php" class="col btn">Update Scorecard</a>
              </form>
            </div>
          </div>
        </div> <!-- END Right Column -->
      </main>
        <?php include("components/global/footer.inc"); ?>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
