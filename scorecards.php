<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Scorecards</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="container">
      <div class="row">
        <?php include("components/global/side-upcoming.inc"); ?>
        <div class=" col s12 m9 border-left"> <!-- BEGIN Right Column -->
          <div class="section">
            <a href="dashboard.php">Dashboard</a> // Scorecards
          </div>
          <div class="section">
            <div class="row"> <!-- BEGIN Section Header -->
              <h4 class="left col">Scorecards</h4>
              <div class="right col">
                <a class="btn pop" href="/new-scorecard.php"><i class="fa fa-plus left"></i> New Scorecard</a>
              </div>
            </div> <!-- END Section Header -->
            <div class="filters">
              <h5>Refine Results</h5>
              <form>
                <div class="input-field col s12 m4">
                  <input placeholder="Name" id="name" type="text">
                  <label for="name">Scorecard Name</label>
                </div>
                <div class="col s6 m4">
                  <label for="from-date">Pick a From Date</label>
                  <input id="from-date" type="date" class="datepicker">
                </div>
                <div class="col s6 m4">
                  <label for="to-date">Pick a To Date</label>
                  <input id="to-date" type="date" class="datepicker">
                </div>
              </form>
            </div>
            
            <table class="bordered white inner"><!-- BEGIN Table -->
              <thead>
                <tr>
                  <th data-field="select"><input type="checkbox" id="all" class="filled-in" /><label for="all"></label></th>
                  <th data-field="name">Scorecard Name</th>
                  <th data-field="using">Active Candidates</th>
                  <th data-field="successful">Successful Hires</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td><input type="checkbox" class="filled-in" id="two" /><label for="two"></label></td>
                  <td><a href="/scorecard-developer.php">Developer</a></td>
                  <td>23</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td><input type="checkbox" class="filled-in" id="one" /><label for="one"></label></td>
                  <td><a class="fake-link" href="#!">Business Development</a></td>
                  <td>10</td>
                  <td>0</td>
                </tr>
                <tr>
                  <td><input type="checkbox" class="filled-in" id="three" /><label for="three"></label></td>
                  <td><a class="fake-link" href="#!">Lead Developer</a></td>
                  <td>18</td>
                  <td>1</td>
                </tr>
                <tr>
                  <td><input type="checkbox" class="filled-in" id="four" /><label for="four"></label></td>
                  <td><a class="fake-link" href="#!">Senior Developer</a></td>
                  <td>15</td>
                  <td>0</td>
                </tr>
                <tr>
                  <td><a href="#!" class="disabled tiny btn waves-effect waves-light">Archive Selected</a></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table> <!-- END Table -->
          </div>
        </div> <!-- END Right Column -->
      </main>
        <?php include("components/global/footer.inc"); ?>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
