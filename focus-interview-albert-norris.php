<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Focus Interview - Albert Norris</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="row">
      <div id="info-bar" class="col s12 m4 l3">
        <div class="sidebar">
          <div class="row">
            <div class="col m4">
              <img class="circle" src="/images/albert.jpg">
            </div>
            <div class="col m8">
              <h5 class="martop-0"><a href="/candidate-albert-norris.php">Albert Norris</a></h5>
              <ul>
                <li><strong>Scorecard:</strong> <a href="/scorecard-developer.php">Developer</a></li>
                <li><strong>Appt:</strong> 07/14/2015 (<a href="mailto:albert.norris@microsoft.com">email</a>) (<a href="tel://1-555-555-5555">phone</a>)</li>
              </ul>
            </div>
            <div class="col m12">
              <a class="waves-effect waves-light btn tiny modal-trigger" href="#refmodal">References</a> <!-- Reference Modal Trigger -->
            </div>
          </div>
          <div class="col s12">
            <h6 class="martop-0"><a href="https://www.linkedin.com/in/albertcmartin" target="_blank"><i class="fa fa-linkedin-square" style="font-size: 1.25rem;"></i></a> Job History</h6>
            <table class="no-bg">
              <thead>
                <tr>
                    <th data-field="id">Years</th>
                    <th data-field="name">Position</th>
                    <th data-field="price">Company</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>2010-2015</td>
                  <td>Senior Developer</td>
                  <td>Oracle</td>
                </tr>
                <tr>
                  <td>2007-2010</td>
                  <td>Junior Developer</td>
                  <td>Microsoft</td>
                </tr>
                <tr>
                  <td>2006-2007</td>
                  <td>Intern</td>
                  <td>Microsoft</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>     

      <div class="col s12 m8 l9 offset-l3 marbot-5">
        <div class="section">
          <a href="/dashboard.php">Dashboard</a> // <a href="/interviews.php">Interviews</a> // Focus - Albert Norris
        </div>
        <div class="section">
          <div class="row">
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div class="scrollspy collapsible-header"><i class="material-icons">my_location</i>Focus Questions</div>
                <div class="collapsible-body padall-1">
                  <h5 class="marbot-1">The purpose of this interview is to talk about your prior SaaS application work. <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="THIS IS NOT A GOTCHA INTERVIEW!  Be friendly and conversational.  Any team members added to this interview need to keep in mind that this candidate is really in a final check."><i class="material-icons">info_outline</i></a></h5>
                  <form>
                    <textarea class="edit text-small" name="purpose" rows="6"></textarea>
                  </form> 
                  <h5 class="marbot-1">What are your biggest accomplishments in this area during your career? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Focus on competencies and this outcome."><i class="material-icons">info_outline</i></a></h5>
                  <form>
                    <textarea class="edit text-small" name="accomplishments" rows="6"></textarea>
                  </form>
                  <h5 class="marbot-1">What are your insights into your biggest mistakes and lessons learned in this area? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Focus on competencies and this outcome."><i class="material-icons">info_outline</i></a></h5>
                  <form>
                    <textarea class="edit text-small" name="mistakes" rows="6"></textarea>
                  </form> 
                </div>
              </li>
              <li>
                <div class="scrollspy collapsible-header"><i class="material-icons">hearing</i>Outcomes</div>
                <div class="collapsible-body padall-1">
                  <table class="bordered white inner-lists"><!-- BEGIN Table -->
                    <thead>
                      <tr>
                        <th class="col-rating">Rating</th>
                        <th>Outcome</th>
                        <th>Description</th>
                        <th class="col-comments">Comments</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <input name="efficiency" type="radio" id="eff-a" /><label for="eff-a" class="marright-2">A</label> <input name="efficiency" type="radio" id="eff-b" /><label for="eff-b" class="marright-2">B</label> <input name="efficiency" type="radio" id="eff-c" /><label for="eff-c">C</label>
                        </td>
                        <td>
                          <p>Maintain 60% test coverage on all code released.</p>
                        </td>
                        <td>
                          <ul>
                            <li>Tracking coverage with codecy, we can see how well your code is performing.</li>
                          </ul>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <input name="enthusiasm" type="radio" id="enth-a" /><label for="enth-a" class="marright-2">A</label> <input name="enthusiasm" type="radio" id="enth-b" /><label for="enth-b" class="marright-2">B</label> <input name="enthusiasm" type="radio" id="enth-c" /><label for="enth-c">C</label>
                        </td>
                        <td>
                          <p>Build enterprise-quality Saas applications for fortune 500 companies that drive 400% ROI over 4 years.</p>
                        </td>
                        <td>
                          <ul>
                            <li>Key to accomplishing a great ROI is a consciousness of business value to the client.  Encourage the client toward prioritizing features that will quickly provide returns.</li>
                            <li>Make sure that the backlog and story sizing drive smart business decisions by being accurate in terms of velocity and committed points.</li>
                          </ul>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>                        
                      </tr>
                      <tr>
                        <td>
                          <input name="worke" type="radio" id="worke-a" /><label for="worke-a" class="marright-2">A</label> <input name="worke" type="radio" id="worke-b" /><label for="worke-b" class="marright-2">B</label> <input name="worke" type="radio" id="worke-c" /><label for="worke-c">C</label>
                        </td>
                        <td>
                          <p>Add one coding language or framework to our toolkit per year.</p>
                        </td>
                        <td>
                          <ul>
                            <li>We already have PHP, Objective C, Ruby, HTML, CSS, and mySQL javascript languages.  As for frameworks, we have Angular, Ember, Drupal, sails, mongoDB, and Solr.</li>
                          </ul>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>                        
                      </tr>
                      <tr>
                        <td>
                          <input name="aggressiveness" type="radio" id="agg-a" /><label for="agg-a" class="marright-2">A</label> <input name="aggressiveness" type="radio" id="agg-b" /><label for="agg-b" class="marright-2">B</label> <input name="aggressiveness" type="radio" id="agg-c" /><label for="agg-c">C</label>
                        </td>
                        <td>
                          <p>Improve adherence to sprint point commitments to 90%.</p>
                        </td>
                        <td>
                          <ul>
                            <li>Our teams are currently at about 85%.</li>
                          </ul>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>                        
                      </tr>
                    </tbody>
                  </table>
                </div>
              </li>
              <li>
                <div class="scrollspy collapsible-header"><i class="material-icons">hearing</i>Competencies</div>
                <div class="collapsible-body padall-1">
                  <table class="bordered white inner-lists"><!-- BEGIN Table -->
                    <thead>
                      <tr>
                        <th class="col-rating">Rating</th>
                        <th>Competency</th>
                        <th>Description</th>
                        <th class="col-comments">Comments</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <input name="efficiency" type="radio" id="eff-a" /><label for="eff-a" class="marright-2">A</label> <input name="efficiency" type="radio" id="eff-b" /><label for="eff-b" class="marright-2">B</label> <input name="efficiency" type="radio" id="eff-c" /><label for="eff-c">C</label>
                        </td>
                        <td>
                          <b>Efficiency</b>
                        </td>
                        <td>
                          <p>Able to produce significant output with minimal wasted effort.</p>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <input name="enthusiasm" type="radio" id="enth-a" /><label for="enth-a" class="marright-2">A</label> <input name="enthusiasm" type="radio" id="enth-b" /><label for="enth-b" class="marright-2">B</label> <input name="enthusiasm" type="radio" id="enth-c" /><label for="enth-c">C</label>
                        </td>
                        <td>
                          <b>Enthusiasm</b>
                        </td>
                        <td>
                          <p>Exhibits passion and excitement over work. Has a can-do attitude.</p>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>                        
                      </tr>
                      <tr>
                        <td>
                          <input name="worke" type="radio" id="worke-a" /><label for="worke-a" class="marright-2">A</label> <input name="worke" type="radio" id="worke-b" /><label for="worke-b" class="marright-2">B</label> <input name="worke" type="radio" id="worke-c" /><label for="worke-c">C</label>
                        </td>
                        <td>
                          <b>Work Ethic</b>
                        </td>
                        <td>
                          <p>Possesses a strong willingness to work hard and sometimes long hours to get the job done. Has a track record of working hard.</p>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>                        
                      </tr>
                      <tr>
                        <td>
                          <input name="aggressiveness" type="radio" id="agg-a" /><label for="agg-a" class="marright-2">A</label> <input name="aggressiveness" type="radio" id="agg-b" /><label for="agg-b" class="marright-2">B</label> <input name="aggressiveness" type="radio" id="agg-c" /><label for="agg-c">C</label>
                        </td>
                        <td>
                          <b>Aggressiveness</b>
                        </td>
                        <td>
                          <p>Moves quickly and takes a forceful stand without being overly abrasive.</p>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>                        
                      </tr>
                      <tr>
                        <td>
                          <input name="creative" type="radio" id="create-a" /><label for="create-a" class="marright-2">A</label> <input name="creative" type="radio" id="create-b" /><label for="create-b" class="marright-2">B</label> <input name="creative" type="radio" id="create-c" /><label for="create-c">C</label>
                        </td>
                        <td>
                          <b>Creativity/Innovation</b>
                        </td>
                        <td>
                          <p>Generates new and innovative approaches to problems.</p>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>                        
                      </tr>
                      <tr>
                        <td>
                          <input name="standards" type="radio" id="stand-a" /><label for="stand-a" class="marright-2">A</label> <input name="standards" type="radio" id="stand-b" /><label for="stand-b" class="marright-2">B</label> <input name="standards" type="radio" id="stand-c" /><label for="stand-c">C</label>
                        </td>
                        <td>
                          <b>High Standards</b>
                        </td>
                        <td>
                          <p>Expects personal performance and team performance to be nothing short of the best.</p>
                        </td>
                        <td>
                          <form>
                            <textarea class="edit-comments text-small" name="test"></textarea>
                          </form>
                        </td>                        
                      </tr>
                    </tbody>
                  </table>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div id="general" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">General Notes</h5>
            <form>
              <textarea class="edit text-small" name="test"></textarea>
            </form> 
          </div>
        </div>
        <div id="general" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">Candidate's Focus Interview Checklist and Score <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Only those who have both checks and a score of 90+ go on to the reference interview rounds."><i class="material-icons">info_outline</i></a></h5>
            <form>
              <div class='col s12 white z-depth-1'>
                <div class="input-field col s6 m7 border-right">
                  <input type="checkbox" class="filled-in" id="push-pull" />
                  <label for="push-pull">Candidate fits our cultural competencies.</label>
                  <input type="checkbox" class="filled-in" id="references" />
                  <label for="references">Any additional references recorded <a class="modal-trigger" href="#refmodal">here</a> with correct spelling.</label>
                </div>
                <div class="input-field col s6 m2 martop-2">
                  <input placeholder="1-100" id="score" type="number" min="1" max="100">
                  <label for="score">Focus Score</label>
                </div>
                <div class="input-field col s6 m3 martop-2">
                  <a class="btn disabled">Promote to References</a>
                </div>
              </div>
            </form> 
          </div>
        </div>
        <div class="fixed-action-btn" style="bottom: 24px; left: 24px;">
          <h6 class="good-color">last autosave: 12:43am</h6>
        </div>
      </div>
      
      <!-- Reference Modal Body -->
      <div id="refmodal" class="modal bottom-sheet">
        <div class="modal-content">
          <h5>References</h5>
          <div class="col s12 m6 border-right">
            <ul>
              <li>John Franklin (<a href="#!" class="fake-link">edit</a>)</li>
              <li>Ben Jackson (<a href="#!" class="fake-link">edit</a>)</li>
              <li>Sally Loyd (<a href="#!" class="fake-link">edit</a>)</li>
            </ul>
          </div>
          <div class="col s12 m6">
            <form>
              <div class="input-field col s12">
                <input placeholder="Reference Name" id="name" type="text">
                <label for="name">Add a Reference</label>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer no-bg">
          <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Save/Close</a>
        </div>
      </div>
        <?php include("components/global/bottom-competencies-modal.inc"); ?>
        <?php include("components/global/bottom-outcomes-modal.inc"); ?>
    </main>
    <?php include("components/global/footer.inc"); ?>
    <?php include("components/global/foot.inc"); ?>
  </body>
</html>
