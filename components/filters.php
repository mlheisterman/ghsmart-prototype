<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Test Page</title>
    <!-- Grab the prettify script to output HTML Code -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?linenums=false"></script>

    <?php include("global/head.inc"); ?>
  </head>
  <body>      
    <div class="row">
      <div class="large-12 columns">
        <a href="patterns.php">&laquo; Go Back</a>
      </div>
    </div>
    
    <div class="row">
      <div class="small-12 columns">
        <div class="island marbot-5">
          <div class="island-header">
            <h1>Filters</h1>
          </div>
          <div class="island-contents">
            
          </div>
        </div>
      </div>

      <div class="filters">
        <h4><a href="#" class="expand-collapse-trigger"><i class="fa fa-plus textXSmall"></i> Filter Results</a></h4>

        <div class="small-12 medium-6 large-2 columns">
          <label for="filter-startDate">Date From</label>
          <input type="text" id="filter-startDate" class="datepicker" />
        </div>

        <div class="small-12 medium-6 large-2 columns">
          <label for="filter-endDate">To</label>
          <input type="text" id="filter-endDate" class="datepicker" />
        </div>

        <div class="small-12 medium-6 large-2 columns">
          <label for="filter-one">Filter One</label>
          <select class="multiselect">
            <option></option>
            <option>Option One</option>
            <option>Option Two</option>
            <option>Option Three</option>
            <option>Option Four</option>
          </select>
        </div>

        <div class="small-12 medium-6 large-3 columns">
          <label for="filter-two">Filter Two</label>
          <select class="multiselect">
            <option></option>
            <option>Option One</option>
            <option>Option Two</option>
            <option>Option Three</option>
            <option>Option Four</option>
          </select>
        </div>

        <div class="small-12 medium-6 large-3 columns">
          <label for="filter-three">Filter Three</label>
          <select class="multiselect">
            <option></option>
            <option>Option One</option>
            <option>Option Two</option>
            <option>Option Three</option>
            <option>Option Four</option>
          </select>
        </div>

        <div class="small-12 medium-6 large-3 columns noPadRight">
          <label for="status">Show only</label>
          <select>
            <option>Status One</option>
            <option>Status Two</option>
            <option>Status Three</option>
            <option>Status Four</option>
          </select>
        </div>

        <div class="small-12 medium-6 large-6 columns noPadRight">
          <a href="" class="right button small">Filter</a>
        </div>
      </div>
    </div>

    

    <div class="row">
  
</div>
<script src="../js/modernizr.js"></script>
<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="../js/jquery.inputmask.js"></script>
<script src="../js/jquery.inputmask.date.extensions.js"></script>
<!-- <script src="../js/payment.js"></script> -->
<script src="../js/app.min.js"></script>
<script src="../js/interface.js"></script>
<script src="../js/d3.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

  </body>
</html>
