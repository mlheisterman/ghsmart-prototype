<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Test Page</title>
    
    <?php include("global/head.inc"); ?>
    <a href="/">&laquo; Home</a>

    <style type="text/css">
      body {
        padding: 40px;
      }

      .atom {
        margin-bottom: 5em;
      }

      .atom label:first-child {
        color: #666;
        border-bottom: 1px solid #999;
        display: block;
        font-size: 11px;
        text-transform: uppercase;
        font-weight: bold;
        margin-bottom: 10px;
        padding-bottom: 4px;
      }

      small.d-block {
        color: #666;
        border-bottom: 1px dashed #999;
        margin-bottom: 10px;
      }

    </style>
  </head>
  <body>
    <nav>
      <ul class="inline-list">
        <li><a href="global.php">Global</a></li>
        <li><a href="atoms.php"><strong>Atoms</strong></a></li>
        <li><a href="molecules.php">Molecules</a></li>
        <li><a href="organisms.php">Organisms</a></li>
      </ul>
    </nav>

    <div class="clearfix martop-5"></div>

    <div class="atom">
      <label>Headings</label>
      <h1>Heading 1</h1>
      <h2>Heading 2</h1>
      <h3>Heading 3</h3>
      <h4>Heading 4</h4>
      <h5>Heading 5</h5>
      <h6>Heading 6</h6>
    </div>

    <div class="atom">
      <label>Paragraph</label>
      <p>Mr. Fogg and his servant went ashore at Aden to have the passport again visaed; Fix, unobserved, followed them.  The visa procured, Mr. Fogg returned on board to resume his former habits; while Passepartout, according to custom, sauntered about among the mixed population of Somalis, Banyans, Parsees, Jews, Arabs, and Europeans who comprise the twenty-five thousand inhabitants of Aden.  He gazed with wonder upon the fortifications which make this place the Gibraltar of the Indian Ocean, and the vast cisterns where the English engineers were still at work, two thousand years after the engineers of Solomon.</p>
    </div>

    <div class="atom">
      <label>Blockquote</label>
      <blockquote>
        The quick brown fox <em>jumped</em> over the lazy dog
      </blockquote>
    </div>

    <div class="atom">
      <label>Inline Elements</label>
      
      <p><a>link</a></p>
      <p><strong>strong</strong></p>
      <p><em>emphasis</em></p>
      <p><u>underline</u></p>
      <p><del>deleted text</del> and <ins>inserted text</ins></p>
      <p>superscript<sup>&reg;</sup></p>
      <p>subscript for things like H<sub>2</sub>O</p>
      <p><small>Small text</small></p>
      <p>abbreviation for things like <abbr>HTML</abbr></p>
      <p>Keyboard input like <kbd>cmd</kbd></p>
      <p>Inline quotation: <q>All the king's horses and..</q></p>
      <p><cite>Citation</cite></p>
      For definitions, use the <dfn>definition element</dfn>
      <p>Here is <mark>mark (highlighted) text</mark></p>
      <p>inline <code>code</code></p>
      <p><samp>sample output</samp></p>
      <p>variable element, <var>x = y</var></p>
    </div>

    <div class="atom">
      <label>Time</label>
      
      <time datetime="2015-04-06T12:62+00:00">Apr 6, 2015 - 8pm</time>
    </div>

    <div class="atom">
      <label>Preformatted</label>
      
      <pre>
P R E F O R M A T T E D T E X T
! " # $ % & ' ( ) * + , - . /
0 1 2 3 4 5 6 7 8 9 : ; < = > ?
@ A B C D E F G H I J K L M N O
P Q R S T U V W X Y Z [ \ ] ^ _
` a b c d e f g h i j k l m n o
p q r s t u v w x y z { | } ~
      </pre>
    </div>

    <div class="atom">
      <label>hr</label>
      <hr>
    </div>

    <div class="atom">
      <label>Strikethrough (strike)</label>
      <p><strike>This is some strikethrough text</strike></p>
    </div>

    <div class="atom">
      <label>Lists</label>

      <small class="d-block martop-2">Unordered List</small>
      <ul>
        <li>Top level list item</li>
        <li>Top level list item
          <ul>
            <li>2nd level list item</li>
            <li>2nd level list item</li>
            <li>2nd level list item</li>
          </ul>
        </li>
        <li>Top level list item</li>
        <li>Top level list item
          <ul>
            <li>2nd level list item
              <ul>
                <li>3rd level list item</li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>

      <small class="d-block martop-2">Ordered List</small>
      <ol>
        <li>Top level list item</li>
        <li>Top level list item
          <ol>
            <li>2nd level list item</li>
            <li>2nd level list item</li>
            <li>2nd level list item</li>
          </ol>
        </li>
        <li>Top level list item</li>
        <li>Top level list item
          <ol>
            <li>2nd level list item
              <ol>
                <li>3rd level list item</li>
              </ol>
            </li>
          </ol>
        </li>
      </ol>

      <small class="d-block martop-2">Definition List</small>
      <dl>
        <dt>Agile</dt>
        <dd>Characterized by quickness, lightness, and ease of movement; nimble.</dd>
        <dt>Scrum</dt>
        <dd>An iterative and incremental agile software development methodology for managing product development.</dd>
      </dl>
    </div>

    <div class="atom">
      <label>Images</label>
      <small class="d-block martop-2">Logo</small>
      <img src="http://placehold.it/200x75" alt="Logo">

      <small class="d-block martop-2">Landscape 4x3</small>
      <img src="http://placehold.it/400x300" alt="Landscape 4x3">

      <small class="d-block martop-2">Landscape 16x9</small>
      <img src="http://placehold.it/1600x900" alt="Landscape 16x9">

      <small class="d-block martop-2">Square</small>
      <img src="http://placehold.it/200x200" alt="Square">

      <small class="d-block martop-2">Avatar</small>
      <img src="http://placehold.it/80x80" class="avatar" alt="Alias">

      <small class="d-block martop-2">Icon Set</small>
      <i class="fa fa-star"></i><i class="fa fa-search"></i><i class="fa fa-facebook"></i><i class="fa fa-arrow-up"></i>

      <small class="d-block martop-2">Loading Icon</small>
      <i class="fa fa-circle-o-notch fa-spin"></i>

      <small class="d-block martop-2">Favicon</small>
      <img src="http://placehold.it/16x16">
      <img src="http://placehold.it/32x32">
    </div>

    <div class="atom">
      <label>Forms</label>

      <small class="d-block martop-2">Text Input</small>
      <input type="text" placeholder="Enter some text">

      <small class="d-block martop-2">Password</small>
      <input type="password" placeholder="Type your password">

      <small class="d-block martop-2">Web Address</small>
      <input type="url" placeholder="http://praxent.com">

      <small class="d-block martop-2">Email Address</small>
      <input type="email" placeholder="Enter your email">

      <small class="d-block martop-2">Search</small>
      <input type="search" placeholder="Search something">

      <small class="d-block martop-2">Number</small>
      <input type="number" pattern="[0-9]" placeholder="Numbers only">

      <small class="d-block martop-2">Select</small>
      <select name="state"> 
        <option value="AL" selected="selected">Alabama</option> 
        <option value="AK">Alaska</option> 
        <option value="AZ">Arizona</option> 
        <option value="AR">Arkansas</option> 
        <option value="CA">California</option> 
        <option value="CO">Colorado</option> 
        <option value="CT">Connecticut</option> 
        <option value="DE">Delaware</option> 
        <option value="DC">District Of Columbia</option> 
        <option value="FL">Florida</option> 
        <option value="GA">Georgia</option> 
        <option value="HI">Hawaii</option> 
        <option value="ID">Idaho</option> 
        <option value="IL">Illinois</option> 
        <option value="IN">Indiana</option> 
        <option value="IA">Iowa</option> 
        <option value="KS">Kansas</option> 
        <option value="KY">Kentucky</option> 
        <option value="LA">Louisiana</option> 
        <option value="ME">Maine</option> 
        <option value="MD">Maryland</option> 
        <option value="MA">Massachusetts</option> 
        <option value="MI">Michigan</option> 
        <option value="MN">Minnesota</option> 
        <option value="MS">Mississippi</option> 
        <option value="MO">Missouri</option> 
        <option value="MT">Montana</option> 
        <option value="NE">Nebraska</option> 
        <option value="NV">Nevada</option> 
        <option value="NH">New Hampshire</option> 
        <option value="NJ">New Jersey</option> 
        <option value="NM">New Mexico</option> 
        <option value="NY">New York</option> 
        <option value="NC">North Carolina</option> 
        <option value="ND">North Dakota</option> 
        <option value="OH">Ohio</option> 
        <option value="OK">Oklahoma</option> 
        <option value="OR">Oregon</option> 
        <option value="PA">Pennsylvania</option> 
        <option value="RI">Rhode Island</option> 
        <option value="SC">South Carolina</option> 
        <option value="SD">South Dakota</option> 
        <option value="TN">Tennessee</option> 
        <option value="TX">Texas</option> 
        <option value="UT">Utah</option> 
        <option value="VT">Vermont</option> 
        <option value="VA">Virginia</option> 
        <option value="WA">Washington</option> 
        <option value="WV">West Virginia</option> 
        <option value="WI">Wisconsin</option> 
        <option value="WY">Wyoming</option>
      </select>

      <small class="d-block martop-2">Checkbox</small>
      <input type="checkbox" id="option1" name="checkbox"><label for="option1">Option 1</label>
      <input type="checkbox" id="option2" name="checkbox"><label for="option2">Option 2</label>

      <small class="d-block martop-2">Radio Buttons</small>
      <input type="radio" id="radio1" name="radio"><label for="radio1">Radio 1</label>
      <input type="radio" id="radio2" name="radio"><label for="radio2">Radio 2</label>

      <small class="d-block martop-2">Textarea</small>
      <textarea placholder="Enter your message here"></textarea>

      <small class="d-block martop-2">Success</small>
      <input type="text" class="success">

      <small class="d-block martop-2">Error</small>
      <input type="text" class="error">
    </div>

    <div class="atom">
      <label>Buttons</label>

      <button>Button</button>
      <button class="button alt">Alternate Button</button>
      <button class="button subtle">Subtle Button</button>
      <button class="button disabled">Disabled Button</button>
    </div>

    <div class="atom">
      <label>Table</label>
      <table>
        <thead>
          <tr>
            <th>Heading 1</th>
            <th>Heading 2</th>
            <th>Heading 3</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Cell 1</td>
            <td>Cell 2</td>
            <td>Cell 3</td>
          </tr>
          <tr>
            <td>Cell 4</td>
            <td>Cell 5</td>
            <td>Cell 6</td>
          </tr>
          <tr>
            <td>Cell 7</td>
            <td>Cell 8</td>
            <td>Cell 9</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td>Foot 1</td>
            <td>Foot 2</td>
            <td>Foot 3</td>
          </tr>
        </tfoot>
      </table>
    </div>
  </body>
</html>