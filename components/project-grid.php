<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Test Page</title>
    <!-- Grab the prettify script to output HTML Code -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?linenums=false"></script>

    <?php include("includes/head.inc"); ?>
  </head>
  <body>      
    <div class="row">
      <div class="large-12 columns">
        <a href="/components.php">&laquo; Go Back</a>
      </div>
    </div>
    
    <div class="row">
      <div class="small-12 columns">
        <div class="island marbot-5">
          <div class="island-header">
            <h1>Project Grid</h1>
          </div>
          <div class="island-contents">
            
          </div>
        </div>
      </div>

      <div class="project-grid">
        <div class="project">
          <img src="http://placehold.it/800x800">
          <h4 class="title">Project Title</h4>
        </div>

        <div class="project">
          <img src="http://placehold.it/800x800">
          <h4 class="title">Project Title</h4>
        </div>

        <div class="project">
          <img src="http://placehold.it/800x800">
          <h4 class="title">Project Title</h4>
        </div>
      </div>

    

    <div class="row">
  
</div>
<script src="../js/modernizr.js"></script>
<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="../js/jquery.inputmask.js"></script>
<script src="../js/jquery.inputmask.date.extensions.js"></script>
<!-- <script src="../js/payment.js"></script> -->
<script src="../js/app.min.js"></script>
<script src="../js/interface.js"></script>
<script src="../js/d3.min.js"></script>
<script type="text/javascript" src="../js/select2.min.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

  </body>
</html>
