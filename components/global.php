<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Test Page</title>

    <?php include("global/head.inc"); ?>
    <a href="/">&laquo; Home</a>

    <style type="text/css">
      body {
        padding: 40px;
      }

      .global {
        clear: both;
        margin-bottom: 5em;
        overflow: hidden;
      }

      .global label:first-child {
        color: #666;
        border-bottom: 1px solid #999;
        display: block;
        font-size: 11px;
        text-transform: uppercase;
        font-weight: bold;
        margin-bottom: 10px;
        padding-bottom: 4px;
      }

      small.d-block {
        color: #666;
        border-bottom: 1px dashed #999;
        margin-bottom: 10px;
      }

    </style>
  </head>
  <body>
    <nav>
      <ul class="inline-list">
        <li><a href="global.php"><strong>Global</strong></a></li>
        <li><a href="atoms.php">Atoms</a></li>
        <li><a href="molecules.php">Molecules</a></li>
        <li><a href="organisms.php">Organisms</a></li>
      </ul>
    </nav>

    <div class="clearfix martop-5"></div>

    <div class="global">
      <label>Colors</label>
      <ul class="inline-list no-bullets swatches text-center">
        <li>
          <span class="swatch bgc-primary"></span>
          <small>#000000<br>Usage</small>
        </li>
        <li>
          <span class="swatch bgc-secondary"></span>
          <small>#000000<br>Usage</small>
        </li>
        <li>
          <span class="swatch bgc-tertiary"></span>
          <small>#000000<br>Usage</small>
        </li>
        <li>
          <span class="swatch bgc-light"></span>
          <small>#000000<br>Usage</small>
        </li>
        <li>
          <span class="swatch bgc-dark"></span>
          <small>#000000<br>Body copy and headlines</small>
        </li>
      </ul>
    </div>

    <div class="global">
      <label>Font Stacks</label>
      <p><strong>Headings</strong>: Helvetica Neue, Helvetica, Arial, sans-serif</p>
      <p><strong>Body</strong>: Helvetica Neue, Helvetica, Arial, sans-serif</p>
    </div>

    <div class="global">
      <label>Scale</label>
      <pre>
// Based on perfect fourth ratio @16px base

$scale-3: .422rem;  // 6px
$scale-2: .563rem;  // 9px
$scale-1: .75rem;   // 12px
$scale-base: 1rem;  // 16px
$scale1: 1.333rem;  // 21px
$scale2: 1.777rem;  // 28px
$scale3: 2.369rem;  // 37px
$scale4: 3.157rem;  // 50px
$scale5: 4.209rem;  // 67px
$scale6: 5.61rem;   // 89px
$scale7: 7.478rem;  // 119px
$scale8: 9.969rem;  // 159px
$scale9: 13.288rem; // 212px
      </pre>
    </div>

  </body>
</html>