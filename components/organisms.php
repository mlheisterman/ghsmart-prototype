<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Test Page</title>

    <?php include("global/head.inc"); ?>
    <a href="/">&laquo; Home</a>

    <style type="text/css">
      body {
        padding: 40px;
      }

      .organism {
        clear: both;
        margin-bottom: 5em;
        overflow: hidden;
      }

      .organism label:first-child {
        color: #666;
        border-bottom: 1px solid #999;
        display: block;
        font-size: 11px;
        text-transform: uppercase;
        font-weight: bold;
        margin-bottom: 10px;
        padding-bottom: 4px;
      }

      small.d-block {
        color: #666;
        border-bottom: 1px dashed #999;
        margin-bottom: 10px;
      }

    </style>
  </head>
  <body>
    <nav>
      <ul class="inline-list">
        <li><a href="global.php">Global</a></li>
        <li><a href="atoms.php">Atoms</a></li>
        <li><a href="molecules.php">Molecules</a></li>
        <li><a href="organisms.php"><strong>Organisms</strong></a></li>
      </ul>
    </nav>

    <div class="clearfix martop-5"></div>

    <div class="organism">
      <label>Header</label>

      <header class="global-header">
        <img src="http://placehold.it/200x75" class="logo">
        <nav class="primary">
          <ul class="inline-list">
            <li><a href="">Home</a></li>
            <li><a href="">About</a></li>
            <li><a href="">Services</a></li>
            <li><a href="">Products</a></li>
            <li><a href="">Contact</a></li>
          </ul>
        </nav>
        <div class="form search">
          <input type="text" class="inline" />
          <button class="inline text-medium"><i class="fa fa-search"></i></button>
        </div>
      </header>
    </div>

    <div class="organism">
      <label>Footer</label>

      <footer class="global-footer">
        <nav class="footer-nav">
          <ul class="inline-list">
            <li><a href="">Home</a></li>
            <li><a href="">About</a></li>
            <li><a href="">Services</a></li>
            <li><a href="">Products</a></li>
            <li><a href="">Contact</a></li>
          </ul>
        </nav>
        <p>&copy; 2015 SomeWebsite.com, All rights reserved</p>
      </footer>
    </div>

  </body>
</html>