<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Core</title>
    <!-- Grab the prettify script to output HTML Code -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?linenums=false"></script>

    <?php include("global/head.inc"); ?>
  </head>
  <body>

    <!-- Login Modal -->
    <div id="login-modal" class="reveal-modal" data-reveal>
      <div class="small-12 medium-5 large-4 small-centered columns">
        <form class="login" data-abide="ajax"><img src="http://placehold.it/300x125.png" alt="Logo" id="app-logo">
          <input type="text" id="username" placeholder="username" required pattern="alpha_numeric">
          <small class="error">Please enter your username.</small>

          <input type="password" id="password" placeholder="password" required>
          <small class="error">Please enter your password.</small>

          <a href="" class="button expand">Log In</a>
          <p class="textSmall"><a href="" >Forgot your password?</a> It happens.</p>
        </form>
      </div>
    </div>


    <!-- Impersonation Modal -->
    <div id="impersonation-modal" class="reveal-modal" data-reveal>
      <div class="impersonation">
        <p>You are Logged in as <em>Username</em> | <a href="">Logout</a></p>
      </div>
    </div>

    
    <!-- Messages Modal -->
    <div id="messages-modal" class="reveal-modal" data-reveal>
      <div class="message">
        <p>This is a default message, but you can change it by adding classes like: success, error, note, and warning.</p>
      </div>

      <div class="message success">
        <p>This is a success message.</p>
      </div>

      <div class="message alert">
        <p>This is an alert.</p>
      </div>

      <div class="message note">
        <p>This is a note.</p>
      </div>

      <div class="message warning">
        <p>This is a warning.</p>
      </div>
    </div>

    <!-- Supernav Modal -->
    <div id="supernav-modal" class="reveal-modal" data-reveal>
      <h5 class="supernav padTop-5 large-text-right">Logged in as <em>Firstname Lastname</em> <span class="glyphicons chevron-right textXSmall"></span><a href="">My&nbsp;Account</a><a href="/">Sign&nbsp;Out</a></h5>
    </div>

    <!-- Search Modal -->
    <div id="search-modal" class="reveal-modal" data-reveal>
      <div class="search">
        <input type="text" class="inline" />
        <button class="inline text-medium"><i class="fa fa-search"></i></button>
      </div>
    </div>

    <!-- Account Settings Modal -->
    <div id="account-settings-modal" class="reveal-modal" data-reveal>
      <form class="account-settings">
        <div class="panel hide-overflow">
          <a class="right button">Edit</a>
          <h3><strong>Name</strong>: Nick Comito</h3>
        </div>

        <div class="panel hide-overflow editing">
          <div class="small-3 small-push-9 columns">
            <a class="right button">Save</a>
          </div>
          <div class="small-9 small-pull-3 columns">
            <input type="text" value="Nick Comito">
          </div>
        </div>

        <div class="panel hide-overflow">
          <div class="small-3 small-push-9 columns">
            <a class="right button">Edit</a>
          </div>
          <div class="small-9 small-pull-3 columns">
            <h3><strong>Phone</strong>: (512) 553-6830</h3>
          </div>
        </div>

        <div class="panel hide-overflow">
          <div class="small-3 small-push-9 columns">
            <a class="right button">Edit</a>
          </div>
          <div class="small-9 small-pull-3 columns">
            <h3><strong>Email</strong>: nick@astonishdesign.com</h3>
          </div>
        </div>

        <div class="panel hide-overflow">
          <div class="small-3 small-push-9 columns">
            <div class="right button-group">
              <a class="button">View</a>
              <a class="button">Edit</a>
            </div>
          </div>
          <div class="small-9 small-pull-3 columns">
            <h3><strong>Password</strong>: *******</h3>
          </div>
        </div>
      </form>
    </div>

    <!-- Billboard Modal -->
    <div id="billboard-modal" class="reveal-modal" data-reveal>
      <div class="billboard">
        <div class="small-12 medium-7 large-8 columns">
          <h1 class="marbot-2">Everything should be made as simple as possible, but not simpler.</h1>
          <a class="button padall-1 padleft-5 padright-5">Click Me</a>
        </div>
        <div class="small-12 medium-5 large-4 columns">
          <img src="http://placehold.it/500x400">
        </div>
      </div>
    </div>

    <!-- Contextual Steps Modal -->
    <div id="contextual-steps-modal" class="reveal-modal" data-reveal>
      <!-- Steps Range from 3 to 6 -->
      <ul class="contextual-steps four text-center">
        <li class="step">
          <div class="step-number">1</div>
          <div class="step-text">Whiteboards</div>
        </li>
        <li class="step active">
          <div class="step-number">2</div>
          <div class="step-text">ClickModel</div>
        </li>
        <li class="step">
          <div class="step-number">3</div>
          <div class="step-text">UI</div>
        </li>
        <li class="step">
          <div class="step-number">4</div>
          <div class="step-text">Software</div>
        </li>
      </ul>
    </div>

    <!-- Responsive Table Modal -->
    <div id="responsive-table-modal" class="reveal-modal" data-reveal>
      <table class="responsive-table">
        <thead>
          <tr>
            <th class="data-heading">Date</th>
            <th class="data-heading">Quantity</th>
            <th class="data-heading">Rate</th>
            <th class="data-heading">Status</th>
            <th class="data-heading">Total</th>
          </tr>
        </thead>

        <tbody>
          <tr class="odd">
            <td>10/12/2014</td>
            <td>21</td>
            <td>24%</td>
            <td>Open</td>
            <td>54</td>
          </tr>

          <tr class="even">
            <td>1/12/2015</td>
            <td>41</td>
            <td>44%</td>
            <td>Closed</td>
            <td>104</td>
          </tr>

          <tr class="odd">
            <td>12/18/2014</td>
            <td>29</td>
            <td>26%</td>
            <td>Closed</td>
            <td>59</td>
          </tr>
        </tbody>
      </table>
    </div>

    <!-- CTAs Modal -->
    <div id="ctas-modal" class="reveal-modal" data-reveal>
      <div class="small-5 columns">
        <div class="cta">
          <div class="cta-contents">
            <h2>Call To Action</h2>
            <p>Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <a href="#" class="button expand">Click Here</a>
          </div>
        </div>
      </div>

      <div class="small-7 columns">
        <div class="cta">
          <img src="http://placehold.it/800x300">
          <div class="cta-contents">
            <h2>Call To Action</h2>
            <p>Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <a href="#" class="button expand">Click Here</a>
          </div>
        </div>
      </div>

      <div class="small-3 columns">
        <div class="cta">
          <img src="http://placehold.it/200x400">
          <div class="cta-contents">
            <h2>Call To Action</h2>
            <p>Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <a href="#" class="button expand">More</a>
          </div>
        </div>
      </div>

      <div class="small-4 columns">
        <div class="cta">
          <div class="cta-contents">
            <img src="http://placehold.it/400x400" />
            <h2>Call To Action</h2>
            <p>Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <a href="#" class="button expand">Click Here</a>
          </div>
        </div>
      </div>

      <div class="small-5 columns">
        <div class="cta">
          <div class="cta-contents">
            <img src="http://placehold.it/120x120" class="left" />
            <h2>Call To Action</h2>
            <p>Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <a href="#" class="button">Click Here</a>
          </div>
        </div>
      </div>

      <div class="small-5 columns">
        <div class="cta">
          <div class="cta-contents">
            <img src="http://placehold.it/120x120" class="right" />
            <h2>Call To Action</h2>
            <p>Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <a href="#" class="button">Click Here</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Shopping Cart Modal -->
    <div id="shopping-cart-modal" class="reveal-modal" data-reveal>
      <div class="shopping-cart">
        <table class="responsive-table">
          <thead>
            <tr>
              <th class="data-heading item">Item</th>
              <th class="data-heading image">Image</th>
              <th class="data-heading price">Price</th>
              <th class="data-heading qty">Qty</th>
              <th class="data-heading action text-right">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Widget</td>
              <td><img src="http://placehold.it/80x80" alt="Product Image"></td>
              <td>$12.25</td>
              <td><input type="text" value="1"></td>
              <td class="text-right">
                <a href="">Remove</a>
              </td>
            </tr>
            <tr>
              <td>Foobar</td>
              <td><img src="http://placehold.it/80x80" alt="Product Image"></td>
              <td>$12.25</td>
              <td><input type="text" value="1"></td>
              <td class="text-right">
                <a href="">Remove</a>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="5" class="text-right text-normal">Subtotal (2 items): $24.50</td>
            </tr>
          </tfoot>
        </table>
        <a href="" class="button">Checkout</a>
      </div>
    </div>

    <!-- Checkout Modal -->
    <div id="checkout-modal" class="reveal-modal" data-reveal>
      <form class="checkout">
        <section class="credit-card-info marbot-2">
          <h2>Credit Card Information</h2>
          <div class="credit-card-group">
            <label for="card-number">Credit Card Number</label>
            <input placeholder="1234 5678 9012 3456" pattern="[0-9]*" type="text" class="card-number" id="card-number">
            <label for="card-number">Expiration Date</label>
            <input placeholder="MM/YY" pattern="[0-9]*" type="text" class="card-expiration" id="card-expiration">
            <label for="card-number">CVV Number</label>
            <input placeholder="CVV" pattern="[0-9]*" type="text" class="card-cvv" id="card-cvv">
            <label for="card-number">Billing Zip Code</label>
            <input placeholder="ZIP" pattern="[0-9]*" type="text" class="card-zip" id="card-zip">
          </div>
        </section>

        <div class="row collapse">
          <div class="small-12 medium-6 columns">
            <section class="billing-info marbot-2 hide-overflow">
              <h2>Billing Information</h2>
              <div class="small-12 medium-6 columns">
                <input type="text" placeholder="Address" class="address1">
              </div>
              <div class="small-12 medium-6 columns">
                <input type="text" placeholder="City" class="city">
              </div>
              <div class="small-12 medium-6 columns">
                <select name="state" size="1" class="state">
                  <option value="AK">AK</option>
                  <option value="AL">AL</option>
                  <option value="AR">AR</option>
                  <option value="AZ">AZ</option>
                  <option value="CA">CA</option>
                  <option value="CO">CO</option>
                  <option value="CT">CT</option>
                  <option value="DC">DC</option>
                  <option value="DE">DE</option>
                  <option value="FL">FL</option>
                  <option value="GA">GA</option>
                  <option value="HI">HI</option>
                  <option value="IA">IA</option>
                  <option value="ID">ID</option>
                  <option value="IL">IL</option>
                  <option value="IN">IN</option>
                  <option value="KS">KS</option>
                  <option value="KY">KY</option>
                  <option value="LA">LA</option>
                  <option value="MA">MA</option>
                  <option value="MD">MD</option>
                  <option value="ME">ME</option>
                  <option value="MI">MI</option>
                  <option value="MN">MN</option>
                  <option value="MO">MO</option>
                  <option value="MS">MS</option>
                  <option value="MT">MT</option>
                  <option value="NC">NC</option>
                  <option value="ND">ND</option>
                  <option value="NE">NE</option>
                  <option value="NH">NH</option>
                  <option value="NJ">NJ</option>
                  <option value="NM">NM</option>
                  <option value="NV">NV</option>
                  <option value="NY">NY</option>
                  <option value="OH">OH</option>
                  <option value="OK">OK</option>
                  <option value="OR">OR</option>
                  <option value="PA">PA</option>
                  <option value="RI">RI</option>
                  <option value="SC">SC</option>
                  <option value="SD">SD</option>
                  <option value="TN">TN</option>
                  <option value="TX">TX</option>
                  <option value="UT">UT</option>
                  <option value="VA">VA</option>
                  <option value="VT">VT</option>
                  <option value="WA">WA</option>
                  <option value="WI">WI</option>
                  <option value="WV">WV</option>
                  <option value="WY">WY</option>
                </select>
              </div>
              <div class="small-12 medium-6 columns">
                <input type="text" placeholder="Zip" class="zip">
              </div>

            </section>
          </div>

          <div class="small-12 medium-6 columns">
            <section class="shipping-info">
              <h2>Shipping Information</h2>
              <p class="same-as"><i class="fa fa-check"></i> Same as billing <a href="" class="marleft-2" id="change-shipping">Change</a></p>

              <div class="shipping-info hidden">
                <div class="small-12 medium-6 columns">
                  <input type="text" placeholder="Address" class="address1">
                </div>
                <div class="small-12 medium-6 columns">
                  <input type="text" placeholder="City" class="city">
                </div>
                <div class="small-12 medium-6 columns">
                  <select name="ship-state" size="1" class="State">
                    <option value="AK">AK</option>
                    <option value="AL">AL</option>
                    <option value="AR">AR</option>
                    <option value="AZ">AZ</option>
                    <option value="CA">CA</option>
                    <option value="CO">CO</option>
                    <option value="CT">CT</option>
                    <option value="DC">DC</option>
                    <option value="DE">DE</option>
                    <option value="FL">FL</option>
                    <option value="GA">GA</option>
                    <option value="HI">HI</option>
                    <option value="IA">IA</option>
                    <option value="ID">ID</option>
                    <option value="IL">IL</option>
                    <option value="IN">IN</option>
                    <option value="KS">KS</option>
                    <option value="KY">KY</option>
                    <option value="LA">LA</option>
                    <option value="MA">MA</option>
                    <option value="MD">MD</option>
                    <option value="ME">ME</option>
                    <option value="MI">MI</option>
                    <option value="MN">MN</option>
                    <option value="MO">MO</option>
                    <option value="MS">MS</option>
                    <option value="MT">MT</option>
                    <option value="NC">NC</option>
                    <option value="ND">ND</option>
                    <option value="NE">NE</option>
                    <option value="NH">NH</option>
                    <option value="NJ">NJ</option>
                    <option value="NM">NM</option>
                    <option value="NV">NV</option>
                    <option value="NY">NY</option>
                    <option value="OH">OH</option>
                    <option value="OK">OK</option>
                    <option value="OR">OR</option>
                    <option value="PA">PA</option>
                    <option value="RI">RI</option>
                    <option value="SC">SC</option>
                    <option value="SD">SD</option>
                    <option value="TN">TN</option>
                    <option value="TX">TX</option>
                    <option value="UT">UT</option>
                    <option value="VA">VA</option>
                    <option value="VT">VT</option>
                    <option value="WA">WA</option>
                    <option value="WI">WI</option>
                    <option value="WV">WV</option>
                    <option value="WY">WY</option>
                  </select>
                </div>

                <div class="small-12 medium-6 columns">
                  <input type="text" placeholder="Zip" class="zip">
                </div>
              </div>
            </section>
          </div>
        </div>
      </form>
    </div>

    <!-- Checkout Review Modal -->
    <div id="checkout-review-modal" class="reveal-modal" data-reveal>
      <div class="checkout-review">
        <h2>Review Your Order</h2>
        <table class="responsive-table">
          <tbody>
            <tr>
              <td>Widget</td>
              <td>1</td>
              <td class="text-right">$12.25</td>
            </tr>
            <tr>
              <td>Foobar</td>
              <td>1</td>
              <td class="text-right">$12.25</td>
            </tr>
          </tbody>
          <tfoot>
            <tr class="tax-row">
              <td>Sales Tax</td>
              <td colspan="4" class="text-right">1.50</td>
            </tr>
            <tr class="shipping-row">
              <td>Shipping: Standard Ground</td>
              <td colspan="4" class="text-right">5.00</td>
            </tr>
            <tr class="total-row">
              <td colspan="5" class="text-right text-normal">Total: $31.00</td>
            </tr>
          </tfoot>
        </table>

        <div class="small-12 medium-3 columns">
          <p><strong>Payment Method</strong></p>
          <p><img src="images/cc-visa.png" width="40" class="left"> ending in 4431</p>
        </div>
        
        <div class="small-12 medium-3 columns">
          <p><strong>Billing Address</strong></p>
          <p>1701 Directors Blvd. Ste. 780<br/>Austin, TX 78744</p>
        </div>

        <div class="small-12 medium-3 columns">
          <p><strong>Shipping Address</strong></p>
          <p>720 Shipto Lane<br/>San Diego, CA 92012</p>
        </div>

        <a href="" class="button">Place Order</a>
      </div>
    </div>

    <!-- Checkout Receipt Modal -->
    <div id="checkout-receipt-modal" class="reveal-modal" data-reveal>
      <div class="checkout-receipt">
        <h2>Thank you for your order</h2>
        <div class="small-12 medium-6 columns panel">
          <a href="" class="button right">Print Receipt</a>

          <div class="clearfix"></div>

          <h3>Products</h3>
          <table class="responsive-table">
            <thead>
              <tr>
                <th class="data-heading">Item</th>
                <th class="data-heading">Qty.</th>
                <th class="data-heading">Price</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Widget</td>
                <td>1</td>
                <td class="text-right">$12.25</td>
              </tr>
              <tr>
                <td>Foobar</td>
                <td>1</td>
                <td class="text-right">$12.25</td>
              </tr>
            </tbody>
            <tfoot>
              <tr class="tax-row">
                <td>Sales Tax</td>
                <td colspan="4" class="text-right">$1.50</td>
              </tr>
              <tr class="shipping-row">
                <td>Shipping: Standard Ground</td>
                <td colspan="4" class="text-right">$5.00</td>
              </tr>
              <tr class="total-row">
                <td colspan="5" class="text-right text-normal">Total: $31.00</td>
              </tr>
            </tfoot>
          </table>

          <h3 class="martop-2">Payment Method</h3>
          <p><img src="../images/cc-visa.png" width="40" class="left"> ending in 4431</p>
        
          <h3 class="martop-2">Billing Address</h3>
          <p>1701 Directors Blvd. Ste. 780<br/>Austin, TX 78744</p>

          <h3 class="martop-2">Shipping Address</h3>
          <p>720 Shipto Lane<br/>San Diego, CA 92012</p>
        </div>
      </div>
    </div>

    <!-- Blog Post Modal -->
    <div id="blog-post-modal" class="reveal-modal" data-reveal>
      <div class="blog-post">
        <h1>Blog Post Title</h1>
        <p><span class="caption">John Smith&mdash;January 10, 2015</span></p>
        <img src="http://placehold.it/350x275" class="right marleft-1 blog-image">
        <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        <p>Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem malesuada magna mollis euismod. Donec ullamcorper nulla non metus auctor fringilla.</p>
        <p>Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui. Maecenas sed diam eget risus varius blandit sit amet non magna. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
      </div>
    </div>

    <!-- Blog Teaser Modal -->
    <div id="blog-teaser-modal" class="reveal-modal" data-reveal>
      <div class="blog-teaser">
        <h2><a href="">Blog Post Title</a></h2>
        <p><span class="caption">John Smith&mdash;January 10, 2015</span></p>
        <img src="http://placehold.it/150x125" class="left marright-1 blog-image">
        <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Maecenas sed diam eget risus varius blandit sit amet non magna... <a href="">Continue Reading &raquo;</a></p>
      </div>
    </div>

    <!-- Author Summary Modal -->
    <div id="author-summary-modal" class="reveal-modal" data-reveal>
      <div class="author-summary">
        <a href=""><img src="http://placehold.it/65x65" class="left marright-1 author-image"></a>
        <h3><a href="">John Smith</a></h3>
        <span class="caption">Job Title</span>
      </div>
    </div>

    <!-- People Grid Modal -->
    <div id="people-grid-modal" class="reveal-modal" data-reveal>
      <div class="people-grid">
        <div class="person">
          <img src="http://placehold.it/800x800" alt="Roland Deschain">
          <h4 class="name">Roland Deschain</h4>
          <h5 class="job-title">Job Title</h5>
        </div>

        <div class="person">
          <img src="http://placehold.it/800x800" alt="Odetta Walker">
          <h4 class="name">Odetta Walker</h4>
          <h5 class="job-title">Job Title</h5>
        </div>

        <div class="person">
          <img src="http://placehold.it/800x800" alt="Eddie Dean">
          <h4 class="name">Eddie Dean</h4>
          <h5 class="job-title">Job Title</h5>
        </div>
      </div>
    </div>



    <!-- Tags Modal -->
    <div id="tags-modal" class="reveal-modal" data-reveal>
      <div class="tags">
        <ul class="horizontal">
          <li><a href="">Tag 1</a></li>
          <li><a href="">Tag 2</a></li>
          <li><a href="">Tag 3</a></li>
        </ul>  
      </div>
    </div>

    <!-- Faceted Search Modal -->
    <div id="faceted-search-modal" class="reveal-modal" data-reveal>
      <aside class="faceted-search">
        <h4><strong>Facets</strong></h4>
        <ul class="no-bullets">
          <li><input type="checkbox" id="facet1"><label for="facet1">Facet</label></li>
          <li><input type="checkbox" id="facet2"><label for="facet2">Facet</label></li>
          <li><input type="checkbox" id="facet3"><label for="facet3">Facet</label></li>
          <li><input type="checkbox" id="facet4"><label for="facet4">Facet</label></li>
        </ul>
      </aside>
    </div>

    <!-- Calendar Modal -->
    <div id="calendar-modal" class="reveal-modal" data-reveal>
      <section class="row calendar collapse">
        <div class="small-12 medium-2 columns text-center month-box">
          <h2>MAR</h2>
          <span>2015</span>
        </div>
        <div class="small-12 medium-10 columns">
          <table class="responsive-table">
            <thead>
              <tr>
                <th class="data-heading">Event</th>
                <th class="data-heading text-center">Starts</th>
                <th class="data-heading text-center">Ends</th>
                <th class="data-heading">Location</th>
                <th class="data-heading"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="odd"><a href="">Some Big Event</a></td>
                <td class="text-center"><h5>MAR<br>8</h5></td>
                <td class="text-center"><h5>MAR<br>10</h5></td>
                <td>Austin, TX</td>
                <td><a href="" class="button">Details <i class="fa fa-arrow-right"></i></a></td>
              </tr>
              <tr class="even">
                <td><a href="">Some Big Event</a></td>
                <td class="text-center"><h5>MAR<br>8</h5></td>
                <td class="text-center"><h5>MAR<br>10</h5></td>
                <td>Austin, TX</td>
                <td><a href="" class="button">Details <i class="fa fa-arrow-right"></i></a></td>
              </tr>
              <tr class="odd">
                <td><a href="">Some Big Event</a></td>
                <td class="text-center"><h5>MAR<br>8</h5></td>
                <td class="text-center"><h5>MAR<br>10</h5></td>
                <td>Austin, TX</td>
                <td><a href="" class="button">Details <i class="fa fa-arrow-right"></i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </section>
    </div>

    <!-- Profile Modal -->
    <div id="profile-modal" class="reveal-modal" data-reveal>
      <section class="profile">
        <div class="small-12 medium-2 columns text-center marbot-2">
          <img src="http://placehold.it/100x100" class="circle-mask">
        </div>

        <div class="small-12 small-text-center medium-text-left medium-10 columns">
          <h1><strong>Nick Comito</strong></h1>
          <h2 class="uppercase">Designer</h2>
        </div>

        <hr>

        <div class="small-12 medium-11 columns">
          <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed posuere consectetur est at lobortis.</p>
          <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas faucibus mollis interdum.</p>
        </div>

        <div class="panel small-12 medium-1 columns">
          <ul class="no-bullets social text-large text-center">
            <li><a href=""><i class="fa fa-twitter"></i></a></li>
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-dribbble"></i></a></li>
            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
            <li><a href=""><i class="fa fa-stack-overflow"></i></a></li>
            <li><a href=""><i class="fa fa-codepen"></i></a></li>
          </ul>
        </div>
      </section>
    </div>

    <!-- Profile Modal -->
    <div id="forgot-password-modal" class="reveal-modal" data-reveal>
      <div id="forgot-password">
        <div class="row collapse">
          <h1>Forgot Your Password?</h1>
          <p>Enter your email address below and we'll send you a new one.</p>
          <div class="small-12 columns">
            <input type="text" placeholder="Username or Email" />
          </div>
          
          <div class="small-12 columns">
            <button class="small">Email New Password</button>
          </div>
        </div>
      </div>
    </div>
    


    <div class="row">
      <div class="large-12 columns">
        <a href="/">&laquo; Home</a>
      </div>
    </div>
    
    <div class="row">
      <div class="small-12 columns">
        <div class="island marbot-5">
          <div class="island-header">
            <h1>Patterns</h1>
          </div>
          <div class="island-contents">
            <p>Patterns are reusable chunks of html and css</p>
            <p><strong>To use a pattern</strong>: copy and paste the snippet of HTML below and turn on its CSS by including the appropriate line in the import statement in app.scss (scss/app.scss).</p>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 medium-4 large-3 columns">
        <ul class="side-nav no-bullets">
          <h4>Marketing</h4>
          <li><a href="#author-summary">Author Summary</a></li>
          <li><a href="#billboard">Billboard</a></li>
          <li><a href="#blog-post">Blog Post</a></li>
          <li><a href="#blog-teaser">Blog Teaser</a></li>
          <li><a href="#contextual-steps">Contextual Steps</a></li>
          <li><a href="#ctas">CTAs</a></li>
          <li><a href="#email-template">Email Template</a></li>
          <li><a href="#messages">Messages</a></li>
          <li><a href="#people-grid">People Grid</a></li>
          <li><a href="#project-grid">Project Grid</a></li>
          <li><a href="#tags">Tags</a></li>
        </ul>

        <ul class="side-nav no-bullets">
          <h4>Interaction</h4>
          <li><a href="#account-settings">Account Settings</a></li>
          <li><a href="#calendar">Calendar</a></li>
          <li><a href="#checkout">Checkout</a></li>
          <li><a href="#checkout-receipt">Checkout Receipt</a></li>
          <li><a href="#checkout-review">Checkout Review</a></li>
          <li><a href="#faceted-search">Faceted Search</a></li>
          <li><a href="#filters">Filters</a></li>
          <li><a href="#forgot-pwd">Forgot Password</a></li>
          <li><a href="#impersonation">Impersonation</a></li>
          <li><a href="#login">Login</a></li>
          <li><a href="#profile">Profile</a></li>
          <li><a href="#reset-password">Reset Password</a></li>
          <li><a href="#responsive-table">Responsive Table</a></li>
          <li><a href="#search">Search</a></li>
          <li><a href="#shopping-cart">Shopping Cart</a></li>
          <li><a href="#sticky-table-header">Sticky Table Header</a></li>
          <li><a href="#supernav">Supernav</a></li>
        </ul>
      </div>

      <div class="small-12 medium-8 large-9 columns">
        <div class="component">
          <a href="#" id="login" class="right" data-reveal-id="login-modal">Demo</a>
          <h2>Login</h2>
          <pre class="prettyprint">
            &lt;div class="small-12 medium-5 large-4 small-centered columns"&gt;
              &lt;form class="login" data-abide="ajax"&gt;&lt;img src="http://placehold.it/300x125.png" alt="Logo" id="app-logo"&gt;
                &lt;input type="text" id="username" placeholder="username" required pattern="alpha_numeric"&gt;
                &lt;small class="error"&gt;Please enter your username.&lt;/small&gt;

                &lt;input type="password" id="password" placeholder="password" required&gt;
                &lt;small class="error"&gt;Please enter your password.&lt;/small&gt;

                &lt;a href="" class="button expand"&gt;Log In&lt;/a&gt;
                &lt;p class="textSmall"&gt;&lt;a href="" &gt;Forgot your password?&lt;/a&gt; It happens.&lt;/p&gt;
              &lt;/form&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="impersonation" class="right" data-reveal-id="impersonation-modal">Demo</a>
          <h2>Impersonation</h2>
          <pre class="prettyprint">
            &lt;div class="impersonation"&gt;
              &lt;p&gt;You are Logged in as &lt;em&gt;Username&lt;/em&gt; | &lt;a href=""&gt;Logout&lt;/a&gt;&lt;/p&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="messages" class="right" data-reveal-id="messages-modal">Demo</a>
          <h2>Messages</h2>
          <pre class="prettyprint">
            &lt;div class="message"&gt;
              &lt;p&gt;This is a default message, but you can change it by adding classes like: success, error, note, and warning.&lt;/p&gt;
            &lt;/div&gt;
  
            &lt;div class="message success"&gt;
              &lt;p&gt;This is a success message.&lt;/p&gt;
            &lt;/div&gt;
  
            &lt;div class="message alert"&gt;
              &lt;p&gt;This is an alert.&lt;/p&gt;
            &lt;/div&gt;
  
            &lt;div class="message note"&gt;
              &lt;p&gt;This is a note.&lt;/p&gt;
            &lt;/div&gt;
  
            &lt;div class="message warning"&gt;
              &lt;p&gt;This is a warning.&lt;/p&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="filters.php" id="filters" class="right-modal">Demo</a>
          <h2>Filters</h2>
          <pre class="prettyprint">
            &lt;div class="filters"&gt;
              &lt;h4&gt;&lt;a href="#" class="expand-collapse-trigger"&gt;&lt;i class="fa fa-plus textXSmall"&gt;&lt;/i&gt; Filter Results&lt;/a&gt;&lt;/h4&gt;
  
              &lt;div class="small-12 medium-6 large-2 columns"&gt;
                &lt;label for="filter-startDate"&gt;Date From&lt;/label&gt;
                &lt;input type="text" id="filter-startDate" class="datepicker" /&gt;
              &lt;/div&gt;
  
              &lt;div class="small-12 medium-6 large-2 columns"&gt;
                &lt;label for="filter-endDate"&gt;To&lt;/label&gt;
                &lt;input type="text" id="filter-endDate" class="datepicker" /&gt;
              &lt;/div&gt;
  
              &lt;div class="small-12 medium-6 large-3 columns"&gt;
                &lt;label for="filter-one"&gt;Filter One&lt;/label&gt;
                &lt;select multiple class="multiselect"&gt;
                  &lt;option&gt;&lt;/option&gt;
                  &lt;option&gt;Option One&lt;/option&gt;
                  &lt;option&gt;Option Two&lt;/option&gt;
                  &lt;option&gt;Option Three&lt;/option&gt;
                  &lt;option&gt;Option Four&lt;/option&gt;
                &lt;/select&gt;
              &lt;/div&gt;
  
              &lt;div class="small-12 medium-6 large-3 columns"&gt;
                &lt;label for="filter-two"&gt;Filter Two&lt;/label&gt;
                &lt;select multiple class="multiselect"&gt;
                  &lt;option&gt;&lt;/option&gt;
                  &lt;option&gt;Option One&lt;/option&gt;
                  &lt;option&gt;Option Two&lt;/option&gt;
                  &lt;option&gt;Option Three&lt;/option&gt;
                  &lt;option&gt;Option Four&lt;/option&gt;
                &lt;/select&gt;
              &lt;/div&gt;
  
              &lt;div class="small-12 medium-6 large-3 columns"&gt;
                &lt;label for="filter-three"&gt;Filter Three&lt;/label&gt;
                &lt;select multiple class="multiselect"&gt;
                  &lt;option&gt;&lt;/option&gt;
                  &lt;option&gt;Option One&lt;/option&gt;
                  &lt;option&gt;Option Two&lt;/option&gt;
                  &lt;option&gt;Option Three&lt;/option&gt;
                  &lt;option&gt;Option Four&lt;/option&gt;
                &lt;/select&gt;
              &lt;/div&gt;
  
              &lt;div class="small-12 medium-6 large-3 columns noPadRight"&gt;
                &lt;label for="status"&gt;Show only&lt;/label&gt;
                &lt;select&gt;
                  &lt;option&gt;Status One&lt;/option&gt;
                  &lt;option&gt;Status Two&lt;/option&gt;
                  &lt;option&gt;Status Three&lt;/option&gt;
                  &lt;option&gt;Status Four&lt;/option&gt;
                &lt;/select&gt;
              &lt;/div&gt;
  
              &lt;div class="small-12 medium-6 large-6 columns noPadRight"&gt;
                &lt;a href="" class="right button small"&gt;Filter&lt;/a&gt;
              &lt;/div&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="supernav" class="right" data-reveal-id="supernav-modal">Demo</a>
          <h2>Supernav</h2>
          <pre class="prettyprint">
            &lt;h5 class="supernav large-text-right"&gt;Logged in as &lt;em&gt;Firstname Lastname&lt;/em&gt;
              &lt;span class="fa fa-chevron-right text-xsmall"&gt;&lt;/span&gt;
              &lt;a href=""&gt;My Account&lt;/a&gt;&lt;a href="/"&gt;Sign Out&lt;/a&gt;
            &lt;/h5&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="search" class="right" data-reveal-id="search-modal">Demo</a>
          <h2>Search</h2>
          <pre class="prettyprint">
            &lt;div class="search"&gt;
              &lt;input type="text" class="inline" /&gt;
              &lt;button class="inline text-medium"&gt;&lt;i class="fa fa-search"&gt;&lt;/i&gt;&lt;/button&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="account-settings" class="right" data-reveal-id="account-settings-modal">Demo</a>
          <h2>Account Settings</h2>
          <pre class="prettyprint">
            &lt;form class="account-settings"&gt;
              &lt;div class="panel hide-overflow"&gt;
                &lt;a class="right button"&gt;Edit&lt;/a&gt;
                &lt;h3&gt;&lt;strong&gt;Name&lt;/strong&gt;: Nick Comito&lt;/h3&gt;
              &lt;/div&gt;

              &lt;div class="panel hide-overflow editing"&gt;
                &lt;div class="small-3 small-push-9 columns"&gt;
                  &lt;a class="right button"&gt;Save&lt;/a&gt;
                &lt;/div&gt;
                &lt;div class="small-9 small-pull-3 columns"&gt;
                  &lt;input type="text" value="Nick Comito"&gt;
                &lt;/div&gt;
              &lt;/div&gt;

              &lt;div class="panel hide-overflow"&gt;
                &lt;div class="small-3 small-push-9 columns"&gt;
                  &lt;a class="right button"&gt;Edit&lt;/a&gt;
                &lt;/div&gt;
                &lt;div class="small-9 small-pull-3 columns"&gt;
                  &lt;h3&gt;&lt;strong&gt;Phone&lt;/strong&gt;: (512) 553-6830&lt;/h3&gt;
                &lt;/div&gt;
              &lt;/div&gt;

              &lt;div class="panel hide-overflow"&gt;
                &lt;div class="small-3 small-push-9 columns"&gt;
                  &lt;a class="right button"&gt;Edit&lt;/a&gt;
                &lt;/div&gt;
                &lt;div class="small-9 small-pull-3 columns"&gt;
                  &lt;h3&gt;&lt;strong&gt;Email&lt;/strong&gt;: nick@astonishdesign.com&lt;/h3&gt;
                &lt;/div&gt;
              &lt;/div&gt;

              &lt;div class="panel hide-overflow"&gt;
                &lt;div class="small-3 small-push-9 columns"&gt;
                  &lt;div class="right button-group"&gt;
                    &lt;a class="button"&gt;View&lt;/a&gt;
                    &lt;a class="button"&gt;Edit&lt;/a&gt;
                  &lt;/div&gt;
                &lt;/div&gt;
                &lt;div class="small-9 small-pull-3 columns"&gt;
                  &lt;h3&gt;&lt;strong&gt;Password&lt;/strong&gt;: *******&lt;/h3&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/form&gt;
          </pre>
        </div>




        <div class="component">
          <a href="#" id="billboard" class="right" data-reveal-id="billboard-modal">Demo</a>
          <h2>Billboard</h2>
          <pre class="prettyprint">
            &lt;div class="billboard"&gt;
              &lt;div class="small-12 medium-7 large-8 columns"&gt;
                &lt;h1 class="marbot-2"&gt;Everything should be made as simple as possible, but not simpler.&lt;/h1&gt;
                &lt;a class="button padall-1 padleft-5 padright-5"&gt;Click Me&lt;/a&gt;
              &lt;/div&gt;
              &lt;div class="small-12 medium-5 large-4 columns"&gt;
                &lt;img src="http://placehold.it/500x400"&gt;
              &lt;/div&gt;
            &lt;/div&gt;    
          </pre>
        </div>



        <div class="component">
          <a href="#" id="contextual-steps" class="right" data-reveal-id="contextual-steps-modal">Demo</a>
          <h2>Contextual Steps</h2>
          <pre class="prettyprint">
            &lt;!-- Steps Range from 3 to 6 --&gt;
            &lt;ul class="contextual-steps four text-center"&gt;
              &lt;li class="step"&gt;
                &lt;div class="step-number"&gt;1&lt;/div&gt;
                &lt;div class="step-text"&gt;Whiteboards&lt;/div&gt;
              &lt;/li&gt;
              &lt;li class="step active"&gt;
                &lt;div class="step-number"&gt;2&lt;/div&gt;
                &lt;div class="step-text"&gt;ClickModel&lt;/div&gt;
              &lt;/li&gt;
              &lt;li class="step"&gt;
                &lt;div class="step-number"&gt;3&lt;/div&gt;
                &lt;div class="step-text"&gt;UI&lt;/div&gt;
              &lt;/li&gt;
              &lt;li class="step"&gt;
                &lt;div class="step-number"&gt;4&lt;/div&gt;
                &lt;div class="step-text"&gt;Software&lt;/div&gt;
              &lt;/li&gt;
            &lt;/ul&gt;
          </pre>
        </div>


        <div class="component">
          <a href="#" id="responsive-table" class="right" data-reveal-id="responsive-table-modal">Demo</a>
          <h2>Responsive Table</h2>
          <pre class="prettyprint">
            &lt;table class="responsive-table"&gt;
              &lt;thead&gt;
                &lt;tr&gt;
                  &lt;th class="data-heading">Date&lt;/th&gt;
                  &lt;th class="data-heading">Quantity&lt;/th&gt;
                  &lt;th class="data-heading">Rate&lt;/th&gt;
                  &lt;th class="data-heading">Status&lt;/th&gt;
                  &lt;th class="data-heading">Total&lt;/th&gt;
                &lt;/tr&gt;
              &lt;/thead&gt;

              &lt;tbody&gt;
                &lt;tr class="odd"&gt;
                  &lt;td>10/12/2014&lt;/td&gt;
                  &lt;td>21&lt;/td&gt;
                  &lt;td>24%&lt;/td&gt;
                  &lt;td>Open&lt;/td&gt;
                  &lt;td>54&lt;/td&gt;
                &lt;/tr&gt;

                &lt;tr class="even"&gt;
                  &lt;td>1/12/2015&lt;/td&gt;
                  &lt;td>41&lt;/td&gt;
                  &lt;td>44%&lt;/td&gt;
                  &lt;td>Closed&lt;/td&gt;
                  &lt;td>104&lt;/td&gt;
                &lt;/tr&gt;

                &lt;tr class="odd"&gt;
                  &lt;td>12/18/2014&lt;/td&gt;
                  &lt;td>29&lt;/td&gt;
                  &lt;td>26%&lt;/td&gt;
                  &lt;td>Closed&lt;/td&gt;
                  &lt;td>59&lt;/td&gt;
                &lt;/tr&gt;
              &lt;/tbody&gt;
            &lt;/table&gt;
          </pre>
        </div>



        <div class="component">
          <a href="sticky-table-header.php" id="sticky-table-header" class="right">Demo</a>
          <h2>Sticky Table Header</h2>
          <p>To implement a sticky header on any table, simply add the class "sticky-table" on a table element.</p>
        </div>



        <div class="component">
          <a href="#" id="ctas" class="right" data-reveal-id="ctas-modal">Demo</a>
          <h2>Calls to Action</h2>
          <p><strong>Note</strong>&mdash;<em>not all HTML is required. Included are several examples to show how flexible CTAs can be. Control size with Foundations grid.</em></p>
          <pre class="prettyprint">
            &lt;div class="small-5 columns"&gt;
              &lt;div class="cta"&gt;
                &lt;div class="cta-contents"&gt;
                  &lt;h2>Call To Action&lt;/h2&gt;
                  &lt;p&gt;Nullam id dolor id nibh ultricies vehicula ut id elit.&lt;/p&gt;
                  &lt;a href="#" class="button expand">Click Here&lt;/a&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/div&gt;

            &lt;div class="small-7 columns"&gt;
              &lt;div class="cta"&gt;
                &lt;img src="http://placehold.it/800x300"&gt;
                &lt;div class="cta-contents"&gt;
                  &lt;h2>Call To Action&lt;/h2&gt;
                  &lt;p&gt;Nullam id dolor id nibh ultricies vehicula ut id elit.&lt;/p&gt;
                  &lt;a href="#" class="button expand">Click Here&lt;/a&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/div&gt;

            &lt;div class="small-3 columns"&gt;
              &lt;div class="cta"&gt;
                &lt;img src="http://placehold.it/200x400"&gt;
                &lt;div class="cta-contents"&gt;
                  &lt;h2>Call To Action&lt;/h2&gt;
                  &lt;p&gt;Nullam id dolor id nibh ultricies vehicula ut id elit.&lt;/p&gt;
                  &lt;a href="#" class="button expand">More&lt;/a&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/div&gt;

            &lt;div class="small-4 columns"&gt;
              &lt;div class="cta"&gt;
                &lt;div class="cta-contents"&gt;
                  &lt;img src="http://placehold.it/400x400" /&gt;
                  &lt;h2>Call To Action&lt;/h2&gt;
                  &lt;p&gt;Nullam id dolor id nibh ultricies vehicula ut id elit.&lt;/p&gt;
                  &lt;a href="#" class="button expand">Click Here&lt;/a&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/div&gt;

            &lt;div class="small-5 columns"&gt;
              &lt;div class="cta"&gt;
                &lt;div class="cta-contents"&gt;
                  &lt;img src="http://placehold.it/120x120" class="left" /&gt;
                  &lt;h2>Call To Action&lt;/h2&gt;
                  &lt;p&gt;Nullam id dolor id nibh ultricies vehicula ut id elit.&lt;/p&gt;
                  &lt;a href="#" class="button">Click Here&lt;/a&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/div&gt;

            &lt;div class="small-5 columns"&gt;
              &lt;div class="cta"&gt;
                &lt;div class="cta-contents"&gt;
                  &lt;img src="http://placehold.it/120x120" class="right" /&gt;
                  &lt;h2>Call To Action&lt;/h2&gt;
                  &lt;p&gt;Nullam id dolor id nibh ultricies vehicula ut id elit.&lt;/p&gt;
                  &lt;a href="#" class="button">Click Here&lt;/a&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="shopping-cart" class="right" data-reveal-id="shopping-cart-modal">Demo</a>
          <h2>Shopping Cart</h2>
          <pre class="prettyprint">
            &lt;div class="shopping-cart"&gt;
              &lt;table class="responsive-table"&gt;
                &lt;thead&gt;
                  &lt;tr&gt;
                    &lt;th class="data-heading item">Item&lt;/th&gt;
                    &lt;th class="data-heading image">Image&lt;/th&gt;
                    &lt;th class="data-heading price">Price&lt;/th&gt;
                    &lt;th class="data-heading qty">Qty&lt;/th&gt;
                    &lt;th class="data-heading action text-right">Action&lt;/th&gt;
                  &lt;/tr&gt;
                &lt;/thead&gt;
                &lt;tbody&gt;
                  &lt;tr&gt;
                    &lt;td>Widget&lt;/td&gt;
                    &lt;td>&lt;img src="http://placehold.it/80x80" alt="Product Image">&lt;/td&gt;
                    &lt;td>$12.25&lt;/td&gt;
                    &lt;td>&lt;input type="text" value="1">&lt;/td&gt;
                    &lt;td class="text-right"&gt;
                      &lt;a href="">Remove&lt;/a&gt;
                    &lt;/td&gt;
                  &lt;/tr&gt;
                  &lt;tr&gt;
                    &lt;td>Foobar&lt;/td&gt;
                    &lt;td>&lt;img src="http://placehold.it/80x80" alt="Product Image">&lt;/td&gt;
                    &lt;td>$12.25&lt;/td&gt;
                    &lt;td>&lt;input type="text" value="1">&lt;/td&gt;
                    &lt;td class="text-right"&gt;
                      &lt;a href="">Remove&lt;/a&gt;
                    &lt;/td&gt;
                  &lt;/tr&gt;
                &lt;/tbody&gt;
                &lt;tfoot&gt;
                  &lt;tr&gt;
                    &lt;td colspan="5" class="text-right text-normal">Subtotal (2 items): $24.50&lt;/td&gt;
                  &lt;/tr&gt;
                &lt;/tfoot&gt;
              &lt;/table&gt;
              &lt;a href="" class="button">Checkout&lt;/a&gt;
            &lt;/div&gt;
          </pre>
        </div>



        <div class="component">
          <a href="#" id="checkout" class="right" data-reveal-id="checkout-modal">Demo</a>
          <h2>Checkout</h2>
          <pre class="prettyprint">
            &lt;form class="checkout"&gt;
              &lt;section class="credit-card-info marbot-2"&gt;
                &lt;h2>Credit Card Information&lt;/h2&gt;
                &lt;div class="credit-card-group"&gt;
                  &lt;label for="card-number">Credit Card Number&lt;/label&gt;
                  &lt;input placeholder="1234 5678 9012 3456" pattern="[0-9]*" type="text" class="card-number" id="card-number"&gt;
                  &lt;label for="card-number">Expiration Date&lt;/label&gt;
                  &lt;input placeholder="MM/YY" pattern="[0-9]*" type="text" class="card-expiration" id="card-expiration"&gt;
                  &lt;label for="card-number">CVV Number&lt;/label&gt;
                  &lt;input placeholder="CVV" pattern="[0-9]*" type="text" class="card-cvv" id="card-cvv"&gt;
                  &lt;label for="card-number">Billing Zip Code&lt;/label&gt;
                  &lt;input placeholder="ZIP" pattern="[0-9]*" type="text" class="card-zip" id="card-zip"&gt;
                &lt;/div&gt;
              &lt;/section&gt;

              &lt;div class="row collapse"&gt;
                &lt;div class="small-12 medium-6 columns"&gt;
                  &lt;section class="billing-info marbot-2 hide-overflow"&gt;
                    &lt;h2>Billing Information&lt;/h2&gt;
                    &lt;div class="small-12 medium-6 columns"&gt;
                      &lt;input type="text" placeholder="Address" class="address1"&gt;
                    &lt;/div&gt;
                    &lt;div class="small-12 medium-6 columns"&gt;
                      &lt;input type="text" placeholder="City" class="city"&gt;
                    &lt;/div&gt;
                    &lt;div class="small-12 medium-6 columns"&gt;
                      &lt;select name="state" size="1" class="state"&gt;
                        &lt;option value="AK">AK&lt;/option&gt;
                        &lt;option value="AL">AL&lt;/option&gt;
                        &lt;option value="AR">AR&lt;/option&gt;
                        &lt;option value="AZ">AZ&lt;/option&gt;
                        &lt;option value="CA">CA&lt;/option&gt;
                        &lt;option value="CO">CO&lt;/option&gt;
                        &lt;option value="CT">CT&lt;/option&gt;
                        &lt;option value="DC">DC&lt;/option&gt;
                        &lt;option value="DE">DE&lt;/option&gt;
                        &lt;option value="FL">FL&lt;/option&gt;
                        &lt;option value="GA">GA&lt;/option&gt;
                        &lt;option value="HI">HI&lt;/option&gt;
                        &lt;option value="IA">IA&lt;/option&gt;
                        &lt;option value="ID">ID&lt;/option&gt;
                        &lt;option value="IL">IL&lt;/option&gt;
                        &lt;option value="IN">IN&lt;/option&gt;
                        &lt;option value="KS">KS&lt;/option&gt;
                        &lt;option value="KY">KY&lt;/option&gt;
                        &lt;option value="LA">LA&lt;/option&gt;
                        &lt;option value="MA">MA&lt;/option&gt;
                        &lt;option value="MD">MD&lt;/option&gt;
                        &lt;option value="ME">ME&lt;/option&gt;
                        &lt;option value="MI">MI&lt;/option&gt;
                        &lt;option value="MN">MN&lt;/option&gt;
                        &lt;option value="MO">MO&lt;/option&gt;
                        &lt;option value="MS">MS&lt;/option&gt;
                        &lt;option value="MT">MT&lt;/option&gt;
                        &lt;option value="NC">NC&lt;/option&gt;
                        &lt;option value="ND">ND&lt;/option&gt;
                        &lt;option value="NE">NE&lt;/option&gt;
                        &lt;option value="NH">NH&lt;/option&gt;
                        &lt;option value="NJ">NJ&lt;/option&gt;
                        &lt;option value="NM">NM&lt;/option&gt;
                        &lt;option value="NV">NV&lt;/option&gt;
                        &lt;option value="NY">NY&lt;/option&gt;
                        &lt;option value="OH">OH&lt;/option&gt;
                        &lt;option value="OK">OK&lt;/option&gt;
                        &lt;option value="OR">OR&lt;/option&gt;
                        &lt;option value="PA">PA&lt;/option&gt;
                        &lt;option value="RI">RI&lt;/option&gt;
                        &lt;option value="SC">SC&lt;/option&gt;
                        &lt;option value="SD">SD&lt;/option&gt;
                        &lt;option value="TN">TN&lt;/option&gt;
                        &lt;option value="TX">TX&lt;/option&gt;
                        &lt;option value="UT">UT&lt;/option&gt;
                        &lt;option value="VA">VA&lt;/option&gt;
                        &lt;option value="VT">VT&lt;/option&gt;
                        &lt;option value="WA">WA&lt;/option&gt;
                        &lt;option value="WI">WI&lt;/option&gt;
                        &lt;option value="WV">WV&lt;/option&gt;
                        &lt;option value="WY">WY&lt;/option&gt;
                      &lt;/select&gt;
                    &lt;/div&gt;
                    &lt;div class="small-12 medium-6 columns"&gt;
                      &lt;input type="text" placeholder="Zip" class="zip"&gt;
                    &lt;/div&gt;
        
                  &lt;/section&gt;
                &lt;/div&gt;
        
                &lt;div class="small-12 medium-6 columns"&gt;
                  &lt;section class="shipping-info"&gt;
                    &lt;h2>Shipping Information&lt;/h2&gt;
                    &lt;p class="same-as">&lt;i class="fa fa-check">&lt;/i> Same as billing &lt;a href="" class="marleft-2" id="change-shipping">Change&lt;/a>&lt;/p&gt;
          
                    &lt;div class="shipping-info hidden"&gt;
                      &lt;div class="small-12 medium-6 columns"&gt;
                        &lt;input type="text" placeholder="Address" class="address1"&gt;
                      &lt;/div&gt;
                      &lt;div class="small-12 medium-6 columns"&gt;
                        &lt;input type="text" placeholder="City" class="city"&gt;
                      &lt;/div&gt;
                      &lt;div class="small-12 medium-6 columns"&gt;
                        &lt;select name="ship-state" size="1" class="State"&gt;
                          &lt;option value="AK">AK&lt;/option&gt;
                          &lt;option value="AL">AL&lt;/option&gt;
                          &lt;option value="AR">AR&lt;/option&gt;
                          &lt;option value="AZ">AZ&lt;/option&gt;
                          &lt;option value="CA">CA&lt;/option&gt;
                          &lt;option value="CO">CO&lt;/option&gt;
                          &lt;option value="CT">CT&lt;/option&gt;
                          &lt;option value="DC">DC&lt;/option&gt;
                          &lt;option value="DE">DE&lt;/option&gt;
                          &lt;option value="FL">FL&lt;/option&gt;
                          &lt;option value="GA">GA&lt;/option&gt;
                          &lt;option value="HI">HI&lt;/option&gt;
                          &lt;option value="IA">IA&lt;/option&gt;
                          &lt;option value="ID">ID&lt;/option&gt;
                          &lt;option value="IL">IL&lt;/option&gt;
                          &lt;option value="IN">IN&lt;/option&gt;
                          &lt;option value="KS">KS&lt;/option&gt;
                          &lt;option value="KY">KY&lt;/option&gt;
                          &lt;option value="LA">LA&lt;/option&gt;
                          &lt;option value="MA">MA&lt;/option&gt;
                          &lt;option value="MD">MD&lt;/option&gt;
                          &lt;option value="ME">ME&lt;/option&gt;
                          &lt;option value="MI">MI&lt;/option&gt;
                          &lt;option value="MN">MN&lt;/option&gt;
                          &lt;option value="MO">MO&lt;/option&gt;
                          &lt;option value="MS">MS&lt;/option&gt;
                          &lt;option value="MT">MT&lt;/option&gt;
                          &lt;option value="NC">NC&lt;/option&gt;
                          &lt;option value="ND">ND&lt;/option&gt;
                          &lt;option value="NE">NE&lt;/option&gt;
                          &lt;option value="NH">NH&lt;/option&gt;
                          &lt;option value="NJ">NJ&lt;/option&gt;
                          &lt;option value="NM">NM&lt;/option&gt;
                          &lt;option value="NV">NV&lt;/option&gt;
                          &lt;option value="NY">NY&lt;/option&gt;
                          &lt;option value="OH">OH&lt;/option&gt;
                          &lt;option value="OK">OK&lt;/option&gt;
                          &lt;option value="OR">OR&lt;/option&gt;
                          &lt;option value="PA">PA&lt;/option&gt;
                          &lt;option value="RI">RI&lt;/option&gt;
                          &lt;option value="SC">SC&lt;/option&gt;
                          &lt;option value="SD">SD&lt;/option&gt;
                          &lt;option value="TN">TN&lt;/option&gt;
                          &lt;option value="TX">TX&lt;/option&gt;
                          &lt;option value="UT">UT&lt;/option&gt;
                          &lt;option value="VA">VA&lt;/option&gt;
                          &lt;option value="VT">VT&lt;/option&gt;
                          &lt;option value="WA">WA&lt;/option&gt;
                          &lt;option value="WI">WI&lt;/option&gt;
                          &lt;option value="WV">WV&lt;/option&gt;
                          &lt;option value="WY">WY&lt;/option&gt;
                        &lt;/select&gt;
                      &lt;/div&gt;
            
                      &lt;div class="small-12 medium-6 columns"&gt;
                        &lt;input type="text" placeholder="Zip" class="zip"&gt;
                      &lt;/div&gt;
                    &lt;/div&gt;
                  &lt;/section&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/form&gt;
          </pre>
        </div>


        <div class="component">
          <a href="#" id="checkout-review" class="right" data-reveal-id="checkout-review-modal">Demo</a>
          <h2>Checkout Review</h2>
          <pre class="prettyprint">
            &lt;div class="checkout-review"&gt;
              &lt;h2>Review Your Order&lt;/h2&gt;
              &lt;table class="responsive-table"&gt;
                &lt;tbody&gt;
                  &lt;tr&gt;
                    &lt;td>Widget&lt;/td&gt;
                    &lt;td>1&lt;/td&gt;
                    &lt;td class="text-right">$12.25&lt;/td&gt;
                  &lt;/tr&gt;
                  &lt;tr&gt;
                    &lt;td>Foobar&lt;/td&gt;
                    &lt;td>1&lt;/td&gt;
                    &lt;td class="text-right">$12.25&lt;/td&gt;
                  &lt;/tr&gt;
                &lt;/tbody&gt;
                &lt;tfoot&gt;
                  &lt;tr class="tax-row"&gt;
                    &lt;td>Sales Tax&lt;/td&gt;
                    &lt;td colspan="4" class="text-right">1.50&lt;/td&gt;
                  &lt;/tr&gt;
                  &lt;tr class="shipping-row"&gt;
                    &lt;td>Shipping: Standard Ground&lt;/td&gt;
                    &lt;td colspan="4" class="text-right">5.00&lt;/td&gt;
                  &lt;/tr&gt;
                  &lt;tr class="total-row"&gt;
                    &lt;td colspan="5" class="text-right text-normal">Total: $31.00&lt;/td&gt;
                  &lt;/tr&gt;
                &lt;/tfoot&gt;
              &lt;/table&gt;

              &lt;div class="small-12 medium-3 columns"&gt;
                &lt;p&gt;&lt;strong>Payment Method&lt;/strong>&lt;/p&gt;
                &lt;p&gt;&lt;img src="../images/cc-visa.png" width="40" class="left"> ending in 4431&lt;/p&gt;
              &lt;/div&gt;
              
              &lt;div class="small-12 medium-3 columns"&gt;
                &lt;p&gt;&lt;strong>Billing Address&lt;/strong>&lt;/p&gt;
                &lt;p&gt;1701 Directors Blvd. Ste. 780&lt;br/>Austin, TX 78744&lt;/p&gt;
              &lt;/div&gt;

              &lt;div class="small-12 medium-3 columns"&gt;
                &lt;p&gt;&lt;strong>Shipping Address&lt;/strong>&lt;/p&gt;
                &lt;p&gt;720 Shipto Lane&lt;br/>San Diego, CA 92012&lt;/p&gt;
              &lt;/div&gt;

              &lt;a href="" class="button">Place Order&lt;/a&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="checkout-receipt" class="right" data-reveal-id="checkout-receipt-modal">Demo</a>
          <h2>Checkout Receipt</h2>
          <pre class="prettyprint">
            &lt;div class="checkout-receipt"&gt;
              &lt;h2>Thank you for your order&lt;/h2&gt;
              &lt;div class="small-12 medium-6 columns panel"&gt;
                &lt;a href="" class="button right">Print Receipt&lt;/a&gt;

                &lt;div class="clearfix">&lt;/div&gt;

                &lt;h3>Products&lt;/h3&gt;
                &lt;table class="responsive-table"&gt;
                  &lt;thead&gt;
                    &lt;tr&gt;
                      &lt;th class="data-heading">Item&lt;/th&gt;
                      &lt;th class="data-heading">Qty.&lt;/th&gt;
                      &lt;th class="data-heading">Price&lt;/th&gt;
                    &lt;/tr&gt;
                  &lt;/thead&gt;
                  &lt;tbody&gt;
                    &lt;tr&gt;
                      &lt;td>Widget&lt;/td&gt;
                      &lt;td>1&lt;/td&gt;
                      &lt;td class="text-right">$12.25&lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                      &lt;td>Foobar&lt;/td&gt;
                      &lt;td>1&lt;/td&gt;
                      &lt;td class="text-right">$12.25&lt;/td&gt;
                    &lt;/tr&gt;
                  &lt;/tbody&gt;
                  &lt;tfoot&gt;
                    &lt;tr class="tax-row"&gt;
                      &lt;td>Sales Tax&lt;/td&gt;
                      &lt;td colspan="4" class="text-right">$1.50&lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr class="shipping-row"&gt;
                      &lt;td>Shipping: Standard Ground&lt;/td&gt;
                      &lt;td colspan="4" class="text-right">$5.00&lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr class="total-row"&gt;
                      &lt;td colspan="5" class="text-right text-normal">Total: $31.00&lt;/td&gt;
                    &lt;/tr&gt;
                  &lt;/tfoot&gt;
                &lt;/table    &gt;

                &lt;h3 class="martop-2">Payment Method&lt;/h3&gt;
                &lt;p&gt;&lt;img src="../images/cc-visa.png" width="40" class="left"> ending in 4431&lt;/p&gt;
              
                &lt;h3 class="martop-2">Billing Address&lt;/h3&gt;
                &lt;p&gt;1701 Directors Blvd. Ste. 780&lt;br/>Austin, TX 78744&lt;/p&gt;

                &lt;h3 class="martop-2">Shipping Address&lt;/h3&gt;
                &lt;p&gt;720 Shipto Lane&lt;br/>San Diego, CA 92012&lt;/p&gt;
              &lt;/div&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a id="email-template"></a>
          <h2>Email Template</h2>
          <p><a href="/components/html-components/responsive-email-template.html">View the template</a></p>
        </div>

        <div class="component">
          <a href="#" id="blog-post" class="right" data-reveal-id="blog-post-modal">Demo</a>
          <h2>Blog Post</h2>
          <pre class="prettyprint">
            &lt;div class="blog-post"&gt;
              &lt;h1&gt;Blog Post Title&lt;/h1&gt;
              &lt;p&gt;&lt;span class="caption"&gt;John Smith&mdash;January 10, 2015&lt;/span&gt;&lt;/p&gt;
              &lt;img src="http://placehold.it/350x275" class="right marleft-1 blog-image"&gt;
              &lt;p&gt;Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Maecenas sed diam eget risus varius blandit sit amet non magna.&lt;/p&gt;
              &lt;p&gt;Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem malesuada magna mollis euismod. Donec ullamcorper nulla non metus auctor fringilla.&lt;/p&gt;
              &lt;p&gt;Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui. Maecenas sed diam eget risus varius blandit sit amet non magna. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.&lt;/p&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="blog-teaser" class="right" data-reveal-id="blog-teaser-modal">Demo</a>
          <h2>Blog Teaser</h2>
          <pre class="prettyprint">
            &lt;div class="blog-teaser"&gt;
              &lt;h2&gt;&lt;a href=""&gt;Blog Post Title&lt;/a&gt;&lt;/h2&gt;
              &lt;p&gt;&lt;span class="caption"&gt;John Smith&mdash;January 10, 2015&lt;/span&gt;&lt;/p&gt;
              &lt;img src="http://placehold.it/150x125" class="left marright-1 blog-image"&gt;
              &lt;p&gt;Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Maecenas sed diam eget risus varius blandit sit amet non magna... &lt;a href=""&gt;Continue Reading &raquo;&lt;/a&gt;&lt;/p&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="author-summary" class="right" data-reveal-id="author-summary-modal">Demo</a>
          <h2>Author Summary</h2>
          <pre class="prettyprint">
            &lt;div class="author-summary"&gt;
              &lt;a href="">&lt;img src="http://placehold.it/65x65" class="left marright-1 author-image">&lt;/a&gt;
              &lt;h3&gt;&lt;a href=""&gt;John Smith&lt;/a&gt;&lt;/h3&gt;
              &lt;span class="caption"&gt;Job Title&lt;/span&gt;
            &lt;/div&gt;
          </pre>
        </div>





        <div class="component">
          <a href="#" id="people-grid" class="right" data-reveal-id="people-grid-modal">Demo</a>
          <h2>People Grid</h2>
          <pre class="prettyprint">
            &lt;div class="people-grid"&gt;
              &lt;div class="person"&gt;
                &lt;img src="http://placehold.it/800x800" alt="Roland Deschain"&gt;
                &lt;h4 class="name"&gt;Roland Deschain&lt;/h4&gt;
                &lt;h5 class="job-title"&gt;Job Title&lt;/h5&gt;
              &lt;/div&gt;

              &lt;div class="person"&gt;
                &lt;img src="http://placehold.it/800x800" alt="Odetta Walker"&gt;
                &lt;h4 class="name"&gt;Odetta Walker&lt;/h4&gt;
                &lt;h5 class="job-title"&gt;Job Title&lt;/h5&gt;
              &lt;/div&gt;

              &lt;div class="person"&gt;
                &lt;img src="http://placehold.it/800x800" alt="Eddie Dean"&gt;
                &lt;h4 class="name"&gt;Eddie Dean&lt;/h4&gt;
                &lt;h5 class="job-title"&gt;Job Title&lt;/h5&gt;
              &lt;/div&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="tags" class="right" data-reveal-id="tags-modal">Demo</a>
          <h2>Tags</h2>
          <pre class="prettyprint">
            &lt;div class="tags"&gt;
              &lt;ul class="horizontal"&gt;
                &lt;li&gt;&lt;a href=""&gt;Tag 1&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href=""&gt;Tag 2&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href=""&gt;Tag 3&lt;/a&gt;&lt;/li&gt;
              &lt;/ul&gt;  
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="project-grid.php" id="project-grid" class="right-modal">Demo</a>
          <h2>Project Grid</h2>
          <pre class="prettyprint">
            &lt;div class="project-grid"&gt;
              &lt;div class="project"&gt;
                &lt;img src="http://placehold.it/800x800"&gt;
                &lt;h4 class="title"&gt;Project Title&lt;/h4&gt;
              &lt;/div&gt;

              &lt;div class="project"&gt;
                &lt;img src="http://placehold.it/800x800"&gt;
                &lt;h4 class="title"&gt;Project Title&lt;/h4&gt;
              &lt;/div&gt;

              &lt;div class="project"&gt;
                &lt;img src="http://placehold.it/800x800"&gt;
                &lt;h4 class="title"&gt;Project Title&lt;/h4&gt;
              &lt;/div&gt;
            &lt;/div&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="faceted-search" class="right" data-reveal-id="faceted-search-modal">Demo</a>
          <h2>Faceted Search</h2>
          <pre class="prettyprint">
            &lt;aside class="faceted-search"&gt;
              &lt;h4&gt;&lt;strong&gt;Facets&lt;/strong&gt;&lt;/h4&gt;
              &lt;ul class="no-bullets"&gt;
                &lt;li&gt;&lt;input type="checkbox" id="facet1"&gt;&lt;label for="facet1"&gt;Facet&lt;/label&gt;&lt;/li&gt;
                &lt;li&gt;&lt;input type="checkbox" id="facet2"&gt;&lt;label for="facet2"&gt;Facet&lt;/label&gt;&lt;/li&gt;
                &lt;li&gt;&lt;input type="checkbox" id="facet3"&gt;&lt;label for="facet3"&gt;Facet&lt;/label&gt;&lt;/li&gt;
                &lt;li&gt;&lt;input type="checkbox" id="facet4"&gt;&lt;label for="facet4"&gt;Facet&lt;/label&gt;&lt;/li&gt;
              &lt;/ul&gt;
            &lt;/aside&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="profile" class="right" data-reveal-id="profile-modal">Demo</a>
          <h2>Profile</h2>
          <pre class="prettyprint">
            &lt;section class="profile"&gt;
              &lt;div class="small-12 medium-2 columns text-center marbot-2"&gt;
                &lt;img src="http://placehold.it/100x100" class="circle-mask"&gt;
              &lt;/div&gt;

              &lt;div class="small-12 small-text-center medium-text-left medium-10 columns"&gt;
                &lt;h1&gt;&lt;strong&gt;Nick Comito&lt;/strong&gt;&lt;/h1&gt;
                &lt;h2 class="uppercase"&gt;Designer&lt;/h2&gt;
              &lt;/div&gt;

              &lt;hr&gt;

              &lt;div class="small-12 medium-11 columns"&gt;
                &lt;p&gt;Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed posuere consectetur est at lobortis.&lt;/p&gt;
                &lt;p&gt;Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas faucibus mollis interdum.&lt;/p&gt;
              &lt;/div&gt;

              &lt;div class="panel small-12 medium-1 columns"&gt;
                &lt;ul class="no-bullets social text-large text-center"&gt;
                  &lt;li&gt;&lt;a href=""&gt;&lt;i class="fa fa-twitter"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;
                  &lt;li&gt;&lt;a href=""&gt;&lt;i class="fa fa-facebook"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;
                  &lt;li&gt;&lt;a href=""&gt;&lt;i class="fa fa-dribbble"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;
                  &lt;li&gt;&lt;a href=""&gt;&lt;i class="fa fa-google-plus"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;
                  &lt;li&gt;&lt;a href=""&gt;&lt;i class="fa fa-linkedin"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;
                  &lt;li&gt;&lt;a href=""&gt;&lt;i class="fa fa-stack-overflow"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;
                  &lt;li&gt;&lt;a href=""&gt;&lt;i class="fa fa-codepen"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;
                &lt;/ul&gt;
              &lt;/div&gt;
            &lt;/section&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="calendar" class="right" data-reveal-id="calendar-modal">Demo</a>
          <h2>Calendar</h2>
          <pre class="prettyprint">
            &lt;section class="row calendar collapse"&gt;
              &lt;div class="small-12 medium-2 columns text-center month-box"&gt;
                &lt;h2&gt;MAR&lt;/h2&gt;
                &lt;span&gt;2015&lt;/span&gt;
              &lt;/div&gt;
              &lt;div class="small-12 medium-10 columns"&gt;
                &lt;table class="responsive-table"&gt;
                  &lt;thead&gt;
                    &lt;tr&gt;
                      &lt;th class="data-heading"&gt;Event&lt;/th&gt;
                      &lt;th class="data-heading text-center"&gt;Starts&lt;/th&gt;
                      &lt;th class="data-heading text-center"&gt;Ends&lt;/th&gt;
                      &lt;th class="data-heading"&gt;Location&lt;/th&gt;
                      &lt;th class="data-heading"&gt;&lt;/th&gt;
                    &lt;/tr&gt;
                  &lt;/thead&gt;
                  &lt;tbody&gt;
                    &lt;tr&gt;
                      &lt;td class="odd"&gt;&lt;a href=""&gt;Some Big Event&lt;/a&gt;&lt;/td&gt;
                      &lt;td class="text-center"&gt;&lt;h5&gt;MAR&lt;br&gt;8&lt;/h5&gt;&lt;/td&gt;
                      &lt;td class="text-center"&gt;&lt;h5&gt;MAR&lt;br&gt;10&lt;/h5&gt;&lt;/td&gt;
                      &lt;td&gt;Austin, TX&lt;/td&gt;
                      &lt;td&gt;&lt;a href="" class="button"&gt;Details &lt;i class="fa fa-arrow-right"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr class="even"&gt;
                      &lt;td&gt;&lt;a href=""&gt;Some Big Event&lt;/a&gt;&lt;/td&gt;
                      &lt;td class="text-center"&gt;&lt;h5&gt;MAR&lt;br&gt;8&lt;/h5&gt;&lt;/td&gt;
                      &lt;td class="text-center"&gt;&lt;h5&gt;MAR&lt;br&gt;10&lt;/h5&gt;&lt;/td&gt;
                      &lt;td&gt;Austin, TX&lt;/td&gt;
                      &lt;td&gt;&lt;a href="" class="button"&gt;Details &lt;i class="fa fa-arrow-right"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr class="odd"&gt;
                      &lt;td&gt;&lt;a href=""&gt;Some Big Event&lt;/a&gt;&lt;/td&gt;
                      &lt;td class="text-center"&gt;&lt;h5&gt;MAR&lt;br&gt;8&lt;/h5&gt;&lt;/td&gt;
                      &lt;td class="text-center"&gt;&lt;h5&gt;MAR&lt;br&gt;10&lt;/h5&gt;&lt;/td&gt;
                      &lt;td&gt;Austin, TX&lt;/td&gt;
                      &lt;td&gt;&lt;a href="" class="button"&gt;Details &lt;i class="fa fa-arrow-right"&gt;&lt;/i&gt;&lt;/a&gt;&lt;/td&gt;
                    &lt;/tr&gt;
                  &lt;/tbody&gt;
                &lt;/table&gt;
              &lt;/div&gt;
            &lt;/section&gt;
          </pre>
        </div>

        <div class="component">
          <a href="#" id="forgot-pwd" class="right" data-reveal-id="forgot-password-modal">Demo</a>
          <h2>Forgot Password</h2>
          <pre class="prettyprint">
            &lt;div id="forgot-password"&gt;
              &lt;div class="row collapse"&gt;
                &lt;h1&gt;Forgot Your Password?&lt;/h1&gt;
                &lt;p&gt;Enter your email address below and we'll send you a new password.&lt;/p&gt;
                &lt;div class="small-12 columns"&gt;
                  &lt;input type="text" placeholder="Username or Email" /&gt;
                &lt;/div&gt;
                
                &lt;div class="small-12 columns"&gt;
                  &lt;button class="small"&gt;Email New Password&lt;/button&gt;
                &lt;/div&gt;
              &lt;/div&gt;
            &lt;/div&gt;
          </pre>
        </div>

      </div>
    </div>

    <?php include("global/foot.inc"); ?>
  </body>
</html>
