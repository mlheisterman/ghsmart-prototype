<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Test Page</title>

    <?php include("global/head.inc"); ?>
    <a href="/">&laquo; Home</a>

    <style type="text/css">
      body {
        padding: 40px;
      }

      .molecule {
        margin-bottom: 5em;
      }

      .molecule > label:first-child {
        color: #666;
        border-bottom: 1px solid #999;
        display: block;
        font-size: 11px;
        text-transform: uppercase;
        font-weight: bold;
        margin-bottom: 10px;
        padding-bottom: 4px;
      }

      small.d-block {
        color: #666;
        border-bottom: 1px dashed #999;
        margin-bottom: 10px;
      }
    </style>
  </head>
  <body>
    <nav>
      <ul class="inline-list">
        <li><a href="global.php">Global</a></li>
        <li><a href="atoms.php">Atoms</a></li>
        <li><a href="molecules.php"><strong>Molecules</strong></a></li>
        <li><a href="organisms.php">Organisms</a></li>
      </ul>
    </nav>

    <div class="clearfix martop-5"></div>

    <div class="molecule">
      <label>Text</label>
      <small class="d-block">Byline</small>
      <div class="byline">
        <p>by Nick Comito on Wednesday, May 20, 2015</p>
      </div>

      <small class="d-block">Address</small>
      <div class="vcard">
        <div class="org">Company Name</div>
        <div class="adr">
          <div class="street-address">
            1701 Directors Blvd.
          </div>
          <span class="locality">Austin</span>,
          <abbr class="region" title="Texas">TX</abbr>
          <span class="postal-code">78745</span>
          
          <div class="country-name">U.S.A.</div>
        </div>
        <div class="tel">+1.512.553.6830</div>
      </div>

      <small class="d-block">Heading Group</small>
      <hgroup>
        <h1>A primary heading for something big</h1>
        <h2>And some supporting text</h2>
      </hgroup>

      <small class="d-block">Blockquote with citation</small>
      <blockquote>
        Your insight, acute technical knowledge and calm assurance have provided me and the team immense confidence that we indeed selected the perfect partner for this project.
        <cite>Beth Green / CEO / Brief Media, Tulsa OK</cite>
      </blockquote>

      <small class="d-block">Intro text</small>
      <p class="intro-text">The beginning of a paragraph can be a useful opportunity to draw attention. We can use this space to highlight the summary of the proceeding copy.</p>
      <p>It’s almost a given that your business will outgrow an off-the-shelf app sooner or later. It tends to be a slow, painful process, full of workarounds and compromises. Until one day you realize how much it is slowing you down and limiting you. It isn’t working for you; you’re working for it.</p>
    </div>

    <div class="molecule">
      <label>Blocks</label>
      <small class="d-block">Headline + Byline</small>
      <div class="block headline-byline">
        <h3>An example of a headline</h3>
        <div class="byline">
          <p>by Nick Comito on Wednesday, May 20, 2015</p>
        </div>
      </div>
      <small class="d-block">Billboard</small>
      <div class="block billboard home">
        <img src="http://placehold.it/1280x720">
        <h1>The best product ever.</h1>
      </div>
      <small class="d-block">Call to action</small>
      <div class="cta">
        <h3>Call to action</h3>
        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Sed posuere consectetur est at lobortis. Nullam quis risus eget urna mollis ornare vel eu leo. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus.</p>
        <button>Button</button>
      </div>
    </div>

    <div class="molecule">
      <label>Media</label>
      <small class="d-block">Figure with caption</small>
      <figure>
        <img src="http://placehold.it/300x400">
        <figcaption>This is the caption of the image above</figcaption>
      </figure>
    </div>

    <div class="molecule">
      <label>Forms</label>
      <small class="d-block">Search</small>
      <div class="form search">
        <input type="text" class="inline" />
        <button class="inline text-medium"><i class="fa fa-search"></i></button>
      </div>

      <small class="d-block">Comment</small>
      <div class="form comment">
        <label for="name">Name</label>
        <input id="name" type="text" placeholder="Enter your name">
        <label for="email">Email</label>
        <input id="email" type="email" placeholder="Enter your email address">
        <label for="url">URL</label>
        <input id="url" type="url" placeholder="Enter your website">
        <label for="comment">Comment</label>
        <textarea id="comment" placeholder="Enter your comment(s)"></textarea>
      </div>

      <small class="d-block">Newsletter</small>
      <div class="form newsletter">
        <label>Sign up for our newsletter</label>
        <input type="email" class="inline" placeholder="Enter your email" />
        <button class="inline text-medium">Subscribe</button>
      </div>
    </div>

    <div class="molecule">
      <label>Navigation</label>
      <small class="d-block">Super</small>
      <div class="super-nav"><p>Logged in as <em>Firstname Lastname</em> <span class="glyphicons chevron-right textXSmall"></span><a href="">My&nbsp;Account</a><a href="/">Sign&nbsp;Out</a></p></div>

      <small class="d-block">Primary</small>
      <nav class="primary-nav">
        <ul class="inline-list">
          <li><a href="">Home</a></li>
          <li><a href="">About</a></li>
          <li><a href="">Services</a></li>
          <li><a href="">Products</a></li>
          <li><a href="">Contact</a></li>
        </ul>
      </nav>

      <div class="clearfix"></div>

      <small class="d-block">Footer</small>
      <nav class="footer-nav">
        <ul class="inline-list">
          <li><a href="">Home</a></li>
          <li><a href="">About</a></li>
          <li><a href="">Services</a></li>
          <li><a href="">Products</a></li>
          <li><a href="">Contact</a></li>
        </ul>
      </nav>
    </div>

    <div class="clearfix"></div>

    <div class="molecule">
      <label>Components</label>
      <small class="d-block">Social Share</small>
      <ul class="inline-list social-share">
        <li><a href="" class="button"><i class="fa fa-facebook"></i> Like</a></li>
        <li><a href="" class="button"><i class="fa fa-twitter"></i> Tweet</a></li>
        <li><a href="" class="button"><i class="fa fa-email"></i> Email</a></li>
      </ul>

      <div class="clearfix"></div>

      <small class="d-block">Single Comment</small>
      <div class="component comment">
        <p class="comment-body">Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

        <div class="comment-source">
          <img class="avatar" src="http://placehold.it/80x80">
          <span class="comment-name">Nick Comito</span>
        </div>
      </div>
    </div>

    <div class="molecule">
      <label>Messages</label>

      <small class="d-block">Alert</small>
      <div class="message alert">
        <p>This is an alert.</p>
      </div>

      <small class="d-block">Warning</small>
      <div class="message warning">
        <p>This is a warning.</p>
      </div>

      <small class="d-block">Success</small>
      <div class="message success">
        <p>This is a success message.</p>
      </div>

      <small class="d-block">Note</small>
      <div class="message note">
        <p>This is a note.</p>
      </div>


      <small class="d-block">Special</small>
      <div class="message special">
        <p>This is a special message.</p>
      </div>
    </div>
    
  </body>
</html>