<div class="navbar-fixed">
  <nav class="grey darken-1">
    <div class="nav-wrapper">
      <div class="container">
        <a href="#!" class="brand-logo">   
          <img src="/images/ghSMART-Logo_small2_retina.png" height="50px" width="195px" />
        </a>
        <a href="#" data-activates="mobile-nav" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="sass.html">Sass</a></li>
          <li><a href="badges.html">Components</a></li>
        </ul>
        <ul class="side-nav" id="mobile-nav">
          <li><a href="sass.html">Sass</a></li>
          <li><a href="badges.html">Components</a></li>
          <li><a href="collapsible.html">Javascript</a></li>
          <li><a href="mobile.html">Mobile</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>