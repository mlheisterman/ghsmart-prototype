<div class="navbar-fixed">
  <ul id="dropdown-login" class="dropdown-content">
    <li><a href="/dashboard.php#notifications" class="padright-5">Notifications<span class="new badge">4</span></a></li>
    <!--li><a href="/my-account.php">Profile</a></li-->
    <!--li><a href="/myaccount.php">Account</a></li-->
    <!--li><a href="/company.php">Company</a></li-->
    <!--li><a href="/company.php">Support</a></li-->
    <li><a href="/login.php">Sign out</a></li>
  </ul>
  <div class="row">
    <nav class="primary-color">
      <div class="nav-wrapper">
        <div class="col s12">
          <a href="/dashboard.php" class="brand-logo martop-base">   
            <img src="/images/logo-white.png" width="125px" />
          </a>
          <a href="#" data-activates="mobile-nav" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul class="right hide-on-med-and-down">
            <li><a href="/scorecards.php">Scorecards</a></li>
            <li><a href="/candidates.php">Candidates</a></li>
            <li><a href="/interviews.php">Interviews</a></li>
            <li><a href="/reports.php">Reports</a></li>
            <li class="border-left">
              <a class="dropdown-button padleft-5" href="#!" data-beloworigin="true" data-constrainwidth="false" data-activates="dropdown-login">
                <img src="/images/geoff_smart.jpg" class="avatar circle" alt="Geoff Smart">Geoff Smart<i class="material-icons right">arrow_drop_down</i>
              </a>
            </li>
            <li>
              <form>
                <div class="input-field">
                  <input id="search" type="search" required>
                  <label for="search"><i class="material-icons">search</i></label>
                </div>
              </form>
            </li>
          </ul>
          <ul class="side-nav" id="mobile-nav">
            <li><a href="/scorecards.php">Scorecards</a></li>
            <li><a href="/candidates.php">Candidates</a></li>
            <li><a href="/interviews.php">Interviews</a></li>
            <li><a href="/reports.php">Reports</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</div>