<div class="row">
  <h4>Modal Header</h4>
  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header"><i class="material-icons">visibility</i>Overview</div>
      <div class="collapsible-body">
        <p>
          Thank you for reviewing your ClickModel&trade;. This is an iterative process and the end hope is that you have a prototype that will enable you to proceed with confidence into development. Additionally, this prototype and all the associated documentation will give the development team all the instruction they need to effectively build an application that meets the needs of your business as they were at the time this was completed.
        </p>
        <p>
          <strong>Note* </strong>This prototype is <em>not</em> is a visual design representation (no colors, type treatment, etc). Visual design is a separate process and, while this same code base will typically be used to apply design, we are not yet to that point. Please feel free to email the team with any changes you would like to behavior, workflow or general layout.
        </p>
      </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">contacts</i>Team Information</div>
      <div class="collapsible-body">
        <ul>
          <li><strong>[Project Manager]</strong> [email] (Project Manager)</li>
          <li><strong>[UX Architect]</strong> [email] (UX Architect)</li>
          <li><strong>[UX Designer]</strong> [email] (UX/UI Designer)</li>
        </ul>
      </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">navigation</i>ClickModel Navigation</div>
      <div class="collapsible-body">
        <ul>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
        </ul>
      </div>
    </li>
  </ul>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
  <a class="btn-floating btn-large red">
    <i class="large material-icons">mode_edit</i>
  </a>
  <ul>
    <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></i></a></li>
    <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
    <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
    <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
  </ul>
</div>