      <!-- Competencies Modal Body -->
      <div id="compmodal" class="modal bottom-sheet">
        <div class="modal-content">
          <h5>Competencies</h5>
          <div class="col s12 m12">
            <table class="bordered white"><!-- BEGIN Table -->
              <thead>
                <tr>
                  <th>Rating</th>
                  <th>Competency</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input name="efficiency" type="radio" id="eff-a" /><label for="eff-a" class="marright-2">A</label> <input name="efficiency" type="radio" id="eff-b" /><label for="eff-b" class="marright-2">B</label> <input name="efficiency" type="radio" id="eff-c" /><label for="eff-c">C</label>
                  </td>
                  <td>
                    <b>Efficiency</b>
                  </td>
                  <td>
                    <p>Able to produce significant output with minimal wasted effort.</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="enthusiasm" type="radio" id="enth-a" /><label for="enth-a" class="marright-2">A</label> <input name="enthusiasm" type="radio" id="enth-b" /><label for="enth-b" class="marright-2">B</label> <input name="enthusiasm" type="radio" id="enth-c" /><label for="enth-c">C</label>
                  </td>
                  <td>
                    <b>Enthusiasm</b>
                  </td>
                  <td>
                    <p>Exhibits passion and excitement over work. Has a can-do attitude.</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="worke" type="radio" id="worke-a" /><label for="worke-a" class="marright-2">A</label> <input name="worke" type="radio" id="worke-b" /><label for="worke-b" class="marright-2">B</label> <input name="worke" type="radio" id="worke-c" /><label for="worke-c">C</label>
                  </td>
                  <td>
                    <b>Work Ethic</b>
                  </td>
                  <td>
                    <p>Possesses a strong willingness to work hard and sometimes long hours to get the job done. Has a track record of working hard.</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="aggressiveness" type="radio" id="agg-a" /><label for="agg-a" class="marright-2">A</label> <input name="aggressiveness" type="radio" id="agg-b" /><label for="agg-b" class="marright-2">B</label> <input name="aggressiveness" type="radio" id="agg-c" /><label for="agg-c">C</label>
                  </td>
                  <td>
                    <b>Aggressiveness</b>
                  </td>
                  <td>
                    <p>Moves quickly and takes a forceful stand without being overly abrasive.</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="creative" type="radio" id="create-a" /><label for="create-a" class="marright-2">A</label> <input name="creative" type="radio" id="create-b" /><label for="create-b" class="marright-2">B</label> <input name="creative" type="radio" id="create-c" /><label for="create-c">C</label>
                  </td>
                  <td>
                    <b>Creativity/Innovation</b>
                  </td>
                  <td>
                    <p>Generates new and innovative approaches to problems.</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="standards" type="radio" id="stand-a" /><label for="stand-a" class="marright-2">A</label> <input name="standards" type="radio" id="stand-b" /><label for="stand-b" class="marright-2">B</label> <input name="standards" type="radio" id="stand-c" /><label for="stand-c">C</label>
                  </td>
                  <td>
                    <b>High Standards</b>
                  </td>
                  <td>
                    <p>Expects personal performance and team performance to be nothing short of the best.</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>