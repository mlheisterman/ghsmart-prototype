<!-- THIS FILE HOLDS GLOBALLY LOADED STYLES, JAVASCRIPTS, AND META INFORMATION -->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Grab the font awesome icons -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<!-- Modernizr -->
<script src="/js/modernizr.min.js"></script>

<!-- Load jQuery early -->
<script src="/bower_components/jquery/dist/jquery.js"></script>

<!-- Load Select2 -->
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet type="text/css">
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<!-- Load one-fild CC Styles -->
<!-- <link href="/stylesheets/paymentInfo.css" rel="stylesheet" type="text/css"> -->

<!-- Load Froala WYSIWYG Editor CSS -->
<link href="/bower_components/froala/css/froala_editor.min.css" rel="stylesheet" type="text/css" media="screen,projection">
<link href="/bower_components/froala/css/froala_style.min.css" rel="stylesheet" type="text/css" media="screen,projection">

<!-- Load the project-specific CSS -->
<link href="/stylesheets/app.css?version=1" rel="stylesheet" type="text/css" media="screen,projection">