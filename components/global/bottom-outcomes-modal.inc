      <!-- Outcomes Modal Body -->
      <div id="outcomemodal" class="modal bottom-sheet">
        <div class="modal-content">
          <h5>Outcomes</h5>
          <div class="col s12 m12">
            <table class="bordered white inner-list"><!-- BEGIN Table -->
              <thead>
                <tr>
                  <th>Rating</th>
                  <th>Outcome</th>
                  <th>Details</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input name="testcoverage" type="radio" id="test-a" /><label for="test-a" class="marright-2">A</label> <input name="testcoverage" type="radio" id="test-b" /><label for="test-b" class="marright-2">B</label> <input name="testcoverage" type="radio" id="test-c" /><label for="test-c">C</label>
                  </td>
                  <td>
                    <p>Maintain 60% test coverage on all code released.</p>
                  </td>
                  <td>
                    <ul>
                      <li>Tracking coverage with codecy, we can see how well your code is performing.</li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="velocity" type="radio" id="velo-a" /><label for="velo-a" class="marright-2">A</label> <input name="velocity" type="radio" id="velo-b" /><label for="velo-b" class="marright-2">B</label> <input name="velocity" type="radio" id="velo-c" /><label for="velo-c">C</label>
                  </td>
                  <td>
                    <p>Produce at a velocity that matches or exceeds the rest of the team.</p>
                  </td>
                  <td>
                    <ul>
                      <li>Building and sizing a backlog is very important as it is an indicator of your attention to detail.</li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="toolkit" type="radio" id="toolkit-a" /><label for="toolkit-a" class="marright-2">A</label> <input name="toolkit" type="radio" id="toolkit-b" /><label for="toolkit-b" class="marright-2">B</label> <input name="toolkit" type="radio" id="toolkit-c" /><label for="toolkit-c">C</label>
                  </td>
                  <td>
                    <p>Add one coding language or framework to our toolkit per year.</p>
                  </td>
                  <td>
                    <ul>
                      <li>We already have PHP, Objective C, Ruby, HTML, CSS, and mySQL javascript languages.  As for frameworks, we have Angular, Ember, Drupal, sails, mongoDB, and Solr.</li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input name="sprint" type="radio" id="sprint-a" /><label for="sprint-a" class="marright-2">A</label> <input name="sprint" type="radio" id="sprint-b" /><label for="sprint-b" class="marright-2">B</label> <input name="sprint" type="radio" id="sprint-c" /><label for="sprint-c">C</label>
                  </td>
                  <td>
                    <p>Improve adherence to sprint point commitments to 90%.</p>
                  </td>
                  <td>
                    <ul>
                      <li>Our teams are currently at about 85%.</li>
                    </ul>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>