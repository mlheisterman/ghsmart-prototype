                  <h5 class="marbot-1">What were you hired to do? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Build a mental image of their scorecard.What were their mission and key outcomes? What competencies might have mattered? How was your success measured in your role?"><i class="material-icons">info_outline</i></a></h5>
                  <form>
                    <textarea class="edit text-small" name="do-1" rows="6"></textarea>
                  </form>  
                  <h5 class="marbot-1">What accomplishments are you most proud of? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="A players usually talk about OUTCOMES linked to expectations.  B and C players talk about people, events, etc."><i class="material-icons">info_outline</i></a></h5>
                  <form>
                    <textarea class="edit text-small" name="proud-1" rows="6"></textarea>
                  </form> 
                  <h5 class="marbot-1">What were some low points during that job? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="EVERYone has work lows.  If you have trouble getting an answer, reframe: What was your biggest mistake? What would you have done differently? In what ways were your peers stronger than you?"><i class="material-icons">info_outline</i></a></h5>
                  <form>
                    <textarea class="edit text-small" name="low-1" rows="6"></textarea>
                  </form> 
                  <h5 class="marbot-1">Who were the people you worked with? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="This is where the precision and order of the questions really matters.  Follow the questions exactly as they are prepopulated in the textbox.  The portion below the rule is only for people who have managed others."><i class="material-icons">info_outline</i></a></h5>
                  <form>
                    <textarea class="edit text-small" name="low-1" rows="6"><h5>What was your bosses name? How do you spell that?</h5><h5>What was it like working with him/her?</h5><h5>What will he/she tell me were your biggest strengths and areas for improvement?</h5><hr><h5>How would you rate the team you inherited on an A, B, C scale?</h5><h5>Did you hire/fire anybody?</h5><h5>How would you rate the team when you left on an A, B, C scale?</h5></textarea>
                  </form> 
                  <h5 class="marbot-1">Why did you leave that job? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="AVOID judgement.  Be curious.  Do not accept vague answers."><i class="material-icons">info_outline</i></a></h5>
                  <form class="marbot-2">
                    <textarea class="edit text-small" name="low-1" rows="6"></textarea>
                  </form> 