      <footer class="section">
        <p class="center-align col s12 m6 offset-m3 l4 offset-l4">&copy;2015 All Rights Reserved.<br>by ghSMART &amp; Company, Inc.<br><a class="fake-link" href="#!">Privacy and Terms</a></p>
      </footer>
      <div class="black white-text marbot-0 martop-0" style="position: fixed; bottom: 0px; left: 0px; width: 100%; cursor: not-allowed;">
        <div class="container">
          <div class="col">
            <h6 class="martop-0 padall-base center-align">
              <i class="material-icons">warning</i>
              This is a prototype.  Many links in this do not connect to anything. Dead links will show the "not allowed" cursor.
              <i class="material-icons">warning</i>
            </h6>
          </div>
        </div>
      </div>