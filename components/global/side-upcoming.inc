        <div class="col s12 m3"> <!-- BEGIN LEFT COLUMN -->
          <div class="section"> <!-- BEGIN Upcoming -->
            <div class="row padtop-4">
              <h5>Today's Interviews</h5>
              <table class="bordered responsive-table">
                <thead class="hide-on-med-and-up">
                  <tr>
                    <th data-field="id">Time</th>
                    <th data-field="name">Candidate</th>
                    <th data-field="price">Type</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>10:00a<br><a target="_blank" href="/reference-interview-albert-norris.php" class="btn tiny">Start</a></td>
                    <td><a href="/candidate-albert-norris.php">Albert Norris</a></td>
                    <td>Reference<br>John Franklin</td>
                  </tr>
                  <tr>
                    <td>11:00a<br><a href="#!" class="fake-link btn tiny">Start</a></td>
                    <td><a href="#!" class="fake-link">Jack Smith</a></td>
                    <td>Topgrading</td>
                  </tr>
                  <tr>
                    <td>02:00p<br><a href="#!" class="fake-link btn tiny">Start</a></td>
                    <td><a href="#!" class="fake-link">Mansi Puri</a></td>
                    <td>Focus</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div> <!-- END Upcoming -->
          <div class="section"> <!-- BEGIN Activity Log -->
            <div class="row">
              <h5>Activity</h5>
              <table class="bordered responsive-table">
                <thead class="hide-on-med-and-up">
                  <tr>
                    <th data-field="id">Time</th>
                    <th data-field="name">Candidate</th>
                    <th data-field="price">Type</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><label>Yesterday</label></td>
                    <td><a href="#" class="fake-link">John McInroe</a></td>
                    <td>Screening</td>
                  </tr>
                  <tr>
                    <td><label>Yesterday</label></td>
                    <td><a href="#" class="fake-link">Jack Smith</a></td>
                    <td>Topgrading</td>
                  </tr>
                  <tr>
                    <td><label>Tuesday</label></td>
                    <td><a href="#" class="fake-link">Albert Gonzalez</a></td>
                    <td>Focus</td>
                  </tr>
                </tbody>
              </table>
            </div>        
          </div> <!-- END Activity Log -->
          <div class="section">
            <img src="/images/ad_powerscore.png" />
          </div>
        </div> <!-- END Left Column -->