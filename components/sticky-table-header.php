<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Test Page</title>
    <!-- Grab the prettify script to output HTML Code -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?linenums=false"></script>

    <?php include("global/head.inc"); ?>
  </head>
  <body>      
    <div class="row">
      <div class="large-12 columns">
        <a href="patterns.php">&laquo; Go Back</a>
      </div>
    </div>
    
    <div class="row">
      <div class="small-12 columns">
        <div class="island marbot-5">
          <div class="island-header">
            <h1>Sticky Table Header</h1>
          </div>
          <div class="island-contents">
            
          </div>

          <table class="sticky-header">
            <thead>
              <tr>
                <th class="data-heading"><i class="fa fa-arrow-down"></i></th>
                <th class="data-heading">Player Name</th>
                <th class="data-heading">Team</th>
                <th class="data-heading medium-text-center">Seed</th>
                <th class="data-heading medium-text-right">Points PG</th>
                <th class="data-heading medium-text-right">3-Pointers PG</th>
                <th class="data-heading medium-text-right">3-Point %</th>
                <th class="data-heading medium-text-right">Minutes PG</th>
                <th class="data-heading">Health Status</th>
                <th class="data-heading">19 Picks Left</th>
              </tr>
            </thead>
            <tbody>
              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>1</td>
                <td>Harvey, Tyler</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">4</td>
                <td class="medium-text-right">23.4</td>
                <td class="medium-text-right">4.3</td>
                <td class="medium-text-right">47.6%</td>
                <td class="medium-text-right">36.2</td>
                <td>Healthy</td>
                <td class="medium-text-center"><i class="fa fa-user"></i></td>
              </tr>

              <tr class="not-picked even">
                <td>2</td>
                <td>Hawes, Jack</td>
                <td>BYU</td>
                <td class="medium-text-right">6</td>
                <td class="medium-text-right">22.3</td>
                <td class="medium-text-right">1.1</td>
                <td class="medium-text-right">38.7%</td>
                <td class="medium-text-right">39.4</td>
                <td>Injured</td>
                <td></td>
              </tr>

              <tr class="not-picked odd">
                <td>3</td>
                <td>Woodley, Zeek</td>
                <td>Northwestern</td>
                <td class="medium-text-right">8</td>
                <td class="medium-text-right">22.1</td>
                <td class="medium-text-right">2.1</td>
                <td class="medium-text-right">39.4%</td>
                <td class="medium-text-right">34.5</td>
                <td>Healthy</td>
                <td></td>
              </tr>

              <tr class="not-picked even">
                <td>4</td>
                <td>Jones, Jim</td>
                <td>Eastern Wash.</td>
                <td class="medium-text-right">3</td>
                <td class="medium-text-right">25.4</td>
                <td class="medium-text-right">6.3</td>
                <td class="medium-text-right">49.6%</td>
                <td class="medium-text-right">39</td>
                <td>Healthy</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <?php include("global/foot.inc"); ?>
  </body>
</html>
