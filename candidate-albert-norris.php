<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Candidate Albert Norris</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="container">
      <div class="row">
        <?php include("components/global/side-upcoming.inc"); ?>
        <div class=" col s12 m9"> <!-- BEGIN Right Column -->
          <div class="section">
            <a href="dashboard.php">Dashboard</a> // <a href="candidates.php">Candidates</a> // Albert Norris
          </div>
          <div class="section white padall-2">
            <div class="row"> <!-- BEGIN Section Header -->
              <h4 class="col s12 martop-0">Albert Norris <span><a  class="btn tiny" href="/candidate-albert-norris-edit.php">edit</a></span></h4>
              <div class="col s6 m3 l2">
                <img class="circle" src="/images/albert.jpg">
              </div>
              <div class="col s6 m9 l10">
                <div class="col m6">
                  <table>
                    <tbody>
                      <tr>
                        <td class="text-top"><b>Scorecard:</b>
                        <td><a href="/scorecard-developer.php">Developer</a></td>
                      </tr>
                      <tr>
                        <td class="text-top"><b>Source:</b></td>
                        <td>Professional Reference - Josh Gillespie</td>
                      </tr>
                      <tr>
                        <td class="text-top"><b>Address:</b></td>
                        <td>1234 Main Street<br>Austin, TX 78749</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col m6">
                  <table>
                    <tbody>
                        <td class="text-top"><b>Email:</b></td>
                        <td><a href="mailto:albert.norris@microsoft.com">albert.norris@microsoft.com</a></td>
                      </tr>
                      <tr>
                        <td class="text-top"><b>Phone:</b></td>
                        <td><a href="tel://1-555-555-5555">+1 (555) 555-5555</a></td>
                      </tr>
                      <tr>
                        <td class="text-top"><b>Linkedin Profile:</b></td>
                        <td><a target="_blank" href="https://www.linkedin.com/in/albertcmartin">linkedin.com/in/albertcmartin</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div> <!-- END Section Header -->
            
            <div class="section">
              <div class="row martop-1">
                <h5 class="col s12 m4">Interview Progress</h5>
                <p class="col s12 m8 right-align">Skill-Will Match Prediction: <strong class="good-color">94<sup>%</sup></strong> &nbsp;&nbsp;&nbsp; Offer Acceptance Prediction: <strong class="bad-color">69<sup>%</sup></strong><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="These numbers are based on your historical performance which you can view on the reports page."><i class="material-icons">info_outline</i></a></p>
                <div class="col s12">
                  <div class="col s4 m2 relative">
                    <div class="left progression martop-2 marleft-1"></div>
                    <div class="interview left card blue lighten-2">
                      <div class="card-content white-text center-align">
                        <h3 class="martop-0">93<sup>%</sup></h3>
                        <label class="uppercase white-text">Screening</label><br>
                        <label class="uppercase white-text">Jul 7, 2015</label>
                      </div>
                      <div class="card-action center-align grey darken-3">
                        <a class="blue-text marright-0" target="_blank" href="/screening-interview-albert-norris.php">view</a>
                      </div>
                    </div>
                  </div>
                  <div class="col s4 m2 relative">
                    <div class="left progression martop-2 marleft-1"></div>
                    <div class="interview left card blue lighten-1">
                      <div class="card-content white-text center-align">
                        <h3 class="martop-0">91<sup>%</sup></h3>
                        <label class="uppercase white-text">Topgrading</label><br>
                        <label class="uppercase white-text">Jul 14, 2015</label>
                      </div>
                      <div class="card-action center-align grey darken-3">
                        <a class="blue-text marright-0" target="_blank" href="/topgrading-interview-albert-norris.php">view</a>
                      </div>
                    </div>
                  </div>
                  <div class="col s4 m2 relative">
                    <div class="left progression martop-2 marleft-1"></div>
                    <div class="interview left card blue lighten-1">
                      <div class="card-content white-text center-align">
                        <h3 class="martop-0">97<sup>%</sup></h3>
                        <label class="uppercase white-text">Focus</label><br>
                        <label class="uppercase white-text">Today</label>
                      </div>
                      <div class="card-action center-align grey darken-3">
                        <a class="blue-text marright-0" target="_blank" href="/focus-interview-albert-norris.php">View</a>
                      </div>
                    </div>
                    <div class="left progression martop-2 marleft-1"></div>
                  </div>
                  <div class="col s4 m2 relative">
                    <div class="left progression martop-2 marleft-1"></div>                
                    <div class="interview left card grey lighten-1">
                      <div class="card-content grey-text center-align">
                        <h3 class="martop-0">N/A</h3>
                        <label class="uppercase">References</label><br>
                        <label class="uppercase">Various</label>
                      </div>
                      <div class="card-action center-align grey darken-3">
                        <a class="blue-text marright-0" target="_blank" target="_blank" href="/reference-interview-albert-norris.php">Start</a>
                      </div>
                    </div>
                  </div>
                  <div class="col s4 m2 relative">
                    <div class="left progression martop-2 marleft-1"></div>
                    <div class="interview left card grey lighten-1">
                      <div class="card-content grey-text center-align">
                        <h3 class="martop-0">N/A</h3>
                        <label class="uppercase">Offer</label><br>
                        <label class="uppercase">&nbsp;</label>
                      </div>
                      <div class="card-action center-align grey">
                        <a class="grey-text marright-0" href="#">&nbsp;</a>
                      </div>
                    </div>
                  </div>
                  <div class="col s4 m2 relative">
                    <div class="interview left card grey lighten-1">
                      <div class="card-content grey-text center-align">
                        <h3 class="martop-0">N/A</h3>
                        <label class="uppercase">Hire</label><br>
                        <label class="uppercase">&nbsp;</label>
                      </div>
                      <div class="card-action center-align grey">
                        <a class="grey-text marright-0" href="#">&nbsp;</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="section">
              <div class="row martop-1">
                <h5 class="col s12 m6">References Given</h5>
                <table class="hoverable white"><!-- BEGIN Table -->
                  <thead>
                    <tr>
                      <th data-field="interview">Interview</th>
                      <th data-field="name">Name</th>
                      <th data-field="title">Title</th>
                      <th data-field="company">Company</th>
                      <th data-field="email">Email</th>
                      <th data-field="phone">Phone</th>
                      <th data-field="meeting">Meeting</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>Screening</td>
                      <td>John Franklin</td>
                      <td>Lead Developer</td>
                      <td>Oracle</td>
                      <td><a href="mailto:john.franklin@microsoft.com">Email</td>
                      <td><a href="tel://1-555-555-5555">+1 (555) 555-5555</a></td>
                      <td><a href="/reference-interview-albert-norris.php">07/17/2015</a></td>
                    </tr>
                    <tr>
                      <td>Screening</td>
                      <td>Ben Jackson</td>
                      <td>Developer</td>
                      <td>Microsoft</td>
                      <td><a href="mailto:ben.jackson@microsoft.com">Email</td>
                      <td><a href="tel://1-555-555-5555">+1 (555) 555-5555</a></td>
                      <td>Not Scheduled</td>
                    </tr>
                    <tr>
                      <td>Screening</td>
                      <td>Sally Loyd</td>
                      <td>Developer</td>
                      <td>Microsoft</td>
                      <td><a href="mailto:sally.loyd@microsoft.com">Email</td>
                      <td><a href="tel://1-555-555-5555">+1 (555) 555-5555</a></td>
                      <td>Not Scheduled</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            
            <div class="section">
              <div class="row martop-1">
                <h5 class="col s12"><a href="https://www.linkedin.com/in/albertcmartin" target="_blank"><i class="fa fa-linkedin-square"></i></a> LinkedIn Job History</h5>
                <table class="hoverable white">
                  <thead>
                    <tr>
                      <th data-field="id">Years</th>
                      <th data-field="name">Position</th>
                      <th data-field="price">Company</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>2010-2015</td>
                      <td>Senior Developer</td>
                      <td>Oracle</td>
                    </tr>
                    <tr>
                      <td>2007-2010</td>
                      <td>Junior Developer</td>
                      <td>Microsoft</td>
                    </tr>
                    <tr>
                      <td>2006-2007</td>
                      <td>Intern</td>
                      <td>Microsoft</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </main>
        <?php include("components/global/footer.inc"); ?>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
