<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Test Page</title>
    <!-- Grab the prettify script to output HTML Code -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?linenums=false"></script>

    <?php include("includes/head.inc"); ?>
  </head>
  <body>      
    <div class="row">
      <div class="large-12 columns">
        <a href="/">&laquo; Home</a>
      </div>
    </div>
    
    <div class="row">
      <div class="small-12 columns">
        <div class="island marbot-5">
          <div class="island-header">
            <h1>Playground.</h1>
          </div>
          <div class="island-contents">
            <p>Pull in some HTML snippets from the <a href="components.php" target="_blank">components</a>, enable the corresponding CSS, and see it in action.</p>
          </div>
        </div>
      </div>
    </div>

    <!-- Start Playtime -->
    <!-- This is the playground. Start Pasting some HTML. -->
    <div class="row">
      

    </div>
    


    <!-- End Playtime -->

    <?php include("includes/foot.inc"); ?>
  </body>
</html>
