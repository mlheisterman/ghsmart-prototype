<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Topgrading Interview - Albert Norris</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="row">
      <div id="info-bar" class="col s12 m4 l3">
        <div class="sidebar">
          <div class="row">
            <div class="col m4">
              <img class="circle" src="/images/albert.jpg">
            </div>
            <div class="col m8">
              <h5 class="martop-0"><a href="/candidate-albert-norris.php">Albert Norris</a></h5>
              <ul>
                <li><strong>Scorecard:</strong> <a href="/scorecard-developer.php">Developer</a></li>
                <li><strong>Appt:</strong> 07/14/2015 (<a href="mailto:albert.norris@microsoft.com">email</a>) (<a href="tel://1-555-555-5555">phone</a>)</li>
              </ul>
            </div>
            <div class="col m12">
              <a class="waves-effect waves-light btn left tiny modal-trigger" style="margin-right: 4px;" href="#compmodal">Competencies</a>
              <a class="waves-effect waves-light btn left tiny modal-trigger" style="margin-right: 4px;" href="#outcomemodal">Outcomes</a><!-- Competencies Modal Trigger -->
              <a class="waves-effect waves-light btn tiny modal-trigger" href="#refmodal">References</a> <!-- Reference Modal Trigger -->
            </div>
          </div>
          <div class="col s12">
            <h6 class="martop-0"><a href="https://www.linkedin.com/in/albertcmartin" target="_blank"><i class="fa fa-linkedin-square" style="font-size: 1.25rem;"></i></a> Job History</h6>
            <table class="no-bg">
              <thead>
                <tr>
                    <th data-field="id">Years</th>
                    <th data-field="name">Position</th>
                    <th data-field="price">Company</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>2010-2015</td>
                  <td>Senior Developer</td>
                  <td>Oracle</td>
                </tr>
                <tr>
                  <td>2007-2010</td>
                  <td>Junior Developer</td>
                  <td>Microsoft</td>
                </tr>
                <tr>
                  <td>2006-2007</td>
                  <td>Intern</td>
                  <td>Microsoft</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col hide-on-small-only s12">
            <h5 class="marbot-0 martop-2">Jump to Jobs</h5>
            <ul class="table-of-contents">
              <li><a href="#oracle-dev">Oracle - Senior Developer</a></li>
              <li><a href="#micro-dev">Microsoft - Junior Developer</a></li>
              <li><a href="#micro-intern">Microsoft - Intern</a></li>
              <li><a href="#general">General Notes</a></li>
            </ul>
          </div>
        </div>
      </div>     

      <div class="col s12 m8 l9 offset-l3 marbot-5">
        <div class="section">
          <a href="dashboard.php">Dashboard</a> // <a href="/interviews.php">Interviews</a> // Topgrading - Albert Norris
        </div>
        <div class="section">
          <div class="row">
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div id="oracle-dev" class="scrollspy collapsible-header"><i class="material-icons">work</i>Oracle - Senior Developer</div>
                <div class="collapsible-body padall-1">
                    <?php include("components/global/interview-tg-questions.inc"); ?>
                </div>
              </li>
              <li>
                <div id="micro-dev" class="scrollspy collapsible-header"><i class="material-icons">work</i>Microsoft - Junior Developer</div>
                <div class="collapsible-body padall-1">
                    <?php include('components/global/interview-tg-questions.inc'); ?>
                </div>
              </li>
              <li>
                <div id="micro-intern" class="scrollspy collapsible-header"><i class="material-icons">work</i>Microsoft - Intern</div>
                <div class="collapsible-body padall-1">
                    <?php include('components/global/interview-tg-questions.inc'); ?>
                </div>
              </li>
            </ul>
          </div>
          <a href="#!" class="btn waves-effect waves-light"><i class="material-icons left">library_add</i>Add another job</a>
        </div>
        <div id="general" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">General Notes</h5>
            <form>
              <textarea class="edit text-small" name="test"></textarea>
            </form> 
          </div>
        </div>
        <div id="general" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">Candidate's Topgrading Interview Checklist and Score <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Only those who have both checks and a score of 90+ go on to the next interview."><i class="material-icons">info_outline</i></a></h5>
            <form>
              <div class='col s12 white z-depth-1'>
                <div class="input-field col s6 m5 border-right">
                  <input type="checkbox" class="filled-in" id="push-pull" />
                  <label for="push-pull">Candidate was pulled, not pushed, to different jobs.</label>
                  <input type="checkbox" class="filled-in" id="references" />
                  <label for="references">You have recorded all references <a class="modal-trigger" href="#refmodal">here</a> with correct spelling.</label>
                </div>
                <div class="input-field col s6 m2 martop-2">
                  <input placeholder="1-100" id="score" type="number" min="1" max="100">
                  <label for="score">Topgrading Score</label>
                </div>
                <div class="input-field col s6 m3 martop-2">
                  <a class="btn disabled">Promote to Focus</a>
                </div>
              </div>
            </form> 
          </div>
        </div>
        <div class="fixed-action-btn" style="bottom: 50px; left: 24px;">
          <h6 class="good-color">last autosave: 12:43am</h6>
        </div>
      </div>
      
      <!-- Reference Modal Body -->
      <div id="refmodal" class="modal bottom-sheet">
        <div class="modal-content">
          <h5>References</h5>
          <div class="col s12 m6 border-right">
            <ul>
              <li>John Franklin (<a class="fake-link" href="#!">edit</a>)</li>
              <li>Ben Jackson (<a class="fake-link" href="#!">edit</a>)</li>
              <li>Sally Loyd (<a class="fake-link" href="#!">edit</a>)</li>
            </ul>
          </div>
          <div class="col s12 m6">
            <form>
              <div class="input-field col s12">
                <input placeholder="Reference Name" id="name" type="text">
                <label for="name">Add a Reference</label>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer no-bg">
          <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Save/Close</a>
        </div>
      </div>
        <?php include("components/global/bottom-competencies-modal.inc"); ?>
        <?php include("components/global/bottom-outcomes-modal.inc"); ?>
    </main>
    <?php include("components/global/footer.inc"); ?>
    <?php include("components/global/foot.inc"); ?>
  </body>
</html>
