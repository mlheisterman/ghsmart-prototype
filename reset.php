<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Core</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <div class="absolute height-20 bg-image top business"></div>
    <div class="container padtop-6">
      <div class="section">
        <div class="row">
          <form class="col s12 m6 offset-m3 l4 offset-l4 z-depth-1 white">
            <div class="center-align section">
              <img src="/images/logo-color.png" />
            </div>
            <div class="row">
              <div class="input-field col s12 m10 offset-m1">
                <input id="username" type="text"  data-error="Please enter your username." data-success="Thank you." class="validate">
                <label for="username">Username</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m7 offset-m1">
                <input id="password" type="password" data-error="Please enter your password." data-success="Thank you." class="validate">
                <label for="password">Password</label>
              </div>
              <div class="input-field col s12 m4">
                <input type="checkbox" class="filled-in" id="show-pass" checked="checked" />
                <label for="show-pass">Show</label>
              </div>
            </div>
            <a href="" class="col s12 m5 offset-m1 waves-effect waves-light btn"><i class="material-icons left"></i>Log In</a>
            <div class="col s12 m5 offset-m1 martop-base">
              <input type="checkbox" class="filled-in" id="stay-logged" checked="checked" />
              <label for="stay-logged">Stay logged in</label>
            </div>
            <div class="row">
              <p class="center-align col s12"><a href="">Create an account</a> &bullet; <a href="">Trouble logging in?</a>
            </div>
          </form>
        </div>
      </div>
      <div class="section">
        <p class="center-align col s12 m6 offset-m3 l4 offset-l4">&copy;2015 All Rights Reserved.<br>by ghSMART &amp; Company, Inc.<br><a href="">Privacy and Terms</a></p>
      </div>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
