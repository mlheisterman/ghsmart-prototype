<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Core</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <div class="container">
      <div class="section">
        <div class="row">
          <h5>Reports</h5>
        </div>

        <div class="row marbot-4">
          <div class="metrics-container">
            <div class="col s12 m6 l3 center">
              <div class="circle-container">
                <h3 class="martop-0 marbot-0"><strong>98</strong></h3>
                <label class="uppercase dark-color">Candidates</label>
              </div>
            </div>
            <div class="col s12 m6 l3 center">
              <div class="circle-container">
                <h3 class="martop-0 marbot-0"><strong>42</strong></h3>
                <label class="uppercase dark-color">Interviewed</label>
              </div>
            </div>
            <div class="col s12 m6 l3 center">
              <div class="circle-container">
                <h3 class="martop-0 marbot-0"><strong>17</strong></h3>
                <label class="uppercase dark-color">Offered</label>
              </div>
            </div>
            <div class="col s12 m6 l3 center">
              <div class="circle-container">
                <h3 class="martop-0 marbot-0"><strong>7</strong></h3>
                <label class="uppercase dark-color">Remaining</label>
              </div>
            </div>
          </div>
        </div>

        <div class="row marbot-4">
          <div class="process-container">
            <div class="col s12 m4 l2 center dark padall-1">
              <label class="uppercase">Screening</label>
            </div>
            <div class="col s12 m4 l2 center dark padall-1">
              <label class="uppercase">Topgrading</label>
            </div>
            <div class="col s12 m4 l2 center dark padall-1">
              <label class="uppercase">Focus</label>
            </div>
            <div class="col s12 m4 l2 center dark padall-1">
              <label class="uppercase">Reference</label>
            </div>
            <div class="col s12 m4 l2 center dark padall-1">
              <label class="uppercase">Offers</label>
            </div>
            <div class="col s12 m4 l2 center dark padall-1">
              <label class="uppercase">Hires</label>
            </div>
          </div>

          <div class="process-container">
            <div class="col s12 m4 l2 center white padall-1 relative">
              <h4 class="marbot-0 martop-0">100</h4>
              <label>Produced</label>
              <i class="material-icons right absolute">trending_flat</i>
            </div>
            <div class="col s12 m4 l2 center white padall-1 relative">
              <h4 class="marbot-0 martop-0">100</h4>
              <label>Produced</label>
              <i class="material-icons right absolute">trending_flat</i>
            </div>
            <div class="col s12 m4 l2 center white padall-1 relative">
              <h4 class="marbot-0 martop-0">100</h4>
              <label>Produced</label>
              <i class="material-icons right absolute">trending_flat</i>
            </div>
            <div class="col s12 m4 l2 center white padall-1 relative">
              <h4 class="marbot-0 martop-0">100</h4>
              <label>Produced</label>
              <i class="material-icons right absolute">trending_flat</i>
            </div>
            <div class="col s12 m4 l2 center white padall-1 relative">
              <h4 class="marbot-0 martop-0">100</h4>
              <label>Produced</label>
              <i class="material-icons right absolute">trending_flat</i>
            </div>
            <div class="col s12 m4 l2 center white padall-1 relative">
              <h4 class="marbot-0 martop-0">100</h4>
              <label>Produced</label>
              <i class="material-icons right absolute">&nbsp;</i>
            </div>
          </div>
        </div>

        <div class="row marbot-4">
          <div class="col s12 l4 center">
            <hgroup>
              <h3 class="bad-color"><strong>33</strong></h3>
              <h5>Candidates <span class="bad-color">Dismissed</span></h5>
            </hgroup>
          </div>
          <div class="col s12 l4 center">
            <span>! Chart goes here !</span>
            <!-- Drop the chart in here -->
          </div>
          <div class="col s12 l4 center">
            <hgroup>
              <h3 class="good-color"><strong>13</strong></h3>
              <h5>Candidates <span class="good-color">Accepted</span></h5>
            </hgroup>
          </div>
        </div>

        <div class="row">
          <div class="col s12 m6 center">
            <h2 class="bad-color">75%</h2>
            <h5 class="bad-color">Lost</h5>
          </div>
          <div class="col s12 m6 center">
            <h2 class="good-color">25%</h2>
            <h5 class="good-color">Won</h5>
          </div>
        </div>
      </div>
    <?php include("components/global/footer.inc"); ?>
    <?php include("components/global/foot.inc"); ?>
  </body>
</html>
