<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Reference Interview - Albert Norris</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="row">
      <div id="info-bar" class="col s12 m4 l3">
        <div class="sidebar">
          <div class="row">
            <div class="col m4">
              <img class="circle" src="/images/albert.jpg">
            </div>
            <div class="col m8">
              <h5 class="martop-0"><a href="/candidate-albert-norris.php">Albert Norris</a></h5>
              <ul>
                <li><strong>Scorecard:</strong> <a href="/scorecard-developer.php">Developer</a></li>
                <li><strong>Appt:</strong> 07/14/2015 (<a href="mailto:albert.norris@microsoft.com">email</a>) (<a href="tel://1-555-555-5555">phone</a>)</li>
              </ul>
            </div>
          </div>
          <div class="col s12">
            <h6 class="martop-0"><a href="https://www.linkedin.com/in/albertcmartin" target="_blank"><i class="fa fa-linkedin-square" style="font-size: 1.25rem;"></i></a> Job History</h6>
            <table class="no-bg">
              <thead>
                <tr>
                  <th data-field="id">Years</th>
                  <th data-field="name">Position</th>
                  <th data-field="price">Company</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>2010-2015</td>
                  <td>Senior Developer</td>
                  <td>Oracle</td>
                </tr>
                <tr>
                  <td>2007-2010</td>
                  <td>Junior Developer</td>
                  <td>Microsoft</td>
                </tr>
                <tr>
                  <td>2006-2007</td>
                  <td>Intern</td>
                  <td>Microsoft</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col hide-on-small-only s12">
            <h5 class="marbot-0 martop-2">Jump to Reference</h5>
            <ul class="table-of-contents">
              <li><a href="#oracle-dev">John Franklin</a></li>
              <li><a href="#micro-dev">Ben Jackson</a></li>
              <li><a href="#micro-intern">Sally Loyd</a></li>
              <li><a href="#general">General Notes</a></li>
            </ul>
          </div>
        </div>
      </div>     

      <div class="col s12 m8 l9 offset-l3 marbot-5">
        <div class="section">
          <a href="dashboard.php">Dashboard</a> // <a href="/interviews.php">Interviews</a> // References - Albert Norris
        </div>
        <div class="section">
          <div class="row">
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div id="oracle-dev" class="scrollspy collapsible-header"><i class="material-icons">perm_identity</i>John Franklin: Oracle - Lead Developer</div>
                <div class="collapsible-body padall-1">
                  <p>Albert reported to John at Oracle as a Senior Developer 2010-2015<br>Appointment set for 07/17/2015</p>
                    <?php include("components/global/interview-ref-questions.inc"); ?>
                  <h5 class="marbot-1">Albert mentioned that he struggled with hitting deadlines in that job. Can you tell me about that? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="References will resist being authentic in this answer.  They tend to err on the side of kindness toward the candidate...just human nature.  Free them to be honest bu saying things like HE/SHE SAID YOU MIGHT SAY..."><i class="material-icons">info_outline</i></a></h5>
                  <form class="marbot-2">
                    <textarea class="edit text-small" name="low-1" rows="6"></textarea>
                  </form> 
                </div>
              </li>
              <li>
                <div id="micro-dev" class="scrollspy collapsible-header"><i class="material-icons">perm_identity</i>Ben Jackson: Microsoft - Developer</div>
                <div class="collapsible-body padall-1">
                  <p>Albert reported to Ben at Microsoft as a Junior Developer 2007-2010<br>Appointment not set</p>
                    <?php include('components/global/interview-ref-questions.inc'); ?>
                  <h5 class="marbot-1">Albert mentioned that he struggled with a team member in that job.  Can you tell me about that? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="References will resist being authentic in this answer.  They tend to err on the side of kindness toward the candidate...just human nature.  Free them to be honest bu saying things like HE/SHE SAID YOU MIGHT SAY..."><i class="material-icons">info_outline</i></a></h5>
                  <form class="marbot-2">
                    <textarea class="edit text-small" name="low-1" rows="6"></textarea>
                  </form> 
                </div>
              </li>
              <li>
                <div id="micro-intern" class="scrollspy collapsible-header"><i class="material-icons">perm_identity</i>Sally Loyd: Microsoft - Developer</div>
                <div class="collapsible-body padall-1">
                  <p>Albert reported to Sally at Microsoft as a Junior Developer 2006-2007<br>Appointment not set</p>
                    <?php include('components/global/interview-ref-questions.inc'); ?>
                  <h5 class="marbot-1">Albert mentioned that he struggled with finding enough work in that job.  Can you tell me about that? <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="References will resist being authentic in this answer.  They tend to err on the side of kindness toward the candidate...just human nature.  Free them to be honest bu saying things like HE/SHE SAID YOU MIGHT SAY..."><i class="material-icons">info_outline</i></a></h5>
                  <form class="marbot-2">
                    <textarea class="edit text-small" name="low-1" rows="6"></textarea>
                  </form> 
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div id="general" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">General Notes</h5>
            <form>
              <textarea class="edit text-small" name="test"></textarea>
            </form> 
          </div>
        </div>
        <div id="general" class="section scrollspy">
          <div class="row">
            <h5 class="marbot-1">Candidate's Reference Interviews Checklist and Score <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Only those who have both checks and a score of 90+ go on to the next interview."><i class="material-icons">info_outline</i></a></h5>
            <form>
              <div class='col s12 white z-depth-1'>
                <div class="input-field col s6 m5 border-right">
                  <input type="checkbox" class="filled-in" id="push-pull" />
                  <label for="push-pull">References let their guards down and gave real answers.</label>
                  <input type="checkbox" class="filled-in" id="references" />
                  <label for="references">Heard and understood code for risky candidates.</label>
                </div>
                <div class="input-field col s6 m2 martop-2">
                  <input placeholder="1-100" id="score" type="number" min="1" max="100">
                  <label for="score">References Score</label>
                </div>
                <div class="input-field col s6 m3 martop-2">
                  <a class="btn disabled">Promote to Hire</a>
                </div>
              </div>
            </form> 
          </div>
        </div>
        <div class="fixed-action-btn" style="bottom: 50px; left: 24px;">
          <h6 class="good-color">last autosave: 12:43pm</h6>
        </div>
      </div>
    </main>
    <?php include("components/global/footer.inc"); ?>
    <?php include("components/global/foot.inc"); ?>
  </body>
</html>
