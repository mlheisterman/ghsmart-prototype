<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>ClickModel Core</title>
    <!-- Grab the prettify script to output HTML Code -->
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?linenums=false"></script>

    <?php include("components/global/head.inc"); ?>
  </head>
  <body>      
    <div class="row">
      <div class="large-12 columns">
        <a href="/">&laquo; Home</a>
      </div>
    </div>
    
    <div class="row">
      <div class="small-12 columns">
        <div class="island marbot-5">
          <div class="island-header">
            <h1>Utilities</h1>
          </div>
          <div class="island-contents">
            Utilities are classes that use basic CSS in order to speed up some of the tedious styling requirements. These classes help you nudge, hide, pad, and adjust size of HTML elements. Add these classes straight into your HTML.
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 medium-4 large-3 columns">
        <ul class="side-nav no-bullets sticky">
          <h4>Utilities</h4>
          <li><a href="#margins">Margins</a></li>
          <li><a href="#padding">Padding</a></li>
          <li><a href="#kerning">Kerning</a></li>
          <li><a href="#text-size-shape">Text Size &amp; Shape</a></li>
          <li><a href="#buttons">Buttons</a></li>
          <li><a href="#gradients">Gradients</a></li>
          <li><a href="#display">Display</a></li>
        </ul>
      </div>

      <div class="small-12 medium-8 large-9 columns">
        <div class="component">
          <a id="margins"></a>
          <h2>Margins</h2>
          <p>Margins (thickness 1-5) can be added to any HTML element using the following classes:</p>
          <ul class="no-bullets panel">
            <li>marall-0 &mdash; marall-5</li>
            <li>martop-0 &mdash; marall-5</li>
            <li>marright-0 &mdash; marall-5</li>
            <li>marbot-0 &mdash; marall-5</li>
            <li>marleft-0 &mdash; marall-5</li>
          </ul>
          <br>

          <p>Here are a few examples of this utility in use:</p>

          <pre class="prettyprint">
            &lt;div class="marbot-2">&lt;/div&gt;
            &lt;section class="marall-5"&gt;&lt;/section&gt;
            &lt;p class="marleft-5"&gt;&lt;/p&gt;
            &lt;blockquote class="marright-0"&gt;&lt;/p&gt;
          </pre>
        </div>

        <div class="component">
          <a id="padding"></a>
          <h2>Padding</h2>
          <p>Padding (thickness 1-5) can be added to any HTML element using the following classes:</p>
          <ul class="no-bullets panel">
            <li>padall-0 &mdash; padall-5</li>
            <li>padtop-0 &mdash; padall-5</li>
            <li>padright-0 &mdash; padall-5</li>
            <li>padbot-0 &mdash; padall-5</li>
            <li>padleft-0 &mdash; padall-5</li>
          </ul>
          <br>

          <p>Here are a few examples of this utility in use:</p>

          <pre class="prettyprint">
            &lt;div class="padbot-2">&lt;/div&gt;
            &lt;section class="padall-5"&gt;&lt;/section&gt;
            &lt;p class="padleft-5"&gt;&lt;/p&gt;
            &lt;blockquote class="padright-0"&gt;&lt;/blockquote&gt;
          </pre>
        </div>

        <div class="component">
          <a id="padding"></a>
          <h2>Kerning</h2>
          <p>Kerning (spacing between letters) can be added to any HTML element using the following classes:</p>
          <ul class="no-bullets panel">
            <li>kern-tight</li>
            <li>kern-normal</li>
            <li>kern-wide</li>
            <li>kern-xwide</li>
          </ul>
          <br>

          <p>Here are a few examples of this utility in use:</p>

          <pre class="prettyprint">
            &lt;p class="kern-wide">&lt;/p&gt;
            &lt;h1 class="kern-tight"&gt;&lt;/h1&gt;
            &lt;blockquote class="kern-normal"&gt;&lt;/blockquote&gt;
          </pre>
        </div>

        <div class="component">
          <a id="text-size-shape"></a>
          <h2>Text Size &amp; Shape</h2>
          <p>You can adjust font size and case easily using these classes:</p>
          <ul class="no-bullets panel">
            <li>text-tiny</li>
            <li>text-small</li>
            <li>text-normal</li>
            <li>text-medium</li>
            <li>text-large</li>
            <li>text-xlarge</li>
            <li>text-xxlarge</li>
            <li>text-huge</li>
            <li>uppercase</li>
          </ul>
          <br>

          <p>Here are a few examples of this utility in use:</p>

          <pre class="prettyprint">
            &lt;p class="text-tiny">&lt;/p&gt;
            &lt;h1 class="text-huge uppercase"&gt;&lt;/h1&gt;
            &lt;blockquote class="text-normal"&gt;&lt;/blockquote&gt;
          </pre>
        </div>

        <div class="component">
          <a id="buttons"></a>
          <h2>Buttons</h2>
          <p>Buttons are easy, just add the button class and any desired modifying classes:</p>
          <ul class="no-bullets panel">
            <li>.button <i class="fa fa-arrow-right text-small"></i> <em>make a default button</em></li>
            <li>.secondary <i class="fa fa-arrow-right text-small"></i> <em>use the secondary button style</em></li>
            <li>.expand <i class="fa fa-arrow-right text-small"></i> <em>expand the button to take up entire container width</em></li>            
          </ul>
          <br>

          <p>Here are a few examples of this utility in use:</p>

          <pre class="prettyprint">
            &lt;a class="button">&lt;/p&gt;
            &lt;a class="button expand"&gt;&lt;/a&gt;
            &lt;a class="button secondary"&gt;&lt;/a&gt;
          </pre>
        </div>

        <div class="component">
          <a id="display"></a>
          <h2>Display</h2>
          <p>You can use basic display classes as needed:</p>
          <ul class="no-bullets panel">
            <li>.highlighted</li>
            <li>.hidden</li>
            <li>.visibile</li>
            <li>.hide-overflow</li>
            <li>.d-block</li>
          </ul>
          <br>

          <p>Here are a few examples of this utility in use:</p>

          <pre class="prettyprint">
            &lt;div class="hide-overflow">&lt;/div&gt;
            &lt;div class="hidden">&lt;/div&gt;
            &lt;div class="visible">&lt;/div&gt;
            &lt;span class="d-block">&lt;/span&gt;
          </pre>
        </div>


        <div class="component">
          <a id="gradients"></a>
          <h2>Gradients</h2>
          <p>You can't add gradients straight in HTML. You'll have to do this in CSS...</p>
          <p>Gradients are a little trickier than the other utilities because they require using a mixin. By default the gradient mixin uses a white to black so you'll need to change this by passing in hexidecimal values into the mixin. Here's the code:</p>

          <pre class="prettyprint">
            /* Create a gradient from left to right going from medium to dark gray */
            .container {
              @include gradient-lr(#666,#222);
            }

            /* Create a gradient from top to bottom going from blue to green */
            .container {
              @include gradient-tb(#3384e6,#37c62b);
            }
          </pre>
        </div>






      </div>
    </div>

    <?php include("components/global/foot.inc"); ?>
  </body>
</html>
