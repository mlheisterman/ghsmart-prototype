<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Developer | Edit</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="container">
      <div class="row">
        <?php include("components/global/side-upcoming.inc"); ?>
        <div class=" col s12 m9 border-left"> <!-- BEGIN Right Column -->
          <div class="section">
            <a href="/dashboard.php">Dashboard</a> // <a href="/scorecards.php">Scorecards</a> // Developer
          </div>
          <div class="section">
            <div class="row"> <!-- BEGIN Section Header -->
              <h4 class="col s12">Developer Scorecard <a href="/scorecard-developer-edit.php" class="marleft-1 btn tiny">Edit</a></h4>
              <a target="_blank" href="https://www.linkedin.com" class="col btn marleft-1 marright-1 fake-link">Search Linkedin for Matches</a>
            </div> <!-- END Section Header -->
            <div class="col s12 padall-1">
              <form>
                <div class="row white padall-1">
                  <h5 class="col s12">General Information</h5>
                  <div class="col s12">
                    <h6>Mission</h6>
                    <p>To deliver lasting value to our clients through the selection and implementation of bestfit web
                    technologies that solve their business problems elegantly, securely and reliably, all with a
                    friendly, clientcentered approach to service and communication.</p>
                  </div>
                </div>
                <div class="row white padall-1">
                  <h5 class="col s12">Critical Competencies (choose all applicable)</h5>
                  <div class="col s12 m12">
                    <table class="bordered white"><!-- BEGIN Table -->
                      <thead>
                        <tr>
                          <th>Competency</th>
                          <th>Description</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <b>Efficiency</b>
                          </td>
                          <td>
                            <p>Able to produce significant output with minimal wasted effort.</p>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <b>Enthusiasm</b>
                          </td>
                          <td>
                            <p>Exhibits passion and excitement over work. Has a can-do attitude.</p>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <b>Work Ethic</b>
                          </td>
                          <td>
                            <p>Possesses a strong willingness to work hard and sometimes long hours to get the job done. Has a track record of working hard.</p>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <b>Aggressiveness</b>
                          </td>
                          <td>
                            <p>Moves quickly and takes a forceful stand without being overly abrasive.</p>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <b>Creativity/Innovation</b>
                          </td>
                          <td>
                            <p>Generates new and innovative approaches to problems.</p>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <b>High Standards</b>
                          </td>
                          <td>
                            <p>Expects personal performance and team performance to be nothing short of the best.</p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row white martop-1 padall-1">
                  <h5 class="col s12">Outcomes</h5>
                  <div class="col s12 m12">
                    <table class="bordered white inner-list"><!-- BEGIN Table -->
                      <thead>
                        <tr>
                          <th>Outcome</th>
                          <th>Details</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <p>Maintain 60% test coverage on all code released.</p>
                          </td>
                          <td>
                            <ul>
                              <li>Tracking coverage with codecy, we can see how well your code is performing.</li>
                            </ul>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <p>Produce at a velocity that matches or exceeds the rest of the team.</p>
                          </td>
                          <td>
                            <ul>
                              <li>Building and sizing a backlog is very important as it is an indicator of your attention to detail.</li>
                            </ul>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <p>Add one coding language or framework to our toolkit per year.</p>
                          </td>
                          <td>
                            <ul>
                              <li>We already have PHP, Objective C, Ruby, HTML, CSS, and mySQL javascript languages.  As for frameworks, we have Angular, Ember, Drupal, sails, mongoDB, and Solr.</li>
                            </ul>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <p>Improve adherence to sprint point commitments to 90%.</p>
                          </td>
                          <td>
                            <ul>
                              <li>Our teams are currently at about 85%.</li>
                            </ul>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div> <!-- END Right Column -->
      </main>
        <?php include("components/global/footer.inc"); ?>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
