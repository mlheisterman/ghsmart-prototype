<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Overview</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="container">
      <div class="row">
        <?php include("components/global/side-upcoming.inc"); ?>
        <div class=" col s12 m9 border-left"> <!-- BEGIN Right Column -->
          <div class="section martop-2">
            <div class="row"> <!-- BEGIN Section Header -->
              <div class="col s12">
                <h4 class="left">Candidate Pipeline</h4>
                <div class="right">
                  <!-- Dropdown Trigger -->
                  <a class='dropdown-button btn pop' data-beloworigin="true" constrainwidth="false" href='#' data-activates='new-items'><i class="fa fa-plus left"></i> New</a>

                  <!-- Dropdown Structure -->
                  <ul id='new-items' class='dropdown-content'>
                    <li><a href="/new-interview.php">Interview</a></li>
                    <li><a href="/new-candidate.php">Candidate</a></li>
                    <li><a href="/new-scorecard.php">Scorecard</a></li>
                  </ul>
                </div>
              </div>
            </div> <!-- END Section Header -->

            <div class="divider"></div>

            <div class="row"> <!-- BEGIN Tabs -->
              <div class="col s12"> <!-- BEGIN Tab Navigation -->
                <ul class="tabs marbot-1">
                  <li class="tab col l2"><a href="#screening">Screen<span class="pellet">3</span></a></li>
                  <li class="tab col l2"><a href="#topgrading">Topgrade<span class="pellet">3</span></a></li>
                  <li class="tab col l2"><a href="#focus">Focus<span class="pellet">2</span></a></li>
                  <li class="tab col l2"><a href="#reference" class="active">Reference<span class="pellet">3</span></a></li>
                  <li class="tab col l2"><a href="#offer">Offers<span class="pellet">1</span></a></li>
                </ul>
              </div> <!-- END Tab Navigation -->


              <div id="screening" class="col s12"> <!-- BEGIN First Tab Content -->
                <table class="responsive-table white bordered">
                  <thead>
                    <tr>
                      <th width="60%">Candidate</th>
                      <th width="10%">Fit</th>
                      <th width="20%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="odd">
                      <td>
                        <img src="/images/josh.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Business Development</h6>
                        <a href="#!" class="fake-link">Steve Larson</a>
                      </td>
                      <td>
                        <h4>50<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                    <tr class="even">
                      <td>
                        <img src="/images/nick.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">UX Design</h6>
                        <a href="#!" class="fake-link">Nick Lawrence</a>
                      </td>
                      <td>
                        <h4>80<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                    <tr class="odd">
                      <td>
                        <img src="/images/chris.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Developer</h6>
                        <a href="#!" class="fake-link">Chris Walker</a>
                      </td>
                      <td>
                        <h4>90<sup>%</sup></h4>
                      </td>
                      <td width="185">
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                  </tbody>
                </table>
              </div> <!-- END First Tab Content -->
              <div id="topgrading" class="col s12"> <!-- BEGIN Second Tab Content -->
                <table class="responsive-table white bordered">
                  <thead>
                    <tr>
                      <th width="60%">Candidate</th>
                      <th width="10%">Fit</th>
                      <th width="20%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="odd">
                      <td>
                        <img src="/images/matt.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Business Development</h6>
                        <a href="#!" class="fake-link">Matt Pitts</a>
                      </td>
                      <td>
                        <h4>80<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                    <tr class="even">
                      <td>
                        <img src="/images/mansi.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">UX Design</h6>
                        <a href="#!" class="fake-link">Mansi Puri</a>
                      </td>
                      <td>
                        <h4>80<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                    <tr class="odd">
                      <td>
                        <img src="/images/josh.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">UX Design</h6>
                        <a href="#!" class="fake-link">Josh Manor</a>
                      </td>
                      <td>
                        <h4>90<sup>%</sup></h4>
                      </td>
                      <td width="185">
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                  </tbody>
                </table>
              </div> <!-- END Second Tab Content -->          
              <div id="focus" class="col s12"> <!-- BEGIN Third Tab Content -->
                <table class="responsive-table white bordered">
                  <thead>
                    <tr>
                      <th width="50%">Candidate</th>
                      <th width="10%"></th>
                      <th width="10%">Fit</th>
                      <th width="20%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="odd">
                      <td>
                        <img src="/images/chris.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Developer</h6>
                        <a href="#!" class="fake-link">James Thomas</a>
                      </td>
                      <td>
                        <div class="bullseye-container">
                          <div class="bullseye skill">Skill</div>
                          <div class="bullseye scorecard ninety">Scorecard</div>
                          <div class="bullseye will ninety">Will</div>
                        </div>
                      </td>
                      <td>
                        <h4>90<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                    <tr class="even">
                      <td>
                        <img src="/images/matt.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Developer</h6>
                        <a href="#!" class="fake-link">Kevin Jacobs</a>
                      </td>
                      <td>
                        <div class="bullseye-container">
                          <div class="bullseye skill seventy">Skill</div>
                          <div class="bullseye scorecard seventy">Scorecard</div>
                          <div class="bullseye will eighty">Will</div>
                        </div>
                      </td>
                      <td>
                        <h4>60<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                  </tbody>
                </table>
              </div> <!-- END Third Tab Content -->
              <div id="reference" class="col s12"> <!-- BEGIN Fourth Tab Content -->
                <table class="responsive-table white bordered">
                  <thead>
                    <tr>
                      <th width="50%">Candidate</th>
                      <th width="10%"></th>
                      <th width="10%">Fit</th>
                      <th width="20%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="even">
                      <td>
                        <img src="/images/albert.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Developer</h6>
                        <a href="/candidate-albert-norris.php">Albert Norris</a>
                      </td>
                      <td>
                        <div class="bullseye-container">
                          <div class="bullseye skill">Skill</div>
                          <div class="bullseye scorecard ninety">Scorecard</div>
                          <div class="bullseye will ninety">Will</div>
                        </div>
                      </td>
                      <td>
                        <h4>94<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>
                    <tr class="odd">
                      <td>
                        <img src="/images/nick.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Designer</h6>
                        <a href="#!" class="fake-link">Vincent Adams</a>
                      </td>
                      <td>
                        <div class="bullseye-container">
                          <div class="bullseye skill">Skill</div>
                          <div class="bullseye scorecard ninety">Scorecard</div>
                          <div class="bullseye will ninety">Will</div>
                        </div>
                      </td>
                      <td>
                        <h4>90<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                    <tr class="even">
                      <td>
                        <img src="/images/mansi.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Developer</h6>
                        <a href="#!" class="fake-link">Mansi Puri</a>
                      </td>
                      <td>
                        <div class="bullseye-container">
                          <div class="bullseye skill">Skill</div>
                          <div class="bullseye scorecard seventy">Scorecard</div>
                          <div class="bullseye will ninety">Will</div>
                        </div>
                      </td>
                      <td>
                        <h4>80<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Promote</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Dismiss</a></div>
                      </td>
                    </tr>

                  </tbody>
                </table>
              </div> <!-- END Fourth Tab Content -->
              <div id="offer" class="col s12"> <!-- BEGIN Fifth Tab Content -->
                <table class="responsive-table white bordered">
                  <thead>
                    <tr>
                      <th width="30%">Candidate</th>
                      <th width="30%"></th>
                      <th width="10%">Fit</th>
                      <th width="30%"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="odd">
                      <td>
                        <img src="/images/albert.jpg" alt="" class="marright-1 left circle small">
                        <h6 class="title">Developer</h6>
                        <a href="#!" class="fake-link">Zack Richards</a>
                      </td>
                      <td>
                        <div class="bullseye-container">
                          <div class="bullseye skill ninety">Skill</div>
                          <div class="bullseye scorecard ninety">Scorecard</div>
                          <div class="bullseye will ninety">Will</div>
                        </div>
                      </td>
                      <td>
                        <h4>100<sup>%</sup></h4>
                      </td>
                      <td>
                        <div><a class="btn promote btn-block fake-link"><i class="material-icons left">thumb_up</i>Accept Offer</a></div>
                        <div><a class="btn dismiss btn-block fake-link"><i class="material-icons left">thumb_down</i>Reject Offer</a></div>
                      </td>
                    </tr>

                  </tbody>
                </table>
              </div> <!-- END Fifth Tab Content -->
            </div> <!-- END Tabs --> 
          </div>
        </div> <!-- END Right Column -->
      </main>
        <?php include("components/global/footer.inc"); ?>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
