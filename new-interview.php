<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | New Interview</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="container">
      <div class="row">
        <?php include("components/global/side-upcoming.inc"); ?>
        <div class=" col s12 m9 border-left"> <!-- BEGIN Right Column -->
          <div class="section">
            <a href="/dashboard.php">Dashboard</a> // <a href="/interviews.php">Interviews</a> // New Interview
          </div>
          <div class="section">
            <div class="row"> <!-- BEGIN Section Header -->
              <h4 class="col s12">New Interview</h4>
            </div> <!-- END Section Header -->
            <div class="col s12 padall-1">
              <form>
                <div class="row white padall-1">
                  <h5 class="col s12">Candidate</h5>
                  <div class="input-field col s6">
                    <input id="name" type="text" class="validate">
                    <label for="name">Candidate Name (start typing)</label>
                  </div>
                </div>
                <div class="row white martop-1 padall-1">
                  <h5 class="col s12">Interview</h5>
                  <div class="input-field col s12 m4">
                    <select id="period">
                      <option value=""></option>
                      <option value="1">All</option>
                      <option value="2">Screening</option>
                      <option value="3">Topgrading</option>
                      <option value="4">Focus</option>
                      <option value="5">Reference</option>
                    </select>
                    <label for="period">Interview Type</label>
                  </div>
                  <div class="col s12 m4">
                    <label for="date" class="">Pick a Date</label>
                    <input type="date" class="datepicker">
                  </div>
                  <div class="col s12 m4">
                    <label for="time" class="time">Time</label>
                    <input type="time" class="time" name="time">
                  </div>
                </div>
              </form>
              <a href="/interviews.php" class="col btn">Create Interview</a>
            </div>
          </div>
        </div> <!-- END Right Column -->
      </main>
        <?php include("components/global/footer.inc"); ?>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
