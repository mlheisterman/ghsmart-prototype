<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Albert Norris | Edit</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <main class="container">
      <div class="row">
        <?php include("components/global/side-upcoming.inc"); ?>
        <div class=" col s12 m9 border-left"> <!-- BEGIN Right Column -->
          <div class="section">
            <a href="/dashboard.php">Dashboard</a> // <a href="/candidates.php">Candidates</a> // <a href="/candidate-albert-norris.php">Albert Norris</a> // Edit
          </div>
          <div class="section">
            <div class="row"> <!-- BEGIN Section Header -->
              <h4 class="col s12">Albert Norris - Edit</h4>
            </div> <!-- END Section Header -->
            <div class="col s12 padall-1">
              <form>
                <div class="row white padall-1">
                  <h5 class="col s12">Candidate Information</h5>
                  <div class="input-field col s12 m6">
                    <input id="first_name" type="text" class="validate " tabindex="1" value="Albert">
                    <label for="first_name">First Name</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <input id="address_1" type="text" class="validate" tabindex="5" value="1234 Main Street">
                    <label for="address_1">Address 1</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <input id="last_name" type="text" class="validate" tabindex="2" value="Norris">
                    <label for="last_name">Last Name</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <input id="address_2" type="text" class="validate" tabindex="6">
                    <label for="address_2">Address 2</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <input id="email" type="email" class="validate" tabindex="3" value="albert.norris@microsoft.com">
                    <label for="email">Email</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <input id="city" type="text" class="validate" tabindex="7" value="Austin">
                    <label for="city">City</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <input id="phone" type="tel" class="validate" tabindex="4" value="+1 (555) 555-5555">
                    <label for="phone">Phone</label>
                  </div>
                  <div class="input-field col s12 m3">
                    <input id="state" type="text" class="validate" tabindex="8" value="TX">
                    <label for="state">State</label>
                  </div>
                  <div class="input-field col s12 m3">
                    <input id="number" type="text" min="5" max="5" class="validate" tabindex="9" value="78749">
                    <label for="number">Zip</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <input id="linkedin" type="url" class="validate" tabindex="10" value="http://linkedin.com/in/albertcmartin">
                    <label for="linkedin">Linkedin URL</label>
                  </div>
                  <div class="input-field col s12 m6">
                    <a href="#!" class="btn tiny fake-link" tabindex="11">Update Linkedin Job History (3 Listed)</a>
                  </div>

                </div>
                <div class="row white martop-1 padall-1">
                  <h5 class="col s12">Candidate Source</h5>
                  <div class="input-field col s12 m6">
                    <select id="source" tabindex="12">
                      <option value=""disabled>Source of Candidate</option>
                      <option value="1">Personal Network</option>
                      <option value="2" selected>Professional Network</option>
                      <option value="3">Recruiter</option>
                      <option value="4">Researcher</option>
                    </select>
                  </div>
                  <div class="input-field col s12 m6">
                    <input id="source_name" type="text" class="validate" tabindex="13" value="Josh Gillespie">
                    <label for="source_name">Name of Source</label>
                  </div>
                </div>
                <div class="row white martop-1 padall-1">
                  <h5 class="col s12">Candidate Scorecard</h5>
                  <div class="input-field col s12 m6">
                    <select id="type" tabindex="14">
                      <option value=""disabled>Choose Scorecard</option>
                      <option value="2">Business Development</option>
                      <option value="3" selected>Developer</option>
                      <option value="4">Lead Developer</option>
                      <option value="5">Senior Developer</option>
                    </select>
                  </div>
                </div>
                <a href="/candidate-albert-norris.php" class="col btn" tabindex="15">Update Candidate</a>
              </form>
            </div>
          </div>
        </div> <!-- END Right Column -->
      </main>
        <?php include("components/global/footer.inc"); ?>
        <?php include("components/global/foot.inc"); ?>
    </div>
  </body>
</html>
