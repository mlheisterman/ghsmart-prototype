<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>gARVIS | Reports</title>
    <?php include("components/global/head.inc"); ?>
  </head>
  <body>
    <?php include("components/specific/nav-auth.inc"); ?>
    <div>
      <div class="section">
        <div class="row">
          <h5 class="col">Reports</h5>
        </div>
        <div class="row">
          <div class="col s12 m3">
            <!-- Dropdown Trigger -->
            <a class='dropdown-button btn' data-beloworigin="true" constrainwidth="false" href='#' data-activates='new-items'><i class="fa fa-filter left"></i>All Scorecards</a>

            <!-- Dropdown Structure -->
            <ul id='new-items' class='dropdown-content'>
              <li><a class="fake-link" href="#!">Business Development</a></li>
              <li><a class="fake-link" href="#!">Developer</a></li>
              <li><a class="fake-link" href="#!">Lead Developer</a></li>
              <li><a class="fake-link" href="#!">Senior Developer</a></li>
            </ul>
          </div>
          <div class="col s12 m9">
            <span class="left marright-base">Time Period:</span>
            <a href="#!" class="fake-link btn tiny left marright-base grey">Today</a>
            <a href="#!" class="fake-link btn tiny left marright-base grey">Yesterday</a>
            <a href="#!" class="fake-link btn tiny left marright-base grey">This Week</a>
            <a href="#!" class="fake-link btn tiny left marright-base grey">Last Week</a>
            <a href="#!" class="fake-link btn tiny left marright-base grey">This Month</a>
            <a href="#!" class="fake-link btn tiny left marright-base grey">Last Month</a>
            <a href="#!" class="fake-link btn tiny left marright-base grey">This Quarter</a>
            <a href="#!" class="btn tiny left marright-base">Last Quarter</a>
            <a href="#!" class="fake-link btn tiny left marright-base grey">Custom Period</a>
          </div>
        </div>

        <div class="row marbot-2 martop-4">
          <div class="metrics-container">
            <div class="col s12 m6 l3 center">
              <h3 class="martop-0 marbot-0"><strong>100</strong></h3>
              <label class="uppercase dark-color">New Candidates</label>
            </div>
            <div class="col s12 m6 l3 center">
              <h3 class="martop-0 marbot-0"><strong>33</strong></h3>
              <label class="uppercase dark-color">Candidates Screened per Hire</label>
            </div>
            <div class="col s12 m6 l3 center">
              <h3 class="martop-0 marbot-0"><strong>5</strong></h3>
              <label class="uppercase dark-color">Offered</label>
            </div>
            <div class="col s12 m6 l3 center">
              <h3 class="martop-0 marbot-0"><strong>3</strong></h3>
              <label class="uppercase dark-color">New Hires</label>
            </div>
          </div>
        </div>

        <div class="row marbot-1">
          <div class="col s12 reports">
            <div class="col s4 m2 relative">
              <div class="data-bar padall-1 center-align green z-depth-1">
                <h3 class="martop-0 marbot-0 white-text">50<sup>%</sup></h3>
                <label class="uppercase white-text">50 Candidates</label>
              </div>
              <div class="interview left card blue lighten-2">
                <div class="card-action center-align grey darken-2">
                  <a class="fake-link white-text marright-0" href="#">Screening</a>
                </div>
                <div class="card-content white-text center-align"><p class="padtop-4 padbot-4"></p></div>
              </div>
            </div>
            <div class="col s4 m2 relative">
              <div class="data-bar padall-1 center-align green z-depth-1">
                <h3 class="martop-0 marbot-0 white-text">60<sup>%</sup></h3>
                <label class="uppercase white-text">30 Candidates</label>
              </div>
              <div class="interview left card blue lighten-1">
                <div class="card-action center-align grey darken-2">
                  <a class="fake-link white-text marright-0" href="#">Topgrading</a>
                </div>
                <div class="card-content white-text center-align"><p class="padtop-4 padbot-4"></p></div>
              </div>
            </div>
            <div class="col s4 m2 relative">
              <div class="data-bar padall-1 center-align green z-depth-1">
                <h3 class="martop-0 marbot-0 white-text">50<sup>%</sup></h3>
                <label class="uppercase white-text">15 Candidates</label>
              </div>
              <div class="interview left card blue">
                <div class="card-action center-align grey darken-2">
                  <a class="fake-link white-text marright-0" href="#">Focus</a>
                </div>
                <div class="card-content white-text center-align"><p class="padtop-4 padbot-4"></p></div>
              </div>
            </div>
            <div class="col s4 m2 relative">
              <div class="data-bar padall-1 center-align green z-depth-1">
                <h3 class="martop-0 marbot-0 white-text">33<sup>%</sup></h3>
                <label class="uppercase white-text">05 Candidates</label>
              </div>
              <div class="interview left card blue darken-1">
                <div class="card-action center-align grey darken-2">
                  <a class="fake-link white-text marright-0" href="#">Reference</a>
                </div>
                <div class="card-content white-text center-align"><p class="padtop-4 padbot-4"></p></div>
              </div>
            </div>
            <div class="col s4 m2 relative">
              <div class="data-bar padall-1 center-align green z-depth-1">
                <h3 class="martop-0 marbot-0 white-text">60<sup>%</sup></h3>
                <label class="uppercase white-text">03 Candidates</label>
              </div>
              <div class="interview left card blue darken-2">
                <div class="card-action center-align grey darken-2">
                  <a class="fake-link white-text marright-0" href="#">Offer Made</a>
                </div>
                <div class="card-content white-text center-align"><p class="padtop-4 padbot-4"></p></div>
              </div>
            </div>
            <div class="col s4 m2 relative">
              <div class="interview left card blue darken-3">
                <div class="card-action center-align grey darken-2">
                  <a class="fake-link white-text marright-0" href="#">Offer Accepted</a>
                </div>
                <div class="card-content white-text center-align"><p class="padtop-4 padbot-4"></p></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row marbot-4">
          <div class="col s12 l4 center">
            <hgroup>
              <h3><strong>97</strong></h3>
              <h5>Candidates Dismissed</span></h5>
            </hgroup>
          </div>
          <div class="col s12 l4 relative center">
            <span>&nbsp;</span>
            <div id="pieContainer">
              <div class="pieBackground z-depth-1"></div>
              <div id="pieSlice1" class="hold"><div class="pie"></div></div>
            </div>
          </div>
          <div class="col s12 l4 center">
            <hgroup>
              <h3><strong>3</strong></h3>
              <h5>Candidates Accepted</span></h5>
            </hgroup>
          </div>
        </div>

        <div class="row">
          <div class="col s12 m6 center">
            <h2 class="bad-color">40<sup>%</sup></h2>
            <h5 class="bad-color">Offer Rejected</h5>
          </div>
          <div class="col s12 m6 center">
            <h2 class="good-color">60<sup>%</sup></h2>
            <h5 class="good-color">Offer Accepted</h5>
          </div>
        </div>
      </div>
    <?php include("components/global/footer.inc"); ?>
    <?php include("components/global/foot.inc"); ?>
  </body>
</html>
